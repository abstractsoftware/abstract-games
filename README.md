# Abstract Games

Is a suite of games for Linux systems. To send bug reports, look at https://gitlab.com/abstractsoftware/abstract-games/issues/new. For to look at the wiki, and find out how to install and use Abstract Games, look at https://gitlab.com/abstractsoftware/abstract-games/-/wikis/home. All of the source code is licenses under the Mozilla Public License 2.0.

[![Get it from the Snap Store](https://snapcraft.io/static/images/badges/en/snap-store-white.svg)](https://snapcraft.io/abstractgames)
