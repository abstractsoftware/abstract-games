This is the TODO list for Abstract Games:

- [ ] Add in winds into Frogger

- [ ] Change icons in Frogger

- [ ] Add in clouds into JetFighter

- [ ] Complete AI in Chess

- [ ] Fix bugs in iGo

- [ ] Complete SpaceInvaders

- [ ] Make it so that you can use arrows keys in JetFighter and Frogger

- [ ] Add in settings to change which players goes first

- [ ] Fix memory leak in AbstractGames

- [ ] Add whats new screen

- [ ] Improve performance of AbstractGames

- [ ] Add file dialogs so that one can select a game from an external file in AbstractGames
