import QtQuick 2.7
import QtQuick.Controls 2.11
import QtQuick.Layouts 1.7
import QtQuick.Controls.Material 2.9

Pane {
	Material.theme: "Dark"
	Material.accent: Material.Green
	Material.foreground: "white"
	width: 600
	height: 600
	padding: 0
	font.family: "Ubuntu"

	ColumnLayout {
		anchors.fill: parent
		spacing: 0

		ToolBar {
			Material.primary: Material.Teal
			Layout.fillWidth: true
			height: 200

			RowLayout {
				anchors.verticalCenter: parent.verticalCenter
				Label {
					Layout.leftMargin: 18
					text: "Abstract Games"
				}
			}
		}

		Pane {
			padding: 0
			leftPadding: 5
			topPadding: 5
			Layout.topMargin: 5
			Layout.fillHeight: true


			ColumnLayout {
				Label {
					text: "Information"
					font.pointSize: 15
					Layout.leftMargin: 10

				} 
				spacing: 10


				TipButton {
					title: "Abstract Games Launcher"
					text: "Click on the icons to start new games"
				}

				TipButton {
					title: "Source Code"
					text: "visit gitlab.com/abstractsoftware/abstractgames"
					link: "https://gitlab.com/abstractsoftware/abstract-games"
				}

				TipButton {
					title: "Abstract Games License"
					text: "Abstract Games is licensed under GNU GPL version 2"
					link: "http://www.gnu.org/licenses/gpl.html"
				}


				TipButton {
					title: "Coming Soon"
					text: "Frogger is Coming Soon"
				}

				Label {
					 text: "Settings"

					 font.pointSize: 15
					 Layout.leftMargin: 10
					 renderType: Text.NativeRendering	
					 antialiasing: true
				}

				CheckBox {
					objectName: "checkbox"
					Layout.leftMargin: 5
					antialiasing: true
					text: "Enable Beta Games"
				}

				RowLayout {
					Label {
						Layout.leftMargin: 10
						text: "Default Difficulty:"
					}

					ComboBox {
						objectName: "combobox"

			 			Layout.leftMargin: 10
			 			model: ["easy", "medium", "hard"]

			 			popup.background: Rectangle {
				 			radius: 2
				 			color: "black"
						}
					}
				}	

				RowLayout {
					Label {
						Layout.leftMargin: 10
						text: "Default Players:    "
					}

					ComboBox {
						id: x
						objectName: "combobox1"

						Layout.leftMargin: 10
						model: ["one", "two"]

						popup.background: Rectangle {
							radius: 2
							color: "black"
						}
					}
				}

				RowLayout {
					TextField {
						objectName: "bundledGameName"
						Layout.leftMargin: 10
					}

					Button {
						Layout.leftMargin: 5
						objectName: "buttonAddBundledGame"
						text: "Add Game"
						width: x.width
					}
				}
			}
		}
	}
}
