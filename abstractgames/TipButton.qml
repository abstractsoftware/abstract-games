import QtQuick.Controls 2.4
import QtQuick 2.11
import QtQuick.Layouts 1.11

RowLayout {
	spacing: 10
	id: root
	property string link: ""
	property string title
	property string text

	ItemDelegate {
		implicitWidth: 200
		text: root.title
		font.bold: false

		onClicked: {
			x4.opacity = 1
		}
	}

	Label {
		id: x4
		opacity: 0
		text: root.text
		ToolTip.visible: root.link != "" ? ma.containsMouse : false
		ToolTip.text: root.link
		ToolTip.delay: 1000
		ToolTip.timeout: 2000

		Timer {
        	                            	running: parent.opacity == 1
        	                               	interval: 5000

        	                         	        onTriggered: { parent.opacity = 0 }
		}

		MouseArea {
			id: ma
			hoverEnabled: true
                                                anchors.fill: parent
                                                cursorShape: root.link != "" && x4.opacity != 0 ? Qt.PointingHandCursor : Qt.ArrowCursor
						enabled: root.link != ""

                                                onClicked: {
                                                        Qt.openUrlExternally(root.link);
                                                }
                                        }

		Behavior on opacity {
	               	             NumberAnimation { duration: 500 }
     	            }
	}
}		

