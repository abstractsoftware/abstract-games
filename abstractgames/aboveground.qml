import QtQuick 2.7
import QtQuick.Controls 2.7
import QtQuick.Controls.Material 2.6

Item {
	width: 600
	height: 600

	Dialog {
		id: dialog2
                property string theme: "Dark"
                property int accent: Material.Green
                objectName: "dialog"

                anchors.centerIn: parent
                //modal: true
		visible: false

                Material.accent: accent
                Material.theme: theme
                standardButtons: Dialog.Ok | Dialog.Cancel

                property string text: "You're sure?"

                Label { text: dialog2.text; anchors.fill: parent }
        }
}
