#include <QApplication>
#include <QQuickWidget>
#include <QQuickStyle>
#include <QString>
#include <QUrl>
#include <QVector>
#include <QIcon>
#include <QGridLayout>
#include <QFrame>
#include <QMap>
#include <QPair>
#include <QDebug>
#include <QJSValue>
#include <QSpacerItem>
#include <QStackedWidget>
#include <QTest>
#include <QScreen>
#include <QVector>
#include <QtConcurrent>
#include <QDir>
#include <QCommandLineParser>

#include <chess/main.h>
#include <jetfighter/main.h>
#include <igo/main.h>
#include <tictactoe/main.h>
#include <frogger/main.h>
#include "main.h"
#include <libabstractgames/core/core.h>
#include <libabstractgames/widgets/widgets.h>
#include <libabstractgames/logic/logic.h>

// TODO make this a class

QVector<QString> newgames;
TheWindow* widget;
QVector<GameWindow*> games;

GameWindow* addGamePage(QString name, QStackedWidget* parent, QQuickItem* comboBox, bool enabled = true, QString prefix = "", QString icon = "")
{
	GameWindow* frogger = new GameWindow(name, prefix, parent, icon);

	if (frogger->rootObject()->property("error").toBool())
	{
		delete frogger;
		return nullptr;
	}

	parent->addWidget(frogger);

	comboBox->property("add").value<QJSValue>().call({name, enabled, QUrl::fromLocalFile(frogger->icon()).toString()});
	qApp->processEvents();

	return frogger;
}

QQuickWidget* addQMLPage(QString name, QStackedWidget* parent, QQuickItem* comboBox)
{
	QQuickWidget* frogger = new QQuickWidget(QUrl("qrc:/" + name + ".qml"));
	
	parent->addWidget(frogger);

	comboBox->property("add").value<QJSValue>().call({name, true});
	qApp->processEvents();

	return frogger;
}

void reload(QQuickWidget* home, QSettings* settings, QQuickWidget* switcher, QStackedWidget* frame)
{

	QQuickWidget* splash = new QQuickWidget(QUrl("qrc:/splashscreen.qml"), widget);
	splash->setFixedSize(600, 650);
	splash->show();

	settings->beginGroup("abstractgames");
	bool disabled = settings->value("enable", false).toBool();
	home->rootObject()->findChild<QObject*>("checkbox")->setProperty("checked", disabled);
	QString defaultDifficulty = settings->value("defaultDifficulty", "medium").toString();
	home->rootObject()->findChild<QObject*>("combobox")->setProperty("currentIndex", 
			defaultDifficulty ==  "easy" ? 0 : defaultDifficulty=="medium" ? 1 : 2);

	QString defaultPlayers = settings->value("defaultPlayers", "one").toString();
	home->rootObject()->findChild<QObject*>("combobox1")->setProperty("currentIndex", 
			defaultPlayers == "one" ? 0 : 1);

	settings->endGroup();

	for (int i = 0; i < games.size(); i++)
	{
		delete games[i];
	}

	games.clear();

	switcher->rootObject()->property("clear").value<QJSValue>().call();

	games.append(addGamePage("Chess", frame, switcher->rootObject()));
	games.append(addGamePage("iGo", frame, switcher->rootObject()));
	games.append(addGamePage("TicTacToe", frame, switcher->rootObject()));
	games.append(addGamePage("Frogger", frame, switcher->rootObject(), disabled));
	games.append(addGamePage("JetFighter", frame, switcher->rootObject()));
	games.append(addGamePage("Mines", frame, switcher->rootObject()));

	for (QString newgame : newgames)
	{
		QString path = QDir(newgame).absolutePath();
		
		if (QFile(path + "/main.qml").exists())
			games.append(addGamePage(newgame, frame, switcher->rootObject(), true, \
					"file://" + path, path + "/images/icon.svg"));
		else
			qWarning("'%s' is not a valid game", qUtf8Printable(QDir(path).dirName()));
	}

	frame->setCurrentIndex(0);

	splash->hide();
	delete splash;
}

int main(int argc, char* argv[])
{
	// register
	RegisterWidgets();
	RegisterLogic();

	// app
	QApplication* app = new QApplication(argc, argv);
	QCoreApplication::setOrganizationName("abstractdevelop");
	QCoreApplication::setOrganizationDomain("abstractsoftware.gitlab.io");
	QCoreApplication::setApplicationName("Abstract Games");

	if (QQuickStyle::name().isEmpty())
	{
       		QQuickStyle::setStyle("Material");
	}

	// command line
	
	QCommandLineParser parser;
	parser.addPositionalArgument("source", QCoreApplication::translate("main", "Source file to copy."));
	parser.process(*app);

	const QStringList args = parser.positionalArguments();

	for (QString arg : args)
	{
		newgames.append(arg);
	}

	// settings
	
	QSettings settings;

	// widget
	widget = new TheWindow();
	widget->setFixedSize(600, 650);
	widget->setWindowIcon(QIcon(":/icon.svg"));
	widget->setWindowTitle("Abstract Games Launcher");
	widget->show();

	// layout
	QWidget* widget1 = new QWidget(widget);
	QGridLayout* layout = new QGridLayout(widget1);
	QStackedWidget* frame = new QStackedWidget(widget1);
	QQuickWidget* switcher = new QQuickWidget(QUrl("qrc:/switcher.qml"), widget1);
	layout->addWidget(switcher, 1, 0);
	layout->addWidget(frame, 0, 0);
	layout->setContentsMargins(0, 0, 0, 0);
	layout->setSpacing(0);

	// home
	QQuickWidget* home = addQMLPage("Home", frame, switcher->rootObject());
	Controller* c = new Controller(frame, home, &settings, switcher, widget, reload);

	// reload
	reload(home, &settings, switcher, frame);

	// controller
	QObject::connect(switcher->rootObject(), SIGNAL(choosen(int)), c, SLOT(choosen(int)));
	QObject::connect(home->rootObject()->findChild<QObject*>("checkbox"), SIGNAL(toggled()), c, SLOT(toggled()));
	QObject::connect(home->rootObject()->findChild<QObject*>("combobox"), SIGNAL(activated(int)), c, SLOT(activated(int)));
	QObject::connect(home->rootObject()->findChild<QObject*>("combobox1"), SIGNAL(activated(int)), c, 
			SLOT(activated2(int)));
	QObject::connect(home->rootObject()->findChild<QObject*>("buttonAddBundledGame"), SIGNAL(clicked()), c,
			SLOT(bundledGameClicked()));

	// cleanup
	frame->setCurrentIndex(0);
	widget1->show();

	// execute
	return app->exec();
}

bool TheWindow::onClose(QString text, bool close)
{
	if (!stackedWidget) return true;

	using GameWidget = SimpleGameWidget;
	GameController* gameController;
      
	for (int i = 1; i < stackedWidget->count(); i++)
	{
		GameWindow* window = qobject_cast<GameWindow*>(stackedWidget->widget(i));
		gameController = qobject_cast<GameController*>(window->rootObject());

		if (gameController && gameController->swipeview->property("currentIndex").value<int>() != 0)
		{
			stackedWidget->setCurrentIndex(i);
			controller->switcher->rootObject()->setProperty("currentIndex", i);

			GameWidget* gameWidget = qobject_cast<GameWidget*>(gameController->game());
			QObject* dialog2 = gameWidget->dialog2;

			dialog2->setProperty("text", text);
       			dialog2->setProperty("visible", true);

			while (true)
			{
				qApp->processEvents();

				if (dialog2->property("visible") == false)
	       			{
		       			if (dialog2->property("result").value<int>() == 1)
					{
						goto done;
					}
	   	       			else
	      	       			{
						return false;
		      			}
	     			}
       			}
		}
	}

done:
	if (close)
	{
		//readyToClose = true;
		qApp->quit();
		//this->close();
		//std::exit(0);
	}
	return true;
}
