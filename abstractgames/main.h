#pragma once
#include <QSettings>
#include <QObject>
#include <QQuickWidget>
#include <QVector>
#include <QProcess>
#include <QJSValue>
#include <QQuickItem>
#include <QDebug>
#include <QStackedWidget>
#include <QCloseEvent>
#include <QtConcurrent>

#include <libabstractgames/core/core.h>

extern QVector<QString> newgames;

class Controller;

class TheWindow : public QWidget {
	Q_OBJECT

	//bool readyToClose;

public:
	TheWindow() : QWidget() { /*readyToClose = false;*/ }
	

	QStackedWidget* stackedWidget;
	Controller* controller;

	bool onClose(QString text, bool close = true);
protected:
	void closeEvent(QCloseEvent *event)
	{
		onClose("You're sure you want to close?");
		//QtConcurrent::run([=]() { onClose("You're sure you want to close?"); });
		event->ignore();
	}

public:
	QQuickWidget* aboveground;
};

class Controller : public QObject {
        Q_OBJECT
public:
        Controller(QStackedWidget* _stackedWidget,
			QQuickWidget* _home,
			QSettings* _settings,
			QQuickWidget* _switcher,
			TheWindow* _window,
			void (*_reload)(QQuickWidget*, QSettings*, QQuickWidget*, QStackedWidget*))
		
	: QObject() {
		stackedWidget = _stackedWidget;
		home = _home;
		settings = _settings;
		switcher = _switcher;
		reload = _reload;
		window = _window;

		window->stackedWidget = stackedWidget;
		window->controller = this;
	}

public slots:

	void choosen(int index)
	{
		stackedWidget->setCurrentIndex(index);
	}

	void toggled()
	{
		bool val = home->rootObject()->findChild<QObject*>("checkbox")->property("checked").toBool();

		if (!window->onClose("You're sure you want to reload?", false))
		{
			home->rootObject()->findChild<QObject*>("checkbox")->setProperty("checked", !val);
			return;
		}

 	        settings->beginGroup("abstractgames");
	        settings->setValue("enable", val);
	        settings->endGroup();

		(*reload)(home, settings, switcher, stackedWidget);
	}

	void activated(int index)
	{
		Q_UNUSED(index);
		settings->beginGroup("abstractgames");
		QString defaultDifficulty = settings->value("defaultDifficulty", "medium").toString();
		settings->endGroup();

		if (!window->onClose("You're sure you want to reload?", false))
		{
			settings->beginGroup("abstractgames");
			home->rootObject()->findChild<QObject*>("combobox")->setProperty("currentIndex",
					defaultDifficulty ==  "easy" ? 0 : defaultDifficulty=="medium" ? 1 : 2);
			settings->endGroup();
			return;
		}


		QString val = home->rootObject()->findChild<QObject*>("combobox")->property("currentText").toString();

                settings->beginGroup("abstractgames");
                settings->setValue("defaultDifficulty", val);
                settings->endGroup();

                (*reload)(home, settings, switcher, stackedWidget);
	}

	void activated2(int index)
	{
		Q_UNUSED(index);
		settings->beginGroup("abstractgames");
		QString defaultPlayers = settings->value("defaultPlayers", "one").toString();
		settings->endGroup();

		if (!window->onClose("You're sure you want to reload?", false))
		{
			settings->beginGroup("abstractgames");
			home->rootObject()->findChild<QObject*>("combobox1")->setProperty("currentIndex",
                                defaultPlayers == "one" ? 0 : 1);
			settings->endGroup();
			return;
		}

		QString val = home->rootObject()->findChild<QObject*>("combobox1")->property("currentText").toString();

                settings->beginGroup("abstractgames");
                settings->setValue("defaultPlayers", val);
                settings->endGroup();

                (*reload)(home, settings, switcher, stackedWidget);
	}

	void bundledGameClicked()
	{
		QString text = home->rootObject()->findChild<QObject*>("bundledGameName")->property("displayText").toString();

		newgames.append(text);

		if (!window->onClose("You're sure you want to reload?", false))
		{
			return;
		}

		(*reload)(home, settings, switcher, stackedWidget);
	}

public:
	QStackedWidget* stackedWidget;
	QQuickWidget* home;
	QSettings* settings;
	QQuickWidget* switcher;
	TheWindow* window;
	void (*reload)(QQuickWidget*, QSettings*, QQuickWidget*, QStackedWidget*);
};
