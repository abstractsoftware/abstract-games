import QtQuick 2.9
import QtQuick.Layouts 1.9
import QtQuick.Controls 2.9
import QtQuick.Controls.Material 2.9

Pane {
	padding: 0
	Material.theme: "Dark"
	width: 600
	Material.accent: Material.Green
	height: 650

	Image {
		source: "icon.svg"
		anchors.verticalCenter: parent.verticalCenter
		anchors.horizontalCenter: parent.horizontalCenter
	}

	BusyIndicator {
		id: x
		anchors.verticalCenter: parent.verticalCenter
		anchors.horizontalCenter: parent.horizontalCenter
		implicitWidth: 150
		implicitHeight: 150
	}

	Label {
		anchors.top: x.bottom
		anchors.topMargin: 20
		anchors.horizontalCenter: parent.horizontalCenter
		text: "Abstract Games is Loading..."
	}
}
