import QtQuick.Controls 2.9
import QtQuick.Controls.Material 2.9
import QtQuick 2.0

TabBar {
	Material.theme: "Dark"
	Material.accent: Material.Green
	height: 50
	width: 600
	id: t
	signal choosen(int index)
	property var add : function(name, enabled = true, icon = "") { 
		r.model.append({"text": name, "enabled": enabled, "icon": icon}); 
		currentIndex = find(name);
	}
	property var clear : function() {
		if (model.count != 0 && model.count != 1)
		{
			model.remove(1, model.count-1);
		}
	}

	QtObject {
		property var index: parent ? parent.currentIndex : 0

		Behavior on index {
			ScriptAction {
				script: 
				{
					t.choosen(index)
				}
			}
		}
	}

	Repeater {
		id: r
		model: ListModel { id: model; }

		TabButton {
			checkable: true
			enabled: index != -1 ? r.model.get(index).enabled : false
			icon.source:  index != -1 ? r.model.get(index).icon == "" ? 
					r.model.get(index).text.toLowerCase() + ".svg" : 
					r.model.get(index).icon : ""
			icon.width: 25
			icon.color: index != -1 ? r.model.get(index).enabled ? "transparent" : "grey" : "" 

			onClicked: {
				t.choosen(index)
			}

			Keys.onPressed: {
				if (event.key == Qt.Key_Right)
				{
					if (index + 1 >= parent.children.length) return;

					var inc = 1;
					if (!parent.children[index + 1].enabled)
					{
						inc++;
						t.currentIndex += 1;
					}

					t.choosen(index + inc);
				}
				else if (event.key == Qt.Key_Left)
				{
					if (index - 1 < 0) return;

					var inc = 1;

					if (!parent.children[index - 1].enabled) {
						inc++;
						t.currentIndex -= 1;
					}

					t.choosen(index - inc);
				}
			}
		}
	}
}
