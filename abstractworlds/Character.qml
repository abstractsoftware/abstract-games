import QtQuick 2.7

Image {
	id: mob
	property string type
	property bool hasBeenClicked
	property var move: null
	property int lives: 9

	function init() { lives = 10; hasBeenClicked = false; }

	source: {
		if (type == "Human" )
			"images/human.svg"
		else if (type == "Zombie")
			"images/zombie.svg"
                else if (type == "Skeleton")
                        "images/skeleton.svg"
                else if (type == "Enderman")
                        "images/enderman.svg"
                else if (type == "Sun")
                        "images/sun.svg"
	}

	Timer {

		running: move != null
		interval: 1
		repeat: true

		onTriggered: {
			if (gamewidget.stalled) return
			mob.move();
		}
	}

	MouseArea {
		anchors.fill: parent

		onClicked: {
			mob.hasBeenClicked = true;
		}
	}
}
