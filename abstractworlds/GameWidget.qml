import QtQuick 2.7
import QtQuick.Controls 2.7
import QtQuick.Layouts 1.7
import QtQuick.Controls.Material 2.7
import AbstractGames.Widgets 1.0
import "game.js" as Game

SimpleGameWidget {
	id: gamewidget

        // stone, water, wood, earth, grass, ice, sand
	property var mapGen: ["stone", "wood", "earth", "grass", "ice", "sand", "leaves", "water", "sky"]
        property var blocktypes: ["#303031", "#8d7044", "#35241d", "#5d971a","#d8daff", "#cca98a", "#2d5428", "#3c5e7c", "skyblue"]
	property var extrablocks: [""]

	property var currentlandscape: 0
	property var landscapes: []
	property var landscapePos: []

	property var isNight: false
	property var nB: 8
	property bool lock: false
	property var blocks
	property var leafBlockPattern

        property var timeOfDay: 0
	property var endColors: ["#8bb9f9", "#8bb9f9", "#8bb9f9", "#7ea9e2", "#6e94c6", "#5f80ab", "#4b6587", "#374a63", "#29374a", "#19212d", "#19212d", "#19212d", "#374a63", "#5f80ab"]

	property var world
	property var xPos
	property var yPos
	property var yPi
	property var blockPosY
	property var zPos

        property var xView: 500
        property var yView: 500
        property var zView: 0

        property var xSize: 63000
        property var ySize: 5000

	property var stalled: false
	property int currentBlock: 0
	property var characters

	property var yH

	onGameRestarted: {
		Game.Restart();
	}

	Row
	{
		z: 200
		id: selector
		anchors.bottom: parent.bottom
		anchors.bottomMargin: 5
		anchors.left: parent.left
		anchors.leftMargin: 5
		anchors.right: parent.right
		anchors.rightMargin: 5
		height: 50

		Repeater {
			model: 7

			Rectangle {
				border { width: 0.5 }

				height: selector.height
				width: selector.width / 7
				color: index == currentBlock ? Qt.darker(blocktypes[index], 1.25) : blocktypes[index]

				MouseArea {
					anchors.fill: parent

					onClicked: {
						currentBlock = index;
					}
				}
			}
		}
	}

	Character {
		id: you
		visible: false
		z: 200
		width: 60
		height: 150
		x: 10
		y: 136
		type: "Human"
	}

	Rectangle {
		id: yourBlock
		border {
			width: 1
		}

		z: 200
		width: 50
		height: 50
		x: mouse.mouseX - yourBlock.width*0.5//you.x
		y: mouse.mouseY - yourBlock.height*0.5//you.y + you.height - 50
		color: blocktypes[currentBlock]
	}

	Canvas {
		x: 0
		y: 0
		visible: true
		id: canvas
		width: 500
		height: 500
		focus: true

		MouseArea { anchors.fill: parent; id: mouse; hoverEnabled: true }

      Keys.onPressed: {
          if (event.key == Qt.Key_Escape) {
              options.visible = !options.visible
                gamewidget.stalled = options.visible
                selector.visible = !gamewidget.stalled
                yourBlock.visible = !gamewidget.stalled
                //navigator.visible = !gamewidget.stalled

                layout.z = 6000
                if (!gamewidget.stalled) focus = true
              event.accepted = true;
          } else if (event.key == Qt.Key_Space) {
                Game.MakeMove("jump");
          } else if (event.key == Qt.Key_W) {
                Game.MakeMove("right")
          } else if (event.key == Qt.Key_S) {
                Game.MakeMove("left")
          }
      }

	Label {
		id: where
		width: 500;
		height: 100
		visible: false
		text: you.lives
	}

	Rectangle {
		id: options

		z: 5000
		anchors.fill: parent
		visible: false
		focus: visible
		color: "#000000"
		opacity: 0.75
	}

		ColumnLayout {
			id: layout

			anchors.fill: parent
                anchors.leftMargin: 10
                anchors.topMargin: 10
			z: 6000
			Material.theme: Material.Dark

			visible: options.visible
			Label {
				text: "General Options"
				font.pointSize: 14
                                font.family: "mono"
			}

			Switch {
				id: creativeMode
	
				checkable: true
				checked: true		
				text: "Creative Mode"
                                font.family: "mono"
			}

			Label {
				text: "Mobs: NOT IMPLEMENTED"
                                font.pointSize: 14
				font.family: "mono"
			}

			CheckBox { id: doZombie; text: "Zombies"; font.family: "mono" }
			CheckBox { id: doSkeleton; text: "Skeletons"; font.family: "mono"}
			CheckBox { id: doEnderman; text: "Endermen"; font.family: "mono"}

			Item {
				Layout.fillHeight: true
			}
		}

        Rectangle {
                id: sun
                visible: false
		color: "yellow";
                width: 75
                height: 75
                x: 250
                y: 50
        }


      Timer {
		id: timer
               interval: 10000; running: false; repeat: true
               onTriggered: {
                var numColor = endColors[timeOfDay]

		if (gamewidget.timeOfDay < 7) {
			isNight = false
			gamewidget.blocktypes[nB] = numColor
			timeOfDay++
			sun.y += 50
			where.color = "black"
		} else {
			isNight = true
			gamewidget.blocktypes[nB] = numColor
			timeOfDay++

			where.color = "white"
			sun.y -= 50
			if (timeOfDay == 14) timeOfDay = 0
		}

		canvas.requestPaint()
	  }
      }

		onPaint: {
			if (world == null) return;

			focus = true
			var context = getContext("2d");

                        var iP = 0;
			var jP = 0;

			for (var i = gamewidget.xPos; i < gamewidget.xPos + gamewidget.xView; i += 50)
		        {
				jP = 0;
		                for (var j = gamewidget.yPos; j < gamewidget.yPos + gamewidget.yView; j += 50)
		       	        {
			              for (var k = gamewidget.zPos; k < 1; k += 50)
				      {
						var a = world[i][j][k];
						context.fillStyle = gamewidget.blocktypes[a];
						context.fillRect(iP, jP, 50, 50);
						if (a < nB-1) {
							for (var ooo = 0; ooo < 50; ooo += 5) {
								for (var aaa = 0; aaa < 50; aaa += 5) {
									context.fillStyle = Qt.darker(gamewidget.blocktypes[world[i][j][k]], blocks[aaa/5][ooo/5]);
									if (a == 3 && aaa > 5) context.fillStyle = Qt.darker(gamewidget.blocktypes[2], blocks[aaa/5][ooo/5])
									if (leafBlockPattern[ooo/5][aaa/5] == -1 && a == nB-2) context.fillStyle = gamewidget.blocktypes[nB];
									context.fillRect(iP+ooo, jP+aaa, 5, 5);
                                                		}
							}
						}
			              }

                                      jP += 50;
			        }

				iP += 50;
		        }

			//where.text = gamewidget.xPos;
		}

		MouseArea
		{
			anchors.fill: parent
			acceptedButtons: Qt.LeftButton | Qt.RightButton

			onClicked:
			{

				if (stalled) return;

				var _x = xPos + Math.floor(mouse.x / 50) * 50;
				var _y = yPos + Math.floor(mouse.y / 50) * 50;

				if (mouse.button == undefined || mouse.button == Qt.RightButton)
				{
					if (world[_x][_y][0] == nB || world[_x][_y][0] == nB-1)
					{
						try {
							if (world[_x][_y + 50][0] != nB || world[_x + 50][_y][0] != nB || world[_x - 50][_y][0] != nB)
							{
								world[_x][_y][0] = currentBlock;
							}
							else
							{
								var y2 = _y;
								var b = true;
		
								for (;; y2 += 50)
								{
									//for (var i = 0; i < 1000; i++) gamewidget.update();
		
									//gamewidget.update();
	
									if (world[_x][y2][0] != nB) break;
									if (!b) world[_x][y2 - 50][0] = nB;
		
									world[_x][y2][0] = currentBlock;

									canvas.requestPaint();
									b = false;

									for (var i = 0; i < 1000; i++) gamewidget.update();
								}
							}
						}
						catch (all)
						{
							world[_x][_y][0] = currentBlock;
						}
					}
				}
				else if (world[_x][_y][0] != nB-1)
				{
					world[_x][_y][0] = nB;
					if (world[_x-50][_y][0] == nB-1 || world[_x+50][_y][0] == nB-1 ||
						world[_x][_y-50][0] == nB-1 /*|| world[_x][_y+50][0] == nB-1 ||
						world[_x+50][_y-50][0] == nB-1 || world[_x+50][_y+50][0] == nB-1*/) {
						world[_x][_y][0] = nB-1;
					}
				}

				canvas.requestPaint();
			}
		}
	}

	Rectangle
	{
		visible: false
		id: navigator
		width: 100
		height: 75
		color: "#607D8B"
		opacity: 0.5
		x: 100
		y: 100
		z: 100

		MouseArea
		{
			anchors.fill: parent
			drag.target: parent
		}

		ToolButton
		{
			anchors.left: parent.left
			anchors.verticalCenter: parent.verticalCenter

			text: "<"
			font.family: "Ubuntu"
			font.pixelSize: 25
			//color: "black"

			onClicked:
			{
				Game.MakeMove("left");
			}
		}

		ToolButton
		{
			anchors.right: parent.right
			anchors.verticalCenter: parent.verticalCenter

			text: ">"
			font.family: "Ubuntu"
			font.pixelSize: 25
			//color: "black"
			//MouseArea
			//{
			//	anchors.fill: parent

			onClicked:
			{
				Game.MakeMove("right");
			}
		}

                /*ToolButton
                {
                        anchors.top: parent.top
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: "<"
                        font.family: "Ubuntu"
                        rotation: 90
                        font.pixelSize: 25

                                onClicked:
                                {
                                        Game.MakeMove("up");
                                }
                }

                ToolButton
                {
                        anchors.bottom: parent.bottom
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: ">"
                        font.family: "Ubuntu"
                        rotation: 90
                        font.pixelSize: 25

                                onClicked:
                                {
                                        Game.MakeMove("down");
                                }
                }*/
	}
}
