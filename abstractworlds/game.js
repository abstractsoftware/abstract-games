/*
	Core game functions.
*/

function findXPos(character)
{
	if (character != you)
	{
		return gamewidget.xPos + Math.floor((character.x*0.027)-0.27)*50;
	}
	else
	{
		return gamewidget.xPos;
	}
}

function GenerateDefaultBlock()
{
	gamewidget.characters = new Array();
        var landscape = Math.floor(Math.random() * 3)
        var landscapeBlockName = "water";
	var landscapeName = "Nowhere!?";

        if (landscape == 0) landscapeBlockName = "sand", landscapeName = "Desert";
        else if (landscape == 1) landscapeBlockName = "ice", landscapeName = "Ice Plains";
        else if (landscape == 2) landscapeBlockName = "grass", landscapeName = "Savanna";

	gamewidget.landscapes.push(landscapeName)

	return landscapeBlockName;

}

function Restart()
{
	you.init();
	where.text = you.lives;

	/* later add randomized initial coordinate */
	gamewidget.xPos = gamewidget.xSize/2; //Math.floor(Math.random() * gamewidget.xSize);
	gamewidget.yPos = gamewidget.ySize - gamewidget.yView;
	gamewidget.yPi = gamewidget.yPos
	gamewidget.blockPosY = 250; //gamewidget.yPos
	gamewidget.zPos = 0;

	gamewidget.world = new Array(gamewidget.xSize);

	// generate block randomness
        gamewidget.blocks = Array(10);
        for (var i = 0; i < 10; ++i) {
            gamewidget.blocks[i] = Array(10);
            for (var j = 0; j < 10; ++j) {
                gamewidget.blocks[i][j] = (Math.random()*0.5)+1;
            }
        }

        gamewidget.leafBlockPattern = Array(10);
        for (var i = 0; i < 10; ++i) {
            gamewidget.leafBlockPattern[i] = Array(10);
            for (var j = 0; j < 10; ++j) {
                gamewidget.leafBlockPattern[i][j] = (Math.floor(Math.random() * 10) < 6) ? 0 : -1;
            }
        }

        // generate World

	var landscapeBlockName;
	var recountBlocks = 0;
	var poolSize = 0;

	var useTreeBefore, useTreeAfter, useTree
	var useShrub, useShrubBefore;
	var doHill = true, hHills = 0, invHill = false;
	gamewidget.yH = gamewidget.ySize-gamewidget.yView/2+0;
	for (var i = 0; i < gamewidget.xSize; i += 50) {
		gamewidget.world[i] = new Array(gamewidget.xSize);

		if (recountBlocks == 0) {
			landscapeBlockName = GenerateDefaultBlock();
		        gamewidget.landscapePos.push(i)
			recountBlocks = 500;
		}

		var usePool = Math.floor(Math.random() * 20) > 18
		if (usePool) poolSize = 50;
		if (poolSize > 0) usePool = true;

		if ((landscapeBlockName == "grass" || landscapeBlockName == "ice") && (!useTree && !useTreeBefore)) useTreeAfter = Math.floor(Math.random() * 50) > 48;
		else if (landscapeBlockName == "sand")  useShrub = Math.floor(Math.random() * 20) > 18;

		var variableH = 0; //Math.floor((Math.random()*10))*0;
		var height = yH + variableH;


		//if (!usePool) {
			doHill = Math.floor(Math.random() * 10) > 8;
		//}

		/*if (doHill) {
			hHills++;
			height -= 50*hHills
			if (hHills == 10) {
				hHills--;
				doHill = false;
				invHill = true;
			}
		}

		if (invHill) {
			hHills--;
			height += 50*hHills
			if (hHills == 0) {
				invHill = false;
			}
		}*/

		for (var j = 0; j < gamewidget.ySize; j += 50) {
                        gamewidget.world[i][j] = new Array(1);

			if (j > height) {

        	                // 1. Sea
				// 2. Desert
				// 3. Ice
				// 4. Grassland
				// (TODO add hills)

/*			        var landscape = Math.floor(Math.random() * 4)
        	                var landscapeBlockName = "water";

				if (landscape == 0) landscapeBlockName = "water";
				else if (landscape == 1) landscapeBlockName = "sand";
				else if (landscape == 2) landscapeBlockName = "ice";
				else if (landscape == 3) landscapeBlockName = "grass";
                                */

				var useMostCommonBlock = Math.floor(Math.random() * 10) < 6

				if (useMostCommonBlock) {
					if (usePool && j < height+150)
                                                gamewidget.world[i][j][0] = gamewidget.mapGen.lastIndexOf("water");
					else {
						if (landscapeBlockName == "grass" && j > height+50) {
							gamewidget.world[i][j][0] = gamewidget.mapGen.lastIndexOf("earth");
						} else {
                                                        gamewidget.world[i][j][0] = gamewidget.mapGen.lastIndexOf(landscapeBlockName);
						}
					}
				} else {
					var a = Math.floor(Math.random() * (gamewidget.blocktypes.length-3));
					if (landscapeBlockName == "ice" && gamewidget.mapGen[a] == "sand") a = gamewidget.mapGen.lastIndexOf("ice"); 
					else if (landscapeBlockName == "sand" && gamewidget.mapGen[a] == "ice") a = gamewidget.mapGen.lastIndexOf("sand");
					if (j > height+50 && a == gamewidget.mapGen.lastIndexOf("grass")) a = gamewidget.mapGen.lastIndexOf("earth");
					gamewidget.world[i][j][0] = a;
				}
			} else {
				// build a tree
				if (useTree && j > height-150)
					gamewidget.world[i][j][0] = gamewidget.mapGen.lastIndexOf("wood");
				else if ((useTree && j > height-250) || (useTreeBefore && j > height-250 && j < height-100))
					gamewidget.world[i][j][0] = gamewidget.mapGen.lastIndexOf("leaves");
                                else if ((useTreeAfter && j > height-250 && j < height-100))
                                        gamewidget.world[i][j][0] = gamewidget.mapGen.lastIndexOf("leaves");
				else if ((useShrub && j > height-100) || (useShrubBefore && j > height-100))
                                        gamewidget.world[i][j][0] = gamewidget.mapGen.lastIndexOf("leaves");
				else {
                                        gamewidget.world[i][j][0] = gamewidget.mapGen.lastIndexOf("sky");
				}
			}
		}

		recountBlocks -= 10;
		poolSize -= 10

                if (useTree) useTreeBefore = true;
                else useTreeBefore = false

		if (useTreeAfter) useTree = true;
		else useTree = false;

		useTreeAfter = false;
		if (useShrub) useShrubBefore = true;
                else useShrubBefore = false;

		useShrub = false;
	}

	timer.running = true
	gamewidget.blocktypes[nB] = gamewidget.endColors[0]
        canvas.requestPaint();

	upSquare1();
	jumpToBottom(you);
}

function MakeMove(where)
{
       var xPos = gamewidget.xPos, yPos = gamewidget.yPos;

	if (where == "left") {
		xPos -= 50
                yPos = upSquare(you, -2);
	} else if (where == "right") {
		xPos += 50
		yPos = upSquare(you, 2);
	} else if (where == "up") {
		yPos -= 50
	} else if (where == "down") {
                yPos += 50
	} else if (where == "jump") {
		MakeMove("up");
		return;
	}

	if (yPos == -1) return;
        if (xPos < 0 || xPos + gamewidget.xView > gamewidget.xSize) return; 
        if (yPos < 0 || yPos + gamewidget.yView > gamewidget.ySize) return;

	gamewidget.xPos = xPos;
	gamewidget.yPos = yPos;

	canvas.requestPaint();

	jumpToBottom(you);
	makeNewCharacter();
	upSquare1();
}

function upSquare(character, w = 1)
{
	//if (gamewidget.world[xPos+w*50][yPos][0] != nB && gamewidget.world[xPos+w*50][yPos-50][0] != nB) return -1;
	//
	var _x = findXPos(character);

        var nnn = 0
        var waterFoundAtPos = -1, airFoundAtPos = -1, blocksFoundAfterAirAtPos = -1;
        for (nnn = gamewidget.ySize-50; nnn > gamewidget.yPos-(yPi-yH)-100; nnn -= 50) {
                if (gamewidget.world[_x+w*50][nnn][0] == gamewidget.nB && airFoundAtPos == -1) airFoundAtPos = nnn;
                else if (gamewidget.world[_x+w*50][nnn][0] == gamewidget.nB-1) waterFoundAtPos = nnn;
	        else {
			waterFoundAtPos = -1;
			if (airFoundAtPos != -1) blocksFoundAfterAirAtPos = nnn;
		}
        }

	if (airFoundAtPos > yH) return gamewidget.yPos;
        //if (waterFoundAtPos != -1) gamewidget.blockPosY = 240+50;
        //else gamewidget.blockPosY = 240;
	//if (blocksFoundAfterAirAtPos != -1 && blocksFoundAfterAirAtPos+(yPi-yH) == yH-100) nnn = blocksFoundAfterAirAtPos+(yPi-yH);
	else nnn = airFoundAtPos + (yPi-yH);
        if (nnn < gamewidget.yPos-50) return -1;
	return nnn;

	return gamewidget.yPos;
}

var inWater = false, depth = 0

function upSquare1()
{
/*        var nnn = 0
	var waterFoundTimes = 0
        var waterFoundAtPos = -1, airFoundAtPos = -1;
        for (nnn = gamewidget.ySize-50; nnn > 0; nnn -= 50) {
                if (gamewidget.world[xPos+50][nnn][0] == gamewidget.nB) break;
                else if (gamewidget.world[xPos+50][nnn][0] == gamewidget.nB-1) {
			waterFoundAtPos = nnn;
		} else waterFoundAtPos = -1;
        }

        //for (nnn = yH; nnn < gamewidget.ySize-50; nnn += 50) {
        //        if (gamewidget.world[xPos+50][nnn][0] == gamewidget.nB-1) {
        //                waterFoundTimes++;
        //                if (depth == waterFoundTimes-1) waterFoundAtPos = nnn;
        //        } else waterFoundAtPos = -1;
        //}

        if (waterFoundAtPos != -1) gamewidget.blockPosY = 255+50;
        else depth = 0, gamewidget.blockPosY = 255-(yH-nnn);*/
}

function jumpToBottom(character)
{
	var y2 = 4500;//gamewidget.yPos;
	var b = true;

	var _x = findXPos(character);

	for (;; y2 += 50)
	{
		if (world[_x][y2][0] != gamewidget.nB && world[_x][y2][0] != gamewidget.nB - 1) break;
	}

	var vat = (((y2 - 4500) / 50) * 56) - 200;

	character.y = (((y2 - 4500) / 50) * 56) - 200;

	canvas.requestPaint();

	return vat;
}

function makeNewCharacter()
{
	if (creativeMode.checked) return;

	if (Math.floor(Math.random()*15) == 0)
	{
		var character, component;
		var x = 500;
		var y = Math.random() * 500;

		var attacks = 0, attacking = false;

                var aChar = Math.floor(Math.random() * 3);
		var sChar = "Skeleton";
		if (aChar == 1) sChar = "Zombie";
		else if (aChar == 2) sChar = "Enderman";

		if (!gamewidget.isNight) sChar = "Enderman";
		if (sChar == "Zombie") attacking = true;

		function move()
		{
			//for (var i = 0; i < gamewidget.characters.length; i++)
			//{
			//	if (gamewidget.characters[i] != character)
			//	{
			//		if (gamewidget.characters[i]
			//	}
			//}
			
			if (character.hasBeenClicked)
			{
				attacking = true;
				character.hasBeenClicked = false;
				character.lives -= 2;

				character.x += 50;
				you.lives += 1;

				if (character.lives <= 0)
				{
					you.lives += 10;
					character.destroy();
				}
			}

			if (attacking)
			{
				if (character.x > 75) character.x -= 1;

				if (upSquare(character, -2) != -1)
					attacks += 1;
				else
					character.x += 1
			}
			else
			{
				character.x -= 0.25;

				if (upSquare(character, -2) != -1) ;
				else character.x += 0.25;
			}

			if (attacks == 50)
			{
				where.text = "You're being attacked!";
				attacks = 0;
				you.lives -= 1;
				character.lives += 1;

				if (you.lives < 0)
				{
					if (!gamewidget.lock)
					{
						gamewidget.lock = true;
						character.destroy();
						gamewidget.update();
						gamewidget.endGame("You have died.");
						gamewidget.lock = false;
					}
					return;
				}
			}

			if (character.x < 0 || character.x > 500) character.destroy();

			character.y = jumpToBottom(character);



			where.text = you.lives;
		}

		component = Qt.createComponent("Character.qml");

		character = component.createObject(gamewidget, 
			{"x": x, "y": y, "z": 10, "width": you.width, "height": you.height, "type": sChar, "move": move});

		character.y = jumpToBottom(character);
		//gamewidget.characters.append(character);
	}
}
