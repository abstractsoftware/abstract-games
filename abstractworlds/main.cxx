#include <QApplication>
#include <QQuickView>
#include <QQmlEngine>
#include <QUrl>
#include <QIcon>

#include "../libabstractgames/core/gamewindow/gamewindow.h"
#include "../libabstractgames/widgets/gamecontroller/gamecontroller.h"
#include "../libabstractgames/widgets/simplegamewidget/simplegamewidget.h"

int main(int argc, char* argv[])
{
        qmlRegisterType<GameController>("AbstractGames.Widgets", 1, 0, "GameController");
        qmlRegisterType<SimpleGameWidget>("AbstractGames.Widgets", 1, 0, "SimpleGameWidget");

        QApplication* app = new QApplication(argc, argv);
        GameWindow* win = new GameWindow("AbstractWorlds");

	Q_UNUSED(win);

        return app->exec();
}
