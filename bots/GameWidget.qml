import QtQuick 2.7
import QtQuick.Controls 2.1
import AbstractGames.Widgets 1.0
import "game.js" as Game

SimpleGameWidget
{
	id: gamewidget
	property var pause: false
	property bool lock: false
	property var numbots: difficulty == "easy" ? 10 : difficulty == "medium" ? 20 : 30

	onGameRestarted: {
		Game.Restart();
	}

	Item
	{
		id: robots
		Repeater
		{
			model: gamewidget.numbots

			Image
			{
				id: bot
				property bool killed: false
				property string text: killed ? "*" : "+"
				source: killed ? "images/junkheap.png" : "images/enemy.png"
				width: 32
				height: 32

				Behavior on x
				{
					id: a
					NumberAnimation
					{
						duration: 1000
					}
				}

				Behavior on y
				{
					id: b
					NumberAnimation
                    {
                    	duration: 1000
                    }
                }

			}
		}
	}

	Label
	{
		visible: false
		id: w
		text: "@"
		font.family: "Ubuntu"
		color: "black"
		font.pixelSize: 20
		background: Rectangle { color: "white" }
		width: 32
		height: 32

		Behavior on x 
		{
			NumberAnimation
			{
				duration: 1000
			}
		}

		Behavior on y
		{
			NumberAnimation
			{
				duration: 1000
			}
		}
	}

	Image
	{
		source: "images/hero.png"
		x: w.x
		y: w.y
		width: 32
		height: 32
	}

	Rectangle
	{
		width: 100
		height: 100
		color: "#607D8B"
		opacity: 0.5
		x: 100
		y: 100

		MouseArea
		{
			anchors.fill: parent
			drag.target: parent
		}

		Text
		{
			anchors.left: parent.left
			anchors.leftMargin: 10
			anchors.verticalCenter: parent.verticalCenter
			text: "<"
			font.family: "Ubuntu"
			font.pixelSize: 25
			color: "black"

			MouseArea
			{
				anchors.fill: parent

				onClicked:
				{
					Game.MakeMove("left");
				}
			}
		}

		Text
		{
			anchors.right: parent.right
			anchors.rightMargin: 10
			anchors.verticalCenter: parent.verticalCenter
			text: ">"
			font.family: "Ubuntu"
			font.pixelSize: 25
			color: "black"
			MouseArea
			{
				anchors.fill: parent

				onClicked:
				{
					Game.MakeMove("right");
				}
			}
		}

		Text
		{
			anchors.top: parent.top
			anchors.horizontalCenter: parent.horizontalCenter
			text: "<"
			font.family: "Ubuntu"
			rotation: 90
			font.pixelSize: 25
                    
			MouseArea
			{
				anchors.fill: parent
				
				onClicked:
				{
					Game.MakeMove("up");
				}
			}
		}

		Text
		{
			anchors.bottom: parent.bottom
			anchors.horizontalCenter: parent.horizontalCenter
			text: ">"
			font.family: "Ubuntu"
			rotation: 90
			font.pixelSize: 25
			color: "black"
                    
			MouseArea
			{
				anchors.fill: parent

				onClicked:
				{
					Game.MakeMove("down");
				}
			}
		}

		Text
		{
			anchors.horizontalCenter: parent.horizontalCenter
			anchors.verticalCenter: parent.verticalCenter
			text: "T"
			font.pixelSize: 25
			font.family: "Ubuntu"
			color: "black"

			MouseArea
			{
				anchors.fill: parent

				onClicked:
				{
					Game.MakeMove("teleport");
				}
			}
		}
	}
}
