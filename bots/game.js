.import AbstractGames.Logic 1.0 as Logic

function Restart()
{
	w.x = Math.floor(Math.random() * 20) * (gamewidget.width / 20);
	w.y = Math.floor(Math.random() * 20) * (gamewidget.height / 20);

	for (var i = 0; i < gamewidget.numbots; i++)
	{
		robots.children[i].x = Math.floor(Math.random() * 20) * (gamewidget.width / 20);
		robots.children[i].y = Math.floor(Math.random() * 20) * (gamewidget.height / 20);
		robots.children[i].killed = false;
	}
}

function MoveRobots()
{
	for (var i = 0; i < gamewidget.numbots; i++)
	{
		if (robots.children[i].killed) continue;

		if (robots.children[i].y > w.y)
		{
			robots.children[i].y -= 20;
		}

		if (robots.children[i].y < w.y)
		{
			robots.children[i].y += 20;
		}

		if (robots.children[i].x > w.x)
		{
			robots.children[i].x -= 20;
		}

		if (robots.children[i].x < w.x)
		{
			robots.children[i].x += 20;
		}
	}
}

function MakeMove(whichway)
{
	if (gamewidget.lock) return;

	var realx = w.x;
	var realy = w.y;

	if (whichway == "up")
	{
		if (w.y - 20 < 0) return;
		w.y -= 20;
		realy -= 20;
	}

	if (whichway == "down")
	{
		if (w.y + 20 > gamewidget.height) return;
		w.y += 20;
		realy += 20;
	}

	if (whichway == "left")
	{
		if (w.x - 20 < 0) return;
		w.x -= 20;
		realx -= 20;
	}

	if (whichway == "right")
	{
		if (w.x + 20 > gamewidget.width) return;
		w.x += 20;
		realx += 20;
	}

	if (whichway == "teleport")
	{
		realx = Math.floor(Math.random() * 20) * (gamewidget.width / 20);
		realy = Math.floor(Math.random() * 20) * (gamewidget.height / 20);
		w.x = realx;
		w.y = realy;
	}

	MoveRobots();

	while (true)
	{
		gamewidget.update();
		if (w.x == realx && w.y == realy) break;
	}

	for (var i = 0; i < gamewidget.numbots; i++)
	{
		if (robots.children[i].killed) continue;

		for (var j = 0; j < gamewidget.numbots; j++)
		{
			if (i == j) continue;

			if (Logic.Collisions.isColliding(robots.children[i], robots.children[j]))
			{
				robots.children[i].killed = true;
				robots.children[j].killed = true;

				robots.children[i].x = robots.children[j].x;
				robots.children[i].y = robots.children[j].y;
				
				var all = 0;

				for (var k = 0; k < gamewidget.numbots; k++)
				{
					if (!robots.children[k].killed) all++;
				}

				if (all == 0)
				{
					gamewidget.endGame("You win");
					return;
				}
			}
		}
	}

	for (var i = 0; i < gamewidget.numbots; i++)
	{
		if (robots.children[i].killed) continue;

		if (Logic.Collisions.isColliding(robots.children[i], w))
                {
                        gamewidget.endGame("You lose");
                        return;
                }
	}
}
