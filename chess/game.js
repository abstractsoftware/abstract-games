/*

	Small utility functions.
*/

function alphaBetaMax(alpha, beta, depth)
{
	gamewidget.update();
	var move;
	var score;

	setplayer(true)

	if (depth == 0) return [evaluate(), undefined];

	var moves = allmoves();

	for (var i = 0; i < moves.length; i++)
	{
		gamewidget.update();
		var piece, otherpiece, syscolor, _color;

		var piece = board.children[moves[i][0]].children[0].text;
		var otherpiece = board.children[moves[i][1]].children[0].text;
		var syscolor = board.children[moves[i][0]].children[0].color.toString();
		var _color = board.children[moves[i][1]].children[0].color.toString();

		board.children[moves[i][0]].children[0].text = "";
		board.children[moves[i][1]].children[0].text = piece;
		board.children[moves[i][1]].children[0].color = syscolor;

		score = alphaBetaMin(alpha, beta, depth - 1)[0];
		setplayer(true);

		board.children[moves[i][0]].children[0].text = piece;
		board.children[moves[i][1]].children[0].color = _color;
		board.children[moves[i][1]].children[0].text = otherpiece;

		if (score >= beta)
			return [beta, moves[i]];

		if (score > alpha)
		{
			alpha = score;
			move = moves[i];
		}

	}

	return [alpha, move];
}

function alphaBetaMin(alpha, beta, depth)
{
	gamewidget.update();
	var move;
	var score;

	setplayer(false)

	if (depth == 0) return [-evaluate(), undefined];

	var moves = allmoves();

	for (var i = 0; i < moves.length; i++)
	{
		gamewidget.update();
		var piece, otherpiece, syscolor, _color;

		piece = board.children[moves[i][0]].children[0].text;
		otherpiece = board.children[moves[i][1]].children[0].text;
		syscolor = board.children[moves[i][0]].children[0].color.toString();
		_color = board.children[moves[i][1]].children[0].color.toString();

		board.children[moves[i][0]].children[0].text = "";
		board.children[moves[i][1]].children[0].text = piece;
		board.children[moves[i][1]].children[0].color = syscolor;

		score = alphaBetaMax(alpha, beta, depth - 1)[0];
		setplayer(false);

		board.children[moves[i][0]].children[0].text = piece;
		board.children[moves[i][1]].children[0].color = _color;
		board.children[moves[i][1]].children[0].text = otherpiece;

		if (score <= alpha)
			return [alpha, moves[i]];

		if (score < beta)
		{
			beta = score;
			move = moves[i];
		}
	}

	return [beta, move];
}

function search()
{
	gamewidget.update();
	if (comesfirst())
		return alphaBetaMax(-1000, +1000, 2)[1];
	else
		return alphaBetaMin(-1000, +1000, 2)[1];
}

function comesfirst()
{
	if (board.turn == "#ffffff") return true
	return false;
}

function setplayer(first)
{
	if (first)
	{
		board.turn = "#ffffff";
		return;
	}

	board.turn = "#000000"; 
}

function num(array, entry)
{
	var n = 0;
	for (var i = 0; i < array.length; i++)
	{
		if (array[i] == entry) n++;
	}

	return n;
}

function evaluate()
{
	var n_adj = [ -20, -16, -12, -8, -4,  0,  4,  8, 12];
	var r_adj = [  15,  12,   9,  6,  3,  0, -3, -6, -9];

	var positionalThemes_white = 0;
	var positionalThemes_black = 0;

	var adjustMaterial_white = 0;
	var adjustMaterial_black = 0;

	var blockages_white = 0;
	var blockages_black = 0;

	var SafetyTable = [
     0,  0,   1,   2,   3,   5,   7,   9,  12,  15,
    18,  22,  26,  30,  35,  39,  44,  50,  56,  62,
    68,  75,  82,  85,  89,  97, 105, 113, 122, 131,
    140, 150, 169, 180, 191, 202, 213, 225, 237, 248,
    260, 272, 283, 295, 307, 319, 330, 342, 354, 366,
    377, 389, 401, 412, 424, 436, 448, 459, 471, 483,
    494, 500, 500, 500, 500, 500, 500, 500, 500, 500,
    500, 500, 500, 500, 500, 500, 500, 500, 500, 500,
    500, 500, 500, 500, 500, 500, 500, 500, 500, 500,
    500, 500, 500, 500, 500, 500, 500, 500, 500, 500
	];

	var index_white = [
	    7, 7 + 8, 7 + 8 * 2, 7 + 8 * 3, 7 + 8 * 4, 7 + 8 * 5, 7 + 8 * 6, 7 + 8 * 7,
	    6, 6 + 8, 6 + 8 * 2, 6 + 8 * 3, 6 + 8 * 4, 6 + 8 * 5, 6 + 8 * 6, 6 + 8 * 7,
	    5, 5 + 8, 5 + 8 * 2, 5 + 8 * 3, 5 + 8 * 4, 5 + 8 * 5, 5 + 8 * 6, 5 + 8 * 7,
	    4, 4 + 8, 4 + 8 * 2, 4 + 8 * 3, 4 + 8 * 4, 4 + 8 * 5, 4 + 8 * 6, 4 + 8 * 7,
	    3, 3 + 8, 3 + 8 * 2, 3 + 8 * 3, 3 + 8 * 4, 3 + 8 * 5, 3 + 8 * 6, 3 + 8 * 7,
	    2, 2 + 8, 2 + 8 * 2, 2 + 8 * 3, 2 + 8 * 4, 2 + 8 * 5, 2 + 8 * 6, 2 + 8 * 7,
	    1, 1 + 8, 1 + 8 * 2, 1 + 8 * 3, 1 + 8 * 4, 1 + 8 * 5, 1 + 8 * 6, 1 + 8 * 7,
	    0, 0 + 8, 0 + 8 * 2, 0 + 8 * 3, 0 + 8 * 4, 0 + 8 * 5, 0 + 8 * 6, 0 + 8 * 7,
	];

	var index_black = [
	    0, 0 + 8, 0 + 8 * 2, 0 + 8 * 3, 0 + 8 * 4, 0 + 8 * 5, 0 + 8 * 6, 0 + 8 * 7,
	    1, 1 + 8, 1 + 8 * 2, 1 + 8 * 3, 1 + 8 * 4, 1 + 8 * 5, 1 + 8 * 6, 1 + 8 * 7,
	    2, 2 + 8, 2 + 8 * 2, 2 + 8 * 3, 2 + 8 * 4, 2 + 8 * 5, 2 + 8 * 6, 2 + 8 * 7,
	    3, 3 + 8, 3 + 8 * 2, 3 + 8 * 3, 3 + 8 * 4, 3 + 8 * 5, 3 + 8 * 6, 3 + 8 * 7,
	    4, 4 + 8, 4 + 8 * 2, 4 + 8 * 3, 4 + 8 * 4, 4 + 8 * 5, 4 + 8 * 6, 4 + 8 * 7,
	    5, 5 + 8, 5 + 8 * 2, 5 + 8 * 3, 5 + 8 * 4, 5 + 8 * 5, 5 + 8 * 6, 5 + 8 * 7,
	    6, 6 + 8, 6 + 8 * 2, 6 + 8 * 3, 6 + 8 * 4, 6 + 8 * 5, 6 + 8 * 6, 6 + 8 * 7,
	    7, 7 + 8, 7 + 8 * 2, 7 + 8 * 3, 7 + 8 * 4, 7 + 8 * 5, 7 + 8 * 6, 7 + 8 * 7,
	];

	var pawn_pcsq_mg = [
	     0,   0,   0,   0,   0,   0,   0,   0,
	    -6,  -4,   1,   1,   1,   1,  -4,  -6,
	    -6,  -4,   1,   2,   2,   1,  -4,  -6,
	    -6,  -4,   2,   8,   8,   2,  -4,  -6,
	    -6,  -4,   5,  10,  10,   5,  -4,  -6,
	    -4,  -4,   1,   5,   5,   1,  -4,  -4,
	    -6,  -4,   1, -24,  -24,  1,  -4,  -6,
	     0,   0,   0,   0,   0,   0,   0,   0
	];

	var pawn_pcsq_eg = [
	     0,   0,   0,   0,   0,   0,   0,   0,
	    -6,  -4,   1,   1,   1,   1,  -4,  -6,
	    -6,  -4,   1,   2,   2,   1,  -4,  -6,
	    -6,  -4,   2,   8,   8,   2,  -4,  -6,
	    -6,  -4,   5,  10,  10,   5,  -4,  -6,
	    -4,  -4,   1,   5,   5,   1,  -4,  -4,
	    -6,  -4,   1, -24,  -24,  1,  -4,  -6,
	     0,   0,   0,   0,   0,   0,   0,   0
	];

	var knight_pcsq_mg = [
	    -8,  -8,  -8,  -8,  -8,  -8,  -8,  -8,
	    -8,   0,   0,   0,   0,   0,   0,  -8,
	    -8,   0,   4,   6,   6,   4,   0,  -8,
	    -8,   0,   6,   8,   8,   6,   0,  -8,
	    -8,   0,   6,   8,   8,   6,   0,  -8,
	    -8,   0,   4,   6,   6,   4,   0,  -8,
	    -8,   0,   1,   2,   2,   1,   0,  -8,
	   -16, -12,  -8,  -8,  -8,  -8, -12,  -16
	];

	var knight_pcsq_eg = [
	    -8,  -8,  -8,  -8,  -8,  -8,  -8,  -8,
	    -8,   0,   0,   0,   0,   0,   0,  -8,
	    -8,   0,   4,   6,   6,   4,   0,  -8,
	    -8,   0,   6,   8,   8,   6,   0,  -8,
	    -8,   0,   6,   8,   8,   6,   0,  -8,
	    -8,   0,   4,   6,   6,   4,   0,  -8,
	    -8,   0,   1,   2,   2,   1,   0,  -8,
	   -16, -12,  -8,  -8,  -8,  -8, -12,  -16
	];

	var bishop_pcsq_mg = [
	    -4,  -4,  -4,  -4,  -4,  -4,  -4,  -4,
	    -4,   0,   0,   0,   0,   0,   0,  -4,
	    -4,   0,   2,   4,   4,   2,   0,  -4,
	    -4,   0,   4,   6,   6,   4,   0,  -4,
	    -4,   0,   4,   6,   6,   4,   0,  -4,
	    -4,   1,   2,   4,   4,   2,   1,  -4,
	    -4,   2,   1,   1,   1,   1,   2,  -4,
	    -4,  -4, -12,  -4,  -4, -12,  -4,  -4
	];

	var bishop_pcsq_eg = [
	    -4,  -4,  -4,  -4,  -4,  -4,  -4,  -4,
	    -4,   0,   0,   0,   0,   0,   0,  -4,
	    -4,   0,   2,   4,   4,   2,   0,  -4,
	    -4,   0,   4,   6,   6,   4,   0,  -4,
	    -4,   0,   4,   6,   6,   4,   0,  -4,
	    -4,   1,   2,   4,   4,   2,   1,  -4,
	    -4,   2,   1,   1,   1,   1,   2,  -4,
	    -4,  -4, -12,  -4,  -4, -12,  -4,  -4
	];

	var rook_pcsq_mg = [
	     5,   5,   5,   5,   5,   5,   5,   5,
	    -5,   0,   0,   0,   0,   0,   0,  -5,
	    -5,   0,   0,   0,   0,   0,   0,  -5,
	    -5,   0,   0,   0,   0,   0,   0,  -5,
	    -5,   0,   0,   0,   0,   0,   0,  -5,
	    -5,   0,   0,   0,   0,   0,   0,  -5,
	    -5,   0,   0,   0,   0,   0,   0,  -5,
	     0,   0,   0,   2,   2,   0,   0,   0
	];

	var rook_pcsq_eg = [
	     5,   5,   5,   5,   5,   5,   5,   5,
	    -5,   0,   0,   0,   0,   0,   0,  -5,
	    -5,   0,   0,   0,   0,   0,   0,  -5,
	    -5,   0,   0,   0,   0,   0,   0,  -5,
	    -5,   0,   0,   0,   0,   0,   0,  -5,
	    -5,   0,   0,   0,   0,   0,   0,  -5,
	    -5,   0,   0,   0,   0,   0,   0,  -5,
	     0,   0,   0,   2,   2,   0,   0,   0
	];

	var queen_pcsq_mg = [
 	     0,   0,   0,   0,   0,   0,   0,   0,
  	     0,   0,   1,   1,   1,   1,   0,   0,
       	     0,   0,   1,   2,   2,   1,   0,   0,
	     0,   0,   2,   3,   3,   2,   0,   0,
	     0,   0,   2,   3,   3,   2,   0,   0,
	     0,   0,   1,   2,   2,   1,   0,   0,
	     0,   0,   1,   1,   1,   1,   0,   0,
	    -5,  -5,  -5,  -5,  -5,  -5,  -5,  -5
	];

	var queen_pcsq_eg = [
	     0,   0,   0,   0,   0,   0,   0,   0,
	     0,   0,   1,   1,   1,   1,   0,   0,
	     0,   0,   1,   2,   2,   1,   0,   0,
	     0,   0,   2,   3,   3,   2,   0,   0,
	     0,   0,   2,   3,   3,   2,   0,   0,
	     0,   0,   1,   2,   2,   1,   0,   0,
	     0,   0,   1,   1,   1,   1,   0,   0,
	    -5,  -5,  -5,  -5,  -5,  -5,  -5,  -5
	];

	var king_pcsq_mg = [
	   -40, -30, -50, -70, -70, -50, -30, -40,
	   -30, -20, -40, -60, -60, -40, -20, -30,
	   -20, -10, -30, -50, -50, -30, -10, -20,
	   -10,   0, -20, -40, -40, -20,   0, -10,
	     0,  10, -10, -30, -30, -10,  10,   0,
	    10,  20,   0, -20, -20,   0,  20,  10,
	    30,  40,  20,   0,   0,  20,  40,  30,
 	    40,  50,  30,  10,  10,  30,  50,  40
	];

	var king_pcsq_eg = [
	   -72, -48, -36, -24, -24, -36, -48, -72,
	   -48, -24, -12,   0,   0, -12, -24, -48,
	   -36, -12,   0,  12,  12,   0, -12, -36,
	   -24,   0,  12,  24,  24,  12,   0, -24,
	   -24,   0,  12,  24,  24,  12,   0, -24,
	   -36, -12,   0,  12,  12,   0, -12, -36,
	   -48, -24, -12,   0,   0, -12, -24, -48,
	   -72, -48, -36, -24, -24, -36, -48, -72
	];

	var weak_pawn_pcsq = [
	     0,   0,   0,   0,   0,   0,   0,   0,
	   -10, -12, -14, -16, -16, -14, -12, -10,
	   -10, -12, -14, -16, -16, -14, -12, -10,
	   -10, -12, -14, -16, -16, -14, -12, -10,
	   -10, -12, -14, -16, -16, -14, -12, -10,
	   -10, -12, -14, -16, -16, -14, -12, -10,
	   -10, -12, -14, -16, -16, -14, -12, -10,
	     0,   0,   0,   0,   0,   0,   0,   0
	];

	var passed_pawn_pcsq = [
	     0,   0,   0,   0,   0,   0,   0,   0,
	   140, 140, 140, 140, 140, 140, 140, 140,
	    92,  92,  92,  92,  92,  92,  92,  92,
	    56,  56,  56,  56,  56,  56,  56,  56,
	    32,  32,  32,  32,  32,  32,  32,  32,
	    20,  20,  20,  20,  20,  20,  20,  20,
	    20,  20,  20,  20,  20,  20,  20,  20,
	     0,   0,   0,   0,   0,   0,   0,   0
	];

	var knight_adj = [-20, -16, -12, -8, -4,  0,  4,  8, 12];
	var rook_adj = [15,  12,   9,  6,  3,  0, -3, -6, -9];

	var white_mg_pcsq = 0, white_eg_pcsq = 0;
	var black_mg_pcsq = 0, black_eg_pcsq = 0;

	var mgPst_white = [new Array(64), new Array(64), new Array(64), new Array(64), new Array(64), new Array(64)];
	var mgPst_black = [new Array(64), new Array(64), new Array(64), new Array(64), new Array(64), new Array(64)];

	var egPst_white = [new Array(64), new Array(64), new Array(64), new Array(64), new Array(64), new Array(64)];
        var egPst_black = [new Array(64), new Array(64), new Array(64), new Array(64), new Array(64), new Array(64)];

	var protected_passer_white = new Array(64);
	var protected_passer_black = new Array(64);

	var weak_pawn_white = new Array(64);
	var weak_pawn_black = new Array(64);

	for (var i = 0; i < 64; i++)
	{
		weak_pawn_white[index_white[i]] = weak_pawn_pcsq[i]
	}

	for (var i = 0; i < 64; i++)
        {
                weak_pawn_black[index_black[i]] = weak_pawn_pcsq[i]
        }

	for (var i = 0; i < 64; i++)
	{
		protected_passer_white[index_white[i]] = (passed_pawn_pcsq[i] * 10) / 8;
	}

	for (var i = 0; i < 64; i++)
        {
                protected_passer_black[index_black[i]] = (passed_pawn_pcsq[i] * 10) / 8;
        }

	for (var i = 0; i < 64; i++)
	{
		mgPst_white[0][index_white[i]] = pawn_pcsq_mg[i];
	}

	for (var i = 0; i < 64; i++)
        {
                mgPst_white[1][index_white[i]] = knight_pcsq_mg[i];
        }

	for (var i = 0; i < 64; i++)
        {
                mgPst_white[2][index_white[i]] = bishop_pcsq_mg[i];
        }

	for (var i = 0; i < 64; i++)
        {
                mgPst_white[3][index_white[i]] = rook_pcsq_mg[i];
        }

	for (var i = 0; i < 64; i++)
        {
                mgPst_white[4][index_white[i]] = queen_pcsq_mg[i];
        }

	for (var i = 0; i < 64; i++)
        {
                mgPst_white[5][index_white[i]] = king_pcsq_mg[i];
        }

	/* blacks */

        for (var i = 0; i < 64; i++)
        {
                mgPst_black[0][index_black[i]] = pawn_pcsq_mg[i];
        }

        for (var i = 0; i < 64; i++)
        {
                mgPst_black[1][index_black[i]] = knight_pcsq_mg[i];
        }

        for (var i = 0; i < 64; i++)
        {
                mgPst_black[2][index_black[i]] = bishop_pcsq_mg[i];
        }

        for (var i = 0; i < 64; i++)
        {
                mgPst_black[3][index_black[i]] = rook_pcsq_mg[i];
        }

        for (var i = 0; i < 64; i++)
        {
                mgPst_black[4][index_black[i]] = queen_pcsq_mg[i];
        }

        for (var i = 0; i < 64; i++)
        {
                mgPst_black[5][index_black[i]] = king_pcsq_mg[i];
        }

	// eg

	for (var i = 0; i < 64; i++)
	{
		egPst_white[0][index_white[i]] = pawn_pcsq_mg[i];
	}

	for (var i = 0; i < 64; i++)
        {
                egPst_white[1][index_white[i]] = knight_pcsq_mg[i];
        }

	for (var i = 0; i < 64; i++)
        {
                egPst_white[2][index_white[i]] = bishop_pcsq_eg[i];
        }

	for (var i = 0; i < 64; i++)
        {
                egPst_white[3][index_white[i]] = rook_pcsq_eg[i];
        }

	for (var i = 0; i < 64; i++)
        {
                egPst_white[4][index_white[i]] = queen_pcsq_eg[i];
        }

	for (var i = 0; i < 64; i++)
        {
                egPst_white[5][index_white[i]] = king_pcsq_eg[i];
        }

	/* blacks */

        for (var i = 0; i < 64; i++)
        {
                egPst_black[0][index_black[i]] = pawn_pcsq_eg[i];
        }

        for (var i = 0; i < 64; i++)
        {
                egPst_black[1][index_black[i]] = knight_pcsq_eg[i];
        }

        for (var i = 0; i < 64; i++)
        {
                egPst_black[2][index_black[i]] = bishop_pcsq_eg[i];
        }

        for (var i = 0; i < 64; i++)
        {
                egPst_black[3][index_black[i]] = rook_pcsq_eg[i];
        }

        for (var i = 0; i < 64; i++)
        {
                egPst_black[4][index_black[i]] = queen_pcsq_eg[i];
        }

        for (var i = 0; i < 64; i++)
        {
                egPst_black[5][index_black[i]] = king_pcsq_eg[i];
        }

	var score = 0;
	var mobilitywhite = 0, mobilityblack = 0;
	var white, black;
	var mgMobWhite = 0, mgMobBlack = 0;

	var mgScore = 0, egScore = 0;

	var doubledblackpawns = 0;
	var opposedblackpawns = 0;

	var doubledwhitepawns = 0;
        var opposedwhitepawns = 0;
	
	var supportedwhitepawns = 0, weakwhitepawns = 0;
	var supportedblackpawns = 0, weakblackpawns = 0;

	var phase = 0;
	var whitepawns = 0, whiterooks = 0, whitebishops = 0, whiteknights = 0, whitequeens = 0, whitekings = 0;
	var blackpawns = 0, blackrooks = 0, blackbishops = 0, blackknights = 0, blackqueens = 0, blackkings = 0;

	var blockedwhitepawns = 0, blockedblackpawns = 0;
	var whitematerial = 0, blackmaterial = 0;

	var pawnscore = 0;
	
	for (var i = 0; i < 8*8; i++)
	{
		if (board.children[i].children[0].color == "#ffffff")
		{
			if (board.children[i].children[0].text == "pawn")
			{
				whitematerial += 1;
				white_mg_pcsq += mgPst_white[0][i];
				white_eg_pcsq += egPst_white[0][i];
				whitepawns++;

				if (issupported(i, "#ffffff"))
				{
					/*if (msg.playaiwhite)
					{
						pawnscore += protected_passer_white[i];
					}
					else
					{*/
						pawnscore -= protected_passer_white[i];
					//}
				}
				else
				{
					/*if (msg.playaiwhite)
                                        {
                                                pawnscore += weak_pawn_white[i];
                                        }
                                        else
                                        {*/
                                                pawnscore -= weak_pawn_white[i];
                                        //}
				}
			}
			else if (board.children[i].children[0].text == "rook")
			{
				whitematerial += 5;
				white_mg_pcsq += mgPst_white[3][i];
				white_eg_pcsq += egPst_white[3][i];
				whiterooks++;
			}
			else if (board.children[i].children[0].text == "bishop")
			{
				whitematerial += 3;
				white_mg_pcsq += mgPst_white[2][i];
				white_eg_pcsq += egPst_white[2][i];
				whitebishops++;
			}
			else if (board.children[i].children[0].text == "knight")
			{
				whitematerial += 3;
				white_mg_pcsq += mgPst_white[1][i];
				white_eg_pcsq += egPst_white[1][i];
				whiteknights++;
			}
			else if (board.children[i].children[0].text == "queen")
			{
				whitematerial += 9;
				white_mg_pcsq += mgPst_white[4][i];
				white_eg_pcsq += egPst_white[4][i];
				whitequeens++;
			}
			else if (board.children[i].children[0].text == "king")
			{
				whitematerial += 50;
				white_mg_pcsq += mgPst_white[5][i];
				white_eg_pcsq += egPst_white[5][i];
				whitekings++;
			}
		}
		if (board.children[i].children[0].color == "#000000")
		{
			if (board.children[i].children[0].text == "pawn")
			{
				blackmaterial += 1;
				black_mg_pcsq += mgPst_black[0][i];
				black_eg_pcsq += egPst_black[0][i];
				blackpawns++;

				if (issupported(i, "#000000"))
                                {
					/*if (msg.playaiblack)*/ pawnscore += protected_passer_black[i];
					//else pawnscore -= protected_passer_black[i];
                                }
				else
                                {
                                        //if (msg.playaiblack)
                                        //{
                                                pawnscore += weak_pawn_black[i];
                                        //}
                                        //else
                                        //{
                                        //        pawnscore -= weak_pawn_black[i];
                                        //}
                                }
			}
			if (board.children[i].children[0].text == "rook")
			{
				blackmaterial += 5;
				black_mg_pcsq += mgPst_black[3][i];
				black_eg_pcsq += egPst_black[3][i];
				blackrooks++;
			}
			if (board.children[i].children[0].text == "bishop")
			{
				blackmaterial += 3;
				black_mg_pcsq += mgPst_black[2][i];
				black_eg_pcsq += egPst_black[2][i];
				blackbishops++;
			}
			if (board.children[i].children[0].text == "knight")
			{
				blackmaterial += 3;
				black_mg_pcsq += mgPst_black[1][i];
				black_eg_pcsq += egPst_black[1][i];
				blackknights++;
			}
			if (board.children[i].children[0].text == "queens")
			{
				blackmaterial += 9;
				black_mg_pcsq += mgPst_black[4][i];
				black_eg_pcsq += egPst_black[4][i];
				blackqueens++;
			}
			if (board.children[i].children[0].text == "king")
			{
				blackmaterial += 50;
				black_mg_pcsq += mgPst_black[5][i];
				black_eg_pcsq += egPst_black[5][i];
				blackkings++;
			}
		}
	}

	var orginal = board.turn.toString();

	board.turn = "#ffffff";
	white = allmoves();
	mobilitywhite = white.length;

	board.turn = "#000000";
	black = allmoves();
	mobilityblack = black.length;

	board.turn = orginal;

	var kingmobwhite = 0, queenmobwhite = 0, pawnmobwhite = 0, rookmobwhite = 0, knightmobwhite = 0, bishopmobwhite = 0;
	var kingmobblack = 0, queenmobblack = 0, pawnmobblack = 0, rookmobblack = 0, knightmobblack = 0, bishopmobblack = 0;

	var egMobWhite, mgMobWhite;
	var egMobBlack, mgMobBlack;

	for (var i = 0; i < white.length; i++)
	{
		if (board.children[white[i][0]].children[0].text == "king") kingmobwhite++;
		else if (board.children[white[i][0]].children[0].text == "queen") queenmobwhite++;
		else if (board.children[white[i][0]].children[0].text == "pawn") pawnmobwhite++;
		else if (board.children[white[i][0]].children[0].text == "rook") rookmobwhite++;
		else if (board.children[white[i][0]].children[0].text == "bishop") bishopmobwhite++;
		else if (board.children[white[i][0]].children[0].text == "knight") knightmobwhite++;
	}

	for (var i = 0; i < black.length; i++)
        {
                if (board.children[black[i][0]].children[0].text == "king") kingmobblack++;
                else if (board.children[black[i][0]].children[0].text == "queen") queenmobblack++;
                else if (board.children[black[i][0]].children[0].text == "pawn") pawnmobblack++;
                else if (board.children[black[i][0]].children[0].text == "rook") rookmobblack++;
                else if (board.children[black[i][0]].children[0].text == "bishop") bishopmobblack++;
                else if (board.children[black[i][0]].children[0].text == "knight") knightmobblack++;
        }

	egMobWhite = 4 * (knightmobwhite - 4);
	mgMobWhite = 4 * (knightmobwhite - 4);

	egMobWhite += 3 * (bishopmobwhite - 7);
        mgMobWhite += 3 * (bishopmobwhite - 7);

	egMobWhite += 2 * (rookmobwhite - 7);
        mgMobWhite += 4 * (rookmobwhite - 7);

	egMobWhite += 1 * (queenmobwhite - 14);
        mgMobWhite += 2 * (queenmobwhite - 14);

	// black

	egMobBlack = 4 * (knightmobblack - 4);
        mgMobBlack = 4 * (knightmobblack - 4);

        egMobBlack += 3 * (bishopmobblack - 7);
        mgMobBlack += 3 * (bishopmobblack - 7);

        egMobBlack += 2 * (rookmobblack - 7);
        mgMobBlack += 4 * (rookmobblack - 7);

        egMobBlack += 1 * (queenmobblack - 14);
        mgMobBlack += 2 * (queenmobblack - 14);



	for (var i = 0; i < 8*8; i++)
	{
		if (i + 8 < 8*8)
		{
			if (board.children[i].children[0].text == "pawn" && board.children[i + 8].children[0].text == "pawn")
			{
				doubledwhitepawns++;
				if (i + 9 < 8*8)
				{
					if ((board.children[i + 9].children[0].text == "" || board.children[i + 9].children[0].color == board.turn) &&
					(board.children[i + 7].children[0].text == "" || board.children[i + 7].children[0].color == board.turn))
					{
						if (board.children[i].children[0].color == "#ffffff")
							blockedwhitepawns++;
					}

					if ((board.children[i + 9].children[0].text != "" && board.children[i + 9].children[0].color != board.turn) ||
                                        (board.children[i + 7].children[0].text != "" && board.children[i + 7].children[0].color != board.turn))
                                        {
                                                opposedwhitepawns++;
                                        }
				}
			}
		}

		if (i - 8 >= 0)
		{
			if (board.children[i].children[0].text == "pawn" && board.children[i - 8].children[0].text == "pawn")
			{
				doubledblackpawns++;
				if (i - 7 >= 0)
				{
					if ((board.children[i - 9].children[0].text == "" || board.children[i - 9].children[0].color == board.turn) &&
					(board.children[i - 7].children[0].text == "" || board.children[i - 7].children[0].color == board.turn))
					{
						if (board.children[i].children[0] == "#000000")
							blockedblackpawns++;
					}

					if ((board.children[i - 9].children[0].text != "" && board.children[i - 9].children[0].color != board.turn) ||
					(board.children[i - 7].children[0].text != "" && board.children[i - 7].children[0].color != board.turn))
					{
						opposedblackpawns++;
					}
				}	
			}
		}

		if (board.children[i].children[0].text == "pawn" && board.children[i].children[0].color == "#ffffff")
                {
                	if (issupported(i, board.children[i].children[0].color)) supportedwhitepawns++;
                	else
			{
				
				weakwhitepawns++;
			}
                }

		if (board.children[i].children[0].text == "pawn" && board.children[i].children[0].color == "#000000")
                {
                	if (issupported(i, board.children[i].children[0].color)) supportedblackpawns++;
                        else weakblackpawns++;
                }
	}

	mgScore += whitematerial + whitepawns + white_mg_pcsq - blackmaterial - blackpawns - black_mg_pcsq;
	egScore += whitematerial + whitepawns + black_eg_pcsq - blackmaterial - blackpawns - black_eg_pcsq;

	mgScore += white.length - black.length;
	egScore += white.length - black.length;

	score += 200 * (whitekings-blackkings) + 
		9 * (whitequeens - blackqueens) + 
		5 * (whiterooks - blackrooks) + 
		3 * (whitebishops - blackbishops + whiteknights - blackknights) +
		1 * (whitepawns - blackpawns) -
		0.5 * (blockedwhitepawns - blockedblackpawns) + 
		0.1 * (mobilitywhite - mobilityblack);

	if (blackbishops > 1)
	{
		adjustMaterial_black += 5;
	}
	else if (whitebishops > 1)
	{
		adjustMaterial_white += 5;
	}

	if (blackknights > 1)
	{
		adjustMaterial_black -= 5;
	}
        else if (whiteknights > 1){
		adjustMaterial_white -= 5;
	}

	if (blackrooks > 1){
		adjustMaterial_black -= 5;
	}
        else if (whiterooks > 1)
	{
		adjustMaterial_white -= 5;
	}

	adjustMaterial_black += n_adj[blackpawns] * blackknights;
	adjustMaterial_white += n_adj[whitepawns] * whiteknights;

	adjustMaterial_black += n_adj[blackpawns] * blackrooks;
        adjustMaterial_white += n_adj[whitepawns] * whiterooks;

	pawnscore += 20 * blockedwhitepawns;
        pawnscore -= 20 * blockedblackpawns;

        pawnscore += 4 * opposedwhitepawns;
        pawnscore -= 4 * opposedblackpawns;

	score += (adjustMaterial_white - adjustMaterial_black);

	for (var i = 0; i < 64; i++)
	{
		if (board.children[i].children[1].text == "king" && board.children[i].children[1].color == board.turn)
		{
			result += iskingshielded(i, board.children[i].children[1].color);
		}

		if (board.children[i].children[1].text == "king" && board.children[i].children[1].color != board.turn)
                {
                	result -= iskingshielded(i, board.children[i].children[1].color);
                }

		if (board.children[i].children[1].text == "king")
		{
			if (i + 1 < 8*8)
			{
				if (board.children[i + 1].children[1].text == "bishop" &&
					board.children[i + 1].children[1].color == board.children[i].children[1].color)
				{
					if (board.children[i].children[1].color == "#000000") blockages_white += 5;
					else blockages_black -= 5;
				}
			}

			if (i - 1 < 8*8)
                        {
                        	if (board.children[i + 1].children[1].text == "bishop" &&
                                               board.children[i + 1].children[1].color == board.children[i].children[1].color)
                                       {
                                               if (board.children[i].children[1].color == "#000000") blockages_white += 5;
                                               else blockages_black -= 5;
                                       }
                        }
		}

		if (board.children[i].children[1].text == "bishop")
		{
			if ((i + 8) + 1 < 8*8)
			{
				if (board.children[(i + 8) + 1].children[1].text == "pawn" && 
					board.children[(i + 8) + 1].children[1].color == board.children[i].children[1].color)
				{
					if (board.children[i].children[1].color == "#000000") blockages_white -= 5;
					else blockages_black -= 5;
				}

				if (board.children[(i + 8) - 1].children[1].text == "pawn" &&
					board.children[(i + 8) - 1].children[1].color == board.children[i].children[1].color)
                                       {
                                               if (board.children[i].children[1].color == "#000000") blockages_white -= 5;
                                               else blockages_black -= 5;
                                       }
			}

			if ((i - 8) - 1 >= 0)
                               {
                                       if (board.children[(i - 8) + 1].children[1].text == "pawn" &&
					board.children[(i - 8) + 1].children[1].color == board.children[i].children[1].color)
                                       {
                                               if (board.children[i].children[1].color == "#000000") blockages_white -= 5;
                                               else blockages_black -= 5;
                                       }

                                       if (board.children[(i - 8) - 1].children[1].text == "pawn" &&
					board.children[(i - 8) - 1].children[1].color == board.children[i].children[1].color)
                                       {
                                               if (board.children[i].children[1].color == "#000000") blockages_white -= 5;
                                               else blockages_black -= 5;
                                       }
                               }
		}
	}

	score += (blockages_white - blockages_black);

	mgScore += (mgMobWhite - mgMobBlack);
	egScore += (egMobWhite - mgMobBlack);

	var PawnPhase = 0
	var KnightPhase = 1
	var BishopPhase = 1
	var RookPhase = 2
	var QueenPhase = 4

	var TotalPhase = PawnPhase*16 + KnightPhase*4 + BishopPhase*4 + RookPhase*4 + QueenPhase*2;

	phase = TotalPhase;

	phase -= whitepawns * PawnPhase;
	phase -= blackpawns * PawnPhase;
	phase -= whiteknights * KnightPhase;
	phase -= blackknights * KnightPhase;
	phase -= whitebishops * BishopPhase;
	phase -= blackbishops * BishopPhase;
	phase -= whiterooks * RookPhase;
	phase -= blackrooks * RookPhase;
	phase -= whitequeens * QueenPhase;
	phase -= blackqueens * QueenPhase;

	phase = (phase * 256 + (TotalPhase / 2)) / TotalPhase;

	score += ((mgScore * (256 - phase)) + (egScore * phase)) / 256;

	score += pawnscore;

	if (!comesfirst()) score = -score;

	return score;
}

function iskingshielded(place, side)
{
	var result = 0;

	if (GetInvPos(GetPos(place)[0], GetPos(place)[1] + 1) != -1 &&
		board.children[GetInvPos(GetPos(place)[0], GetPos(place)[1] + 1)].children[1].text != "" &&
		board.children[GetInvPos(GetPos(place)[0], GetPos(place)[1] + 1)].children[1].color == side)
	{
		result += 10;
	}

	if (GetInvPos(GetPos(place)[0], GetPos(place)[1] - 1) != -1 &&
		board.children[GetInvPos(GetPos(place)[0], GetPos(place)[1] - 1)].children[1].text != "" &&
                board.children[GetInvPos(GetPos(place)[0], GetPos(place)[1] - 1)].children[1].color == side)
        {
                result += 10;
        }

	return result;
}

function issupported(place, side)
{
	if (place[0] + 1 < 8*8)
	{
		if (board.children[place + 1].children[0].color == side && board.children[place + 1].children[0].text == "pawn") return true;
	}
	if (place[0] - 1 >= 0)
	{
		if (board.children[place - 1].children[0].color == side  && board.children[place - 1].children[0].text == "pawn") return true;
	}
	return false;
}

function allmoves()
{
	var moves = new Array();
	var othercolor = board.turn.toString();
	var color = othercolor == "#ffffff" ? "#000000" : "#ffffff";

	for (var i = 0; i < 64; i++) {
		for (var j = 0; j < 64; j++) {
			if (
				CheckMove(
					[GetPos(i), GetPos(j)],
					board.children[i].children[0].text,
					board.children[i].children[0].color,
					board.children[j].children[0].text,
					board.children[j].children[0].color
				)
			)
			{
				moves.push([i, j]);
			}
			
		}
	}

	var AllMoves = new Array();

	var move;
	var piece;
	var otherpiece;
	var first = board.turn;
	var next = board.turn == "#ffffff" ? "#000000" : "#ffffff";
	
	for (var i = 0; i < moves.length; i++)
	{
		move = moves[i];//moves[Math.floor(Math.random() * moves.length)];
		piece = board.children[move[0]].children[0].text;
		otherpiece = board.children[move[1]].children[0].text;
		var syscolor = board.children[move[0]].children[0].color.toString();
		var _color = board.children[move[1]].children[0].color.toString();

		board.children[move[0]].children[0].text = "";
		board.children[move[1]].children[0].text = piece;
		board.children[move[1]].children[0].color = syscolor;

		next = board.turn == "#ffffff" ? "#000000" : "#ffffff";
		first = board.turn;

		board.turn = next;

		if (!Check(first))
		{
			board.turn = first;

			AllMoves.push(move);
		}

		board.turn = first;
		board.children[move[0]].children[0].text = piece;
		board.children[move[1]].children[0].color = _color;
		board.children[move[1]].children[0].text = otherpiece;

	}

	return AllMoves;
}

function image(text, color) {
	if (color == "#ffffff")
	{
		if (text == "pawn")
		{
			return "images/pawn_white.svg";
		}
		else if (text == "rook")
		{
			return "images/rook_white.svg";
		}
		else if (text == "knight")
		{
			return "images/knight_white.svg";
		}
		else if (text == "bishop")
		{
			return "images/bishop_white.svg";
		}
		else if (text == "king")
		{
			return "images/king_white.svg";
		}
		else if (text == "queen")
		{
			return "images/queen_white.svg";
		}
	}
	if (color == "#000000")
	{
		if (text == "pawn")
		{
			return "images/pawn_black.svg";
		}
		else if (text == "rook")
		{
			return "images/rook_black.svg";
		}
		else if (text == "knight")
		{
			return "images/knight_black.svg";
		}
		else if (text == "bishop")
		{
			return "images/bishop_black.svg";
		}
		else if (text == "king")
		{
			return "images/king_black.svg";
		}
		else if (text == "queen")
		{
			return "images/queen_black.svg";
		}
	}

	return "";
}

function GetPos(i) {
	/*
		Get the (x, y) pos from the index i.
	*/

	var row = 0;
	var column = 0;

	if (i < 8) {
		column = 0;
		row = i;
	} else if (i < 16) {
		column = 1;
		row = i - 8;
	} else if (i < 24) {
		column = 2;
		row = i - 16;
	} else if (i < 32) {
		column = 3;
		row = i - 24;
	} else if (i < 40) {
		column = 4;
		row = i - 32;
	} else if (i < 48) {
		column = 5;
		row = i - 40;
	} else if (i < 56) {
		column = 6;
		row = i - 48;
	} else if (i < 64) {
		column = 7;
		row = i - 56;
	}

	return [row, column];
}

function GetInvPos(r, c) {
	var i = 0;

	if (r >= 8 || c >= 8 || r == -1 || c == -1 || r < 0 || c < 0) return -1;

	if (c == 0) i = r;
	else if (c == 1) i = r + 8;
	else if (c == 2) i = r + 8*2;
	else if (c == 3) i = r + 8*3;
	else if (c == 4) i = r + 8*4;
	else if (c == 5) i = r + 8*5;
	else if (c == 6) i = r + 8*6;
	else if (c == 7) i = r + 8*7;
	else if (c == 8) i = r + 8*8;
	else return -1;

	return i;
}

function CheckPath(pos)
{
	if (pos[0][0] == pos[1][0])
	{
		if (pos[0][1] < pos[1][1])
		{
			for (var y = pos[0][1] + 1; y < pos[1][1]; y++)
			{
				if (board.children[GetInvPos(pos[0][0], y)].children[0].text != "")
				{
					return false;
				}
			}
		}

		else if (pos[0][1] > pos[1][1])
		{
			for (var y = pos[1][1] + 1; y < pos[0][1]; y++)
			{
				if (board.children[GetInvPos(pos[0][0], y)].children[0].text != "")
				{
					return false;
				}
			}
		}
	}
	else if (pos[0][1] == pos[1][1])
	{
		if (pos[0][0] < pos[1][0])
		{
			for (var x = pos[0][0] + 1; x < pos[1][0]; x++)
			{
				if (board.children[GetInvPos(x, pos[0][1])].children[0].text != "")
				{
					return false;
				}
			}
		}
		else if (pos[0][0] > pos[1][0])
		{
			for (var x = pos[1][0] + 1; x < pos[0][0]; x++)
			{
				if (board.children[GetInvPos(x, pos[0][1])].children[0].text != "")
				{
					return false;
				}
			}
		}
	}
	else if (Math.abs(pos[0][0] - pos[1][0]) == Math.abs(pos[0][1] - pos[1][1]))
	{
		function max(a, b)
		{
			return a > b ? a : b;
		}

		if (pos[0][0] < pos[1][0] && pos[0][1] < pos[1][1])
		{
			var x = pos[0][0] + 1;
			var y = pos[0][1] + 1;

			while (x < pos[1][0] && y < pos[1][1])
			{
				if (board.children[GetInvPos(x, y)].children[0].text != "") return false;
				x++;
				y++;
			}
		}
		else if (pos[0][0] > pos[1][0] && pos[0][1] < pos[1][1])
		{
			var x = pos[0][0] - 1;
			var y = pos[0][1] + 1;

			while (x >= pos[1][0] && y < pos[1][1])
			{
				if (board.children[GetInvPos(x, y)].children[0].text != "") return false;
				x--;
				y++;
			}
		}
		else if (pos[0][0] < pos[1][0] && pos[0][1] > pos[1][1])
		{
			var x = pos[0][0] + 1;
			var y = pos[0][1] - 1;

			while (x < pos[1][0] && y >= pos[1][1])
			{
				if (board.children[GetInvPos(x, y)].children[0].text != "") return false;
				x++;
				y--;
			}
		}
		else if (pos[0][0] > pos[1][0] && pos[0][1] > pos[1][1])
		{
			var x = pos[0][0] - 1;
			var y = pos[0][1] - 1;

			while (x > pos[1][0] && y > pos[1][1])
			{
				if (board.children[GetInvPos(x, y)].children[0].text != "") return false;
				x--;
				y--;
			}
		}
	}

	return true;
}

function PlayAI()
{
	/*
		Method for making new moves.
	*/

	spinner.running = true;
	//swipeview.interactive = false;

	var fmove = search();
	var piece2 = board.children[fmove[0]].children[0].text;

	board.children[fmove[0]].children[0].text = "";
	board.children[fmove[1]].children[0].text = piece2;
	board.children[fmove[1]].children[0].color = board.turn;

	spinner.running = false;
	//swipeview.interactive = true;

	return [GetPos(fmove[0]), GetPos(fmove[1])];
}

function CheckMate(color) {
	/*
		Checks for CheckMate.
	*/

	var othercolor = color == "#ffffff" ? "#000000" : "#ffffff";
	var checkmate = true;

	for (var j = 0; j < 64; j++) {
		if (board.children[j].children[0].text != "" && board.children[j].children[0].color == color) {
			for (var i = 0; i < 64; i++) {
				board.turn = color
				if (
					CheckMove(
						[GetPos(j), GetPos(i)], 
						board.children[j].children[0].text, 
						color, 
						board.children[i].children[0].text, 
						board.children[i].children[0].color
					)
				)
				{
						if (board.children[i] == undefined || board.children[j] == undefined)
						{
							continue;
						}

						var p1 = board.children[j].children[0].text;
						var c1 = board.children[j].children[0].color.toString();

						var p2 = board.children[i].children[0].text;
						var c2 = board.children[i].children[0].color.toString();

						board.children[i].children[0].text = p1;
						board.children[i].children[0].color = c1;

						board.children[j].children[0].text = "";

						board.turn = othercolor;

						if (!Check(color))
							checkmate = false;

						board.turn = color;

						board.children[i].children[0].text = p2;
						board.children[i].children[0].color = c2;

						board.children[j].children[0].text = p1;
						board.children[j].children[0].color = c1;
						//if (!checkmate) return checkmate;
				}
			}
		}
	}

	return checkmate;
}

function Check(color) {
	/*
		Check's for Check.
	*/

	var othercolor = color == "#ffffff" ? "#000000" : "#ffffff";
	var j = 0;
	for (; j < 64; j++) {
		if (board.children[j].children[0].text == "king" && board.children[j].children[0].color == color)
			break;
	}

	for (var i = 0; i < 64; i++) {
		if (board.children[i].children[0].text != "" && board.children[i].children[0].color == othercolor) {
			if (CheckMove([GetPos(i), GetPos(j)], board.children[i].children[0].text, othercolor, "king", color))
				return true;
		}
	}

	return false;
}

function CheckMove(pos, piece, color, otherpiece, othercolor) {
	/*
		Checks to make sure move is a valid move.
	*/

	var accept = false;
	var taking = otherpiece != "";

	if (color == "#ffffff") {
			if (piece == "pawn") {
			if (!taking) {
				if (pos[0][1] + 1 == pos[1][1] && pos[0][0] == pos[1][0])
					accept = true;
				else if (board.pawns_white[pos[0][0]] == true)
				{
					if (pos[0][1] + 2 == pos[1][1] && pos[0][0] == pos[1][0])
						accept = true;
				}
			} else {
				if (pos[0][0] - pos[1][0] == 1 && pos[0][1] - pos[1][1] == -1)
					accept = true;
				else if (pos[0][0] - pos[1][0] == -1 && pos[0][1] - pos[1][1] == -1)
					accept = true;
			}
		} else if (piece == "rook") {
			if (pos[0][0] == pos[1][0])
				accept = true;
			else if (pos[0][1] == pos[1][1])
				accept = true;
		} else if (piece == "king") {
			if (pos[0][1] + 1 == pos[1][1] && pos[0][0] == pos[1][0])
				accept = true;
			else if (pos[0][1] - 1 == pos[1][1] && pos[0][0] == pos[1][0])
				accept = true;
			else if (pos[0][0] + 1 == pos[1][0] && pos[0][1] == pos[1][1])
				accept = true;
			else if (pos[0][0] - 1 == pos[1][0] && pos[0][1] == pos[1][1])
				accept = true;
		else if (pos[0][0] - pos[1][0] == 1 && pos[0][1] - pos[1][1] == -1)
			accept = true;
		else if (pos[0][0] - pos[1][0] == -1 && pos[0][1] - pos[1][1] == -1)
			accept = true;
			else if (pos[0][0] - pos[1][0] == 1 && pos[0][1] - pos[1][1] == 1)
				accept = true;
			else if (pos[0][0] - pos[1][0] == -1 && pos[0][1] - pos[1][1] == 1)
				accept = true;
	   } else if (piece == "bishop") {
			if (Math.abs(pos[0][0] - pos[1][0]) == Math.abs(pos[0][1] - pos[1][1]))
				accept = true;
		} else if (piece == "queen") {
			if (Math.abs(pos[0][0] - pos[1][0]) == Math.abs(pos[0][1] - pos[1][1]))
				accept = true;
			else if (pos[0][0] == pos[1][0])
				accept = true;
			else if (pos[0][1] == pos[1][1])
				accept = true;
		} else if (piece == "knight") {
			if (pos[0][0] + 1 == pos[1][0] && pos[0][1] + 2 == pos[1][1])
				accept = true;
			else if (pos[0][0] - 1 == pos[1][0] && pos[0][1] + 2 == pos[1][1])
				accept = true;
			else if (pos[0][0] + 1 == pos[1][0] && pos[0][1] - 2 == pos[1][1])
				accept = true;
			else if (pos[0][0] - 1 == pos[1][0] && pos[0][1] - 2 == pos[1][1])
				accept = true;
			else if (pos[0][0] + 2 == pos[1][0] && pos[0][1] + 1 == pos[1][1])
				accept = true;
			else if (pos[0][0] - 2 == pos[1][0] && pos[0][1] + 1 == pos[1][1])
				accept = true;
			else if (pos[0][0] + 2 == pos[1][0] && pos[0][1] - 1 == pos[1][1])
				accept = true;
			else if (pos[0][0] - 2 == pos[1][0] && pos[0][1] - 1 == pos[1][1])
				accept = true;
		}
	} else {
		if (piece == "pawn") {
			if (!taking) {
				if (pos[0][1] - 1 == pos[1][1] && pos[0][0] == pos[1][0])
					accept = true;
				else if (board.pawns_black[pos[0][0]] == true)
				{
					if (pos[0][1] - 2 == pos[1][1] && pos[0][0] == pos[1][0])
						accept = true;
				}
			} else {
				if (pos[0][0] - pos[1][0] == 1 && pos[0][1] - pos[1][1] == 1)
					accept = true;
				else if (pos[0][0] - pos[1][0] == -1 && pos[0][1] - pos[1][1] == 1)
					accept = true;
			}
		} else if (piece == "rook") {
			if (pos[0][0] == pos[1][0])
				accept = true;
			else if (pos[0][1] == pos[1][1])
				accept = true;
		} else if (piece == "king") {
			if (pos[0][1] + 1 == pos[1][1] && pos[0][0] == pos[1][0])
				accept = true;
			else if (pos[0][1] - 1 == pos[1][1] && pos[0][0] == pos[1][0])
				accept = true;
			else if (pos[0][0] + 1 == pos[1][0] && pos[0][1] == pos[1][1])
				accept = true;
			else if (pos[0][0] - 1 == pos[1][0] && pos[0][1] == pos[1][1])
				accept = true;
			else if (pos[0][0] - pos[1][0] == 1 && pos[0][1] - pos[1][1] == -1)
				accept = true;
			else if (pos[0][0] - pos[1][0] == -1 && pos[0][1] - pos[1][1] == -1)
				accept = true;
			else if (pos[0][0] - pos[1][0] == 1 && pos[0][1] - pos[1][1] == 1)
				accept = true;
			else if (pos[0][0] - pos[1][0] == -1 && pos[0][1] - pos[1][1] == 1)
				accept = true;
		} else if (piece == "bishop") {
			if (Math.abs(pos[0][0] - pos[1][0]) ==  Math.abs(pos[0][1] - pos[1][1]))
				accept = true;
		} else if (piece == "queen") {
			if (Math.abs(pos[0][0] - pos[1][0]) ==  Math.abs(pos[0][1] - pos[1][1]))
				accept = true;
			else if (pos[0][0] == pos[1][0])
				accept = true;
			else if (pos[0][1] == pos[1][1])
				accept = true;
		} else if (piece == "knight") {
			if (pos[0][0] + 1 == pos[1][0] && pos[0][1] + 2 == pos[1][1])
				accept = true;
			else if (pos[0][0] - 1 == pos[1][0] && pos[0][1] + 2 == pos[1][1])
				accept = true;
			else if (pos[0][0] + 1 == pos[1][0] && pos[0][1] - 2 == pos[1][1])
				accept = true;
			else if (pos[0][0] - 1 == pos[1][0] && pos[0][1] - 2 == pos[1][1])
				accept = true;
			else if (pos[0][0] + 2 == pos[1][0] && pos[0][1] + 1 == pos[1][1])
				accept = true;
			else if (pos[0][0] - 2 == pos[1][0] && pos[0][1] + 1 == pos[1][1])
				accept = true;
			else if (pos[0][0] + 2 == pos[1][0] && pos[0][1] - 1 == pos[1][1])
				accept = true;
			else if (pos[0][0] - 2 == pos[1][0] && pos[0][1] - 1 == pos[1][1])
				accept = true;
		}
	}

	if (taking)
		if (othercolor == color)
			accept = false;

	if (board.turn != color || !CheckPath(pos))
		accept = false;

	return accept;
}

/*
	Restart the game.
*/

function Restart() {
	board.turn = "#ffffff";
	board.first = true;
	board.visible = true;

	board.pawns_white = [true, true, true, true, true, true, true, true];
	board.pawns_black = [true, true, true, true, true, true, true, true];

	var column = 0;
	var row = 0;

	for (var i = 0; i < 64; i++) {
		row = GetPos(i)[0];
		column = GetPos(i)[1];

		board.children[i].children[0].text = "";
		board.children[i].nx = false;
		board.children[i].fx = false;

		if (column % 2 == 1 && row % 2 == 0) {
			board.children[i].white = false;
		} else if (column % 2 == 0 && row % 2 == 1) {
			board.children[i].white = false;
		} else {
			board.children[i].white = true;
		}

		if (column == 0) {
			if (row == 0)
				board.children[i].children[0].text = "rook";
			if (row == 1)
				board.children[i].children[0].text = "knight";
			if (row == 2)
				board.children[i].children[0].text = "bishop";
			if (row == 3)
				board.children[i].children[0].text = "queen";
			if (row == 4)
				board.children[i].children[0].text = "king";
			if (row == 5)
				board.children[i].children[0].text = "bishop";
			if (row == 6)
				board.children[i].children[0].text = "knight";
			if (row == 7)
				board.children[i].children[0].text = "rook";

			board.children[i].children[0].color = "white";

		} else if (column == 1) {
			board.children[i].children[0].text = "pawn";
			board.children[i].children[0].color = "white";
		} else if (column == 6) {
			board.children[i].children[0].text = "pawn";
			board.children[i].children[0].color = "black";
		} else if (column == 7) {
			if (row == 0)
				board.children[i].children[0].text = "rook";
			if (row == 1)
				board.children[i].children[0].text = "knight";
			if (row == 2)
				board.children[i].children[0].text = "bishop";
			if (row == 3)
				board.children[i].children[0].text = "queen";
			if (row == 4)
				board.children[i].children[0].text = "king";
			if (row == 5)
				board.children[i].children[0].text = "bishop";
			if (row == 6)
				board.children[i].children[0].text = "knight";
			if (row == 7)
				board.children[i].children[0].text = "rook";

			board.children[i].children[0].color = "black";
		}

		//board.children[i].border.width = "0";
		board.children[i].selected = false;
	}
}

/*
	Does the user's move.
*/

function MakeMove(firstIndex, lastIndex) {
	var piece = board.children[firstIndex].children[0].text;
	var color = board.children[firstIndex].children[0].color;
	var otherpiece = board.children[lastIndex].children[0].text;
	var othercolor = board.children[lastIndex].children[0].color;
	var othercolor = board.children[lastIndex].children[0].color.toString();
	var pos = [ GetPos(firstIndex), GetPos(lastIndex) ];

	if (CheckMove(pos, piece, color, otherpiece, othercolor)) {
		board.children[firstIndex].children[0].text = "";
		board.children[lastIndex].children[0].text = piece;
		board.children[lastIndex].children[0].color = color;

		board.children[firstIndex].selected = false;
		//board.children[firstIndex].border.width = "0";

		if (piece == "pawn" && pos[1][1] == 7 && color == "#ffffff") {
			board.children[lastIndex].children[0].text = "queen";
		} else if (piece == "pawn" && pos[1][1] == 0 && color == "#000000") {
			board.children[lastIndex].children[0].text = "queen";
       		}

		board.check = false;

		var next = board.turn == "#ffffff" ? "#000000" : "#ffffff";
		var first = board.turn;

		board.turn = next;
		if (Check(first)) {
			board.children[firstIndex].children[0].text = piece;
			board.children[lastIndex].children[0].text = otherpiece;
			board.children[lastIndex].children[0].color = othercolor;
			board.children[firstIndex].selected = true; 
			//board.children[firstIndex].border.width = "2";
			board.turn = first;
			return;
		} else
			board.turn = first;

		board.check = Check(next);
		if (board.check) {
			if (CheckMate(next)) {
				if (next == "#ffffff") {
					if (gamewidget.players == "two")
						gamewidget.endGame("Black Won");
					else
						gamewidget.endGame("Spectrum Won");
				} else {
					//if ()
						gamewidget.endGame("White Won");
					//else
					//	gamewidget.endGame(Spectrum Won");

				}
				return;
			}
			else
			{
				if (next == "#ffffff")
				{
					//if (gamewidget.players == two")
					//{
						gamewidget.updateStatus("White in Check");
					//}
					//else
					//{
					//	gamewidget.endGame(Spectrum in Check");
					//}
				}
				else
				{
					if (gamewidget.players == "two")
					{
						gamewidget.updateStatus("Black in Check");
					}
					else
					{
						gamewidget.updateStatus("Spectrum in Check");
					}
				}
			}
		}
		
		if (piece == "pawn" && color == "#ffffff")
		{
			board.pawns_white[pos[0][0]] = false;
		}
		else if (piece == "pawn" && color == "#000000")
		{
			board.pawns_black[pos[0][0]] = false;
		}
	}
	else
	{
		return;
	}

	board.children[firstIndex].nx = true; 
	board.children[lastIndex].fx = true;
	while (board.is_moving) gamewidget.update(); // wait for move to finish.

	if (board.turn == "#ffffff" && gamewidget.players == "two")
		board.moving2black = true; // XXX 
	else if (gamewidget.players == "two")
		board.moving2white = true;

	board.turn = next;

	if (gamewidget.players != "two") {
		var indexes;
		indexes = PlayAI();

	        piece = board.children[GetInvPos(indexes[1][0], indexes[1][1])].children[0].text;

		if (piece == "pawn" && indexes[1][1] == 0 && gamewidget.players == "one")
			board.children[GetInvPos(indexes[1][0], indexes[1][1])].children[0].text = "queen";

		board.check = Check(first);
		if (board.check) {
			if (CheckMate(first)) {
				if (first == "#ffffff") {
					if (gamewidget.players == "two")
						gamewidget.endGame("Black Won");
					else
						gamewidget.endGame("Spectrum Won");
				} else {
					gamewidget.endGame("White Won");
				}
				return;
			}
			else
			{
				if (first == "#ffffff")
				{
					gamewidget.updateStatus("White in Check");
				}
				else
				{
					if (gamewidget.players == "two")
					{
						gamewidget.updateStatus("Black in Check");
					}
					else
					{
						gamewidget.updateStatus("Spectrum in Check");
					}
				}
			}
		}

		board.children[GetInvPos(indexes[0][0], indexes[0][1])].nx = true;
	        board.children[GetInvPos(indexes[1][0], indexes[1][1])].aix = true;
	        while (board.is_moving) gamewidget.update(); // wait for move to finish.

		board.turn = first;

   		if (piece == "pawn" && gamewidget.players == "one")
  	  	{
    			board.pawns_black[indexes[0][0]] = false;
   		}
	}
}
