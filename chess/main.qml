import QtQuick 2.7 
import AbstractGames.Widgets 1.0
import "game.js" as Game


GameController {
	gameType: "BoardGame"
	name: "Chess"

	background: Grid {
		id: board
		rows: 8
		columns: 8

		Repeater {
			model: 64

			Rectangle {
				width: board.width/8
				height: board.height/8
				id: rect

				property bool white: false 

				color: white ? "lightgray" : "darkgray"

				Component.onCompleted: {
					var row    = Game.GetPos(index)[0];
					var column = Game.GetPos(index)[1];

					if (column % 2 == 1 && row % 2 == 0) {
						board.children[index].white = false;
					} else if (column % 2 == 0 && row % 2 == 1) {
						board.children[index].white = false;
					} else {
						board.children[index].white = true;
					}
				}
			}
		}
	}
}
