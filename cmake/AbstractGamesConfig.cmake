find_package(Qt5 COMPONENTS Widgets QuickWidgets QuickControls2 Test Concurrent REQUIRED)

find_package(ECM 1.7.0 REQUIRED NO_MODULE)
set(CMAKE_MODULE_PATH ${ECM_MODULE_PATH})

include(KDEInstallDirs)
include(FeatureSummary)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)

include_directories(${CMAKE_BINARY_DIR})
add_definitions(${QT_DEFINITIONS})

set(ABSTRACTGAMES_LIBS Qt5::Widgets Qt5::QuickWidgets Qt5::QuickControls2 Qt5::Test Qt5::Concurrent)

include("${CMAKE_CURRENT_LIST_DIR}/AbstractGamesMacros.cmake")
