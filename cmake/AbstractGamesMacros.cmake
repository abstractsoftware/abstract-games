macro(build_game)
	set(OneValueArgs GAME RESOURCE)
        cmake_parse_arguments(ARG "" "${OneValueArgs}" "" ${ARGN})

	set(LIBS abstractgames.core abstractgames.widgets abstractgames.logic Qt5::Widgets Qt5::QuickWidgets Qt5::QuickControls2 Qt5::Test Qt5::Concurrent)
	set(SRCS ${ARG_GAME}/main.cxx ${ARG_GAME}/main.qrc)

	if (NOT ${ARG_RESOURCE} EQUAL "")
		set(DES ${ARG_RESOURCE}/${ARG_GAME}.desktop)
		set(ICO ${ARG_RESOURCE}/${ARG_GAME}.svg)
		set(TAR ${ARG_GAME})

		add_executable(${TAR} ${SRCS})
		target_link_libraries(${TAR} PRIVATE ${LIBS})

		install(TARGETS ${TAR} DESTINATION ${KDE_INSTALL_TARGETS_DEFAULT_ARGS})
		install(FILES ${DES} DESTINATION ${XDG_APPS_INSTALL_DIR})
		install(FILES ${ICO} DESTINATION ${KDE_INSTALL_ICONDIR}/hicolor/scalable/apps)
	endif()
endmacro()
