import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.3
import "game.js" as Game
import AbstractGames.Logic 1.0 as Logic
import AbstractGames.Widgets 1.0

SimpleGameWidget {
	id: gamewidget

	property int minSpeed: gamewidget.difficulty == "easy" ? 10 : gamewidget.difficulty == "medium" ? 15 : 20
	property int maxSpeed: gamewidget.difficulty == "easy" ? 20 : gamewidget.difficulty == "medium" ? 25 : 30
	property int minNum: gamewidget.difficulty == "hard" ? 6 : 3
	property int maxNum: 6
	property int level: 1
	property int maxlevels: 3
	property int speed: level == 1 ? minSpeed : level == 2 ? (minSpeed+maxSpeed)*0.5 : maxSpeed
	property int points: 0
	property int num: level == 1 ? minNum : maxNum
	property int lifes: 0

	focus: true

	Keys.onPressed: {
	}

	onGameRestarted: {
		Game.Restart();
	}

	onGamePaused: {
		mainloop.running = false;
		mainloop.stop();

		for (var i = 0; i < num; i++)
			cars.children[i].running = false;
	}

	onGameStarted: {
		mainloop.running = true;
		mainloop.start();

		for (var i = 0; i < num; i++) cars.children[i].running = true;
	}

	Text {
		visible: false
                anchors.left: parent.left
		anchors.leftMargin: 10
		anchors.top: parent.top
		anchors.topMargin: 10
                color: Material.color(Material.Red)
		text: "lives = " + gamewidget.lifes
		z: 100
		font.family: "Ubuntu"
		font.pointSize: 12
        }


	Timer {
		id: mainloop
		interval: 1
		repeat: true

		onTriggered: {
			for (var i = 0; i < cars.children.length; i++)
			{
				if (Logic.Collisions.isColliding(frog, cars.children[i]))
				{
					for (var j = 0; j < cars.children.length; j++)
					{
						if (j == num) break;
						cars.children[j].mainloop.running = false;
						cars.children[j].mainloop.stop();
					}

					frog.begin = false;
					mainloop.running = false;
					movingfrog.running = false;
					mainloop.stop();
					movingfrog.stop();
					gamewidget.lifes--;

					if (gamewidget.lifes == 0)
					{
						gamewidget.updateStatus("You lose");
						Game.gotoNextLevel();
					}

					Game.movecar(i, cars.children[i].width + frog.width);

					for (var j = 0; j < cars.children.length; j++)
					{
						if (j == num) break;
						cars.children[j].mainloop.running = true;
						cars.children[j].mainloop.start();
					}

					mainloop.start();
				}
			}

			for (var i = 0; i < tokens.children.length; i++)
			{
				if (tokens.children[i].visible)
				{
					if (Logic.Collisions.isColliding(frog, tokens.children[i]))
					{
						tokens.children[i].visible = false;

						if ( tokens.children[i].type == "point")
							gamewidget.points++;
						else
							gamewidget.lifes++;
				       }
				}
			}

		       if (Qt.application.state == Qt.ApplicationInActive)
                       {
                               gamewidget.pauseGame();
                       }
		}
	}

	Timer {
		id: movingfrog
		interval: 1
		repeat: true
		property string dir: ""

		onTriggered: {
			if (!frog.begin)
			{
				running = false;
				stop();
				return;
			}
			Game.movefrogbyone(dir, 5);
		}
	}

	Image {
		id: frog
		width: 24
		property bool begin: false
		height: 24
		source: "images/frog.svg"
		x: gamewidget.width / 2
		y: 10
		z: 10
	}

	Rectangle {
		id: start
		anchors.left: parent.left
		anchors.right: parent.right
		height: 50
		y: -8
		color: Material.color(Material.Green) 
		radius: 10
		smooth: true
	}

	Rectangle {
		color: "transparent"
		anchors.fill: parent
		id: islands

		Repeater {
			model: 2
			
			Rectangle {
				radius: 5
				id: x
				height: 50
				width: 200
				y: (index * 180) + 180
				color: Material.color(Material.Green) 
			}
		}
	}

	Rectangle {
		color: "transparent"
		anchors.fill: parent
		id: tokens

		Repeater {
			model: 3

			Image {
				property var type: "point"
				width: 25
				height: 25
				source: "images/star.svg"
				x: Math.random() * (600 - width)
				y: index * 180 + 97
			}
		}

		Repeater {
			model: 2

			Image {
				property var type: "life"
				width: 25
				height: 25
				source: "images/token.svg"
				x: (index == 0 ? islands.children[0].x + 80 : islands.children[0].x + 200 + 80)
				y: (index == 0 ? islands.children[0].y + 6  : islands.children[0].y + 180 + 6)
			}
		}
	}

	Rectangle {
		color: "transparent"
		anchors.fill: parent
		id: cars
	
		Repeater {
			model: num 
	
			Image {
				property bool begin: false
				property int speed: (Math.random() * 800) + 500
				property bool running: true
				property var mainloop: t
				property int dir: (index % 2) == 0 ? +1 : -1

				z: 10
				width: 80
				height: 30
				source: (index % 2) == 0 ? "images/car-left.svg" : "images/car-right.svg"
				x: (index % 2) == 0 ? 0 : gamewidget.width
				y: (Math.floor(num == 6 ? index * 0.5 : index) * 180) + 10 + 
					(num == 6 ? ((index % 2) == 0 ? 50 : 120) : 80)

				Timer {
					id: t
					repeat: true
					interval: 100
					running: parent.running

					onTriggered: {
						Game.movecar(index);
					}
				}
			}	
		}	
	}

	Rectangle {
		id: end
		anchors.left: parent.left
                anchors.right: parent.right
		height: 50
		radius: 10
		y: gamewidget.height - 50 + 8
		color: Material.color(Material.Green)
	}

	PauseScreen {
                width: 600
                height: 600

                Timer {
                        running: true
                        interval: 500
                        repeat: true

                        onTriggered: {
                                parent.containsMouse = gamewidget.containsMouse;
                        }
                }
	}

	GameBar {
		buttonImages: [
			1,
			2,
			3,
			4
		]

		buttonPresses: [
			function () { Game.movefrog("east", true);  },
			function () { Game.movefrog("west", true);  },
			function () { Game.movefrog("north", true); },
			function () { Game.movefrog("south", true);  }
		]

		buttonReleases: [
			function () { frog.begin = false; },
			function () { frog.begin = false; },
			function () { frog.begin = false; },
			function () { frog.begin = false; }
		]

		text: "lives: " + gamewidget.lifes + (gamewidget.level != 1 ? (" points: " + gamewidget.points) : "")
	}
}
