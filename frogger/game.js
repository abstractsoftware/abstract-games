/*
	Core game functions.
*/

.import AbstractGames.Logic 1.0 as Logic

var inCar = false;

function Restart()
{
	inCar = false;

	gamewidget.level = 1;
	gamewidget.lifes = 1;

	gotoNextLevel();
}

function movefrog(dir, start = false)
{
	if (start) { frog.begin = true; }

	movingfrog.dir = dir;
	movingfrog.running = true;
}

function movefrogbyone(dir, inc)
{
	if (frog.y < 0 && dir == "north") return;
	
	if (frog.y > gamewidget.height && dir == "south") return;

	if (frog.x < 0 && dir == "east") return;
	
	if (frog.x > gamewidget.width && dir == "west") return;

	if (dir == "north")
		frog.y -= inc;
	else if (dir == "south")
		frog.y += inc;
	else if (dir == "east")
		frog.x -= inc;
	else if (dir == "west")
		frog.x += inc;
	else
		return;

	if (Logic.Collisions.isColliding(frog, end) && !inCar)
	{
		inCar = true;

		frog.begin = false;
		mainloop.running = false;
		movingfrog.running = false;
		movingfrog.stop();
		mainloop.stop();

		for (var i = 0; i < gamewidget.num; i++)
		{
			cars.children[i].running = false;
			cars.children[i].mainloop.stop();
		}

		var all = true;

		for (var i = 1; i < tokens.children.length; i++) {
			if (i == 3 || i == 6)
				continue;

			if (tokens.children[i].visible)
				all = false;
		}

		if (all)
		{
			if (gamewidget.level != gamewidget.maxlevels)
			{
				gamewidget.level += 1;
				gamewidget.updateStatus("Onto level " + gamewidget.level + ".");
				gotoNextLevel();
			}
			else
			{
				gamewidget.endGame("You won!");
				return;
			}
		}
		else
		{
			gamewidget.updateStatus("Remember to get all the hearts and stars!");

		}

		for (var i = 0; i < gamewidget.num; i++)
		{
			cars.children[i].mainloop.start();
		}

		mainloop.start();

		inCar = true;

		return;
	}

	if (!Logic.Collisions.isColliding(frog, end) && inCar)
	{
		inCar = false;
	}

	if (frog.x < 0)
		frog.x = 0;

	if (frog.x > gamewidget.width)
		frog.x = gamewidget.width - frog.width;

	gamewidget.sleep(10);
}

function gotoNextLevel()
{
	frog.x = (gamewidget.width / 2) - frog.width;
	frog.y = 10;

	islands.children[0].x = (Math.random() * 200) + 100;
	islands.children[1].x = (Math.random() * 200) + 100;

	for (var i = 0; i < cars.children.length; i++)
	{
		if (i == gamewidget.num) break;
		cars.children[i].speed = (Math.random() * gamewidget.speed) + 10;
	}

	for (var i = 0; i < gamewidget.num; i++)
	{
		cars.children[i].running = true;
		cars.children[i].mainloop.start();
	}

	for (var i = 0; i < 4; i++)
	{
		tokens.children[i].visible = true;
		tokens.children[i].x       = Math.random() * (600 - tokens.children[i].width);
	}

	for (var i = 4; i < 6; i++)
	{
		tokens.children[i].visible = true;
		tokens.children[i].x       = islands.children[i - 4].x + islands.children[i - 4].width*0.4;
		tokens.children[i].y       = islands.children[i - 4].y + islands.children[i - 4].height*0.25;
	}

	mainloop.running = true;

	gamewidget.lifes = 1;

	if (gamewidget.level == 1 && gamewidget.difficulty != "hard")
	{
		tokens.children[0].visible = false;
		tokens.children[1].visible = false;
		tokens.children[2].visible = false;
	}
}

function movecar(index, interval = -1)
{
	var speed = interval;
	if (interval == -1)
		speed = cars.children[index].speed;

	cars.children[index].x += cars.children[index].dir*speed;

	if (cars.children[index].dir == +1)
	{
		if (cars.children[index].x > gamewidget.width)
		{
			cars.children[index].x = 0;
		}
	}
	else
	{       
		if (cars.children[index].x < 0)
		{
			cars.children[index].x = gamewidget.width
		}
	}
}
