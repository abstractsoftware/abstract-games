import QtQuick 2.7
import AbstractGames.Widgets 1.0
import "game.js" as Game

SimpleGameWidget {
	id: gamewidget

	onGameRestarted: {
		Game.Restart();
	}
	
	    Grid {
		    anchors.fill: parent
			property int n: 19 

			id: board
			columns: n 
			rows: n

			property string player
			property bool pause: false


			property var properties: [
				"player",
				"pause"
			]

			Repeater {
				model: board.n*board.n 

				Rectangle {
					width: (gamewidget.width/board.n)
					height: (gamewidget.height/board.n)
					color: "transparent"
					id: square

					MouseArea {
						id: mousearea

						onClicked: {
							Game.Click(index);
						}

						anchors.centerIn: parent
						anchors.fill: parent
					}

					Image {
						id: img
						property string color: ""

						property var properties: [
							"color",
							"visible"
						]

						smooth: true
						source: color == "black" ? "images/black.svg" : "images/white.svg"
						anchors.centerIn: parent
						width: parent.width
						height: parent.height
						visible: false
						z: 1
					}

					Rectangle {
						anchors.centerIn: parent
                                                width: 1
						height: parent.height
						color: "black"
					}

					Rectangle {
						anchors.centerIn: parent
						color: "black"
                                                height: 1
                                                width: parent.width 
					}

					Rectangle {
						anchors.top: parent.top
						visible: index < board.n ? true : false
						color: "transparent"
				                width: gamewidget.width
				                height: (0.5 * (gamewidget.height / board.n)) - 1
					}

					Rectangle {
						anchors.bottom: parent.bottom
                                                visible: index >= ((board.n * board.n) - board.n) ? true : false
                                                color: "transparent"
                                                width: gamewidget.width
                                                height: (0.5 * (gamewidget.height / board.n)) - 1
					}

					Rectangle {
						anchors.left: parent.left
                                                visible: index % board.n == 0 ? true : false
                                                color:  "transparent"
                                                height: gamewidget.height
                                                width: (0.5 * (gamewidget.width / board.n)) - 1
					}

					Rectangle {
                                                anchors.right: parent.right
                                                visible: (index - board.n - 18) % board.n == 0 ? true : false
                                                color: "transparent"
                                                height: gamewidget.height
                                                width: (0.5 * (gamewidget.width / board.n)) - 1
                                        }
				}
			}
		}
	}
