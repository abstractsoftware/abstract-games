function PlayAI()
{
	var ones = new Array();

	for (var i = 0; i < board.n * board.n; i++)
	{
		if (board.children[i].children[1].visible) continue;

		board.children[i].children[1].visible = false;
		board.children[i].children[1].color = "black";

		if (Check(i, false))
		{
			board.children[i].children[1].visible = false;
			board.children[i].children[1].color = "";
                        continue;
		}

		board.children[i].children[1].visible = false;
		
		ones.push(i);
	}

	function RandomIndex()
	{
		return Math.floor(Math.random() * ones.length);
	}

	if (!ones.length)
	{
		return;
	}

	var bestones = ones;

	/*
	 * process ones below
	 * put best ones into bestones
	*/
	
	do {  } while (false);

	if (!bestones.length)
	{
		bestones = ones;
	}

	var choosen_one = bestones[RandomIndex()];

	board.children[choosen_one].children[1].visible = true;
	board.children[choosen_one].children[1].color = "black";
	board.player = "white";
}

function Restart()
{
	for (var i = 0; i < board.n*board.n; i++)
	{
		board.children[i].children[1].visible = false;
	}

	board.children[board.n * 3 + 3].children[1].visible = true;
	board.children[board.n * 3 + 3].children[1].color = "black";

	board.children[board.n * 3 + (board.n - 4)].children[1].visible = true;
        board.children[board.n * 3 + (board.n - 4)].children[1].color = "black";

	board.children[(board.n*board.n) - board.n * 4 + 3].children[1].visible = true;
        board.children[(board.n*board.n) - board.n * 4 + 3].children[1].color = "black";

        board.children[(board.n*board.n) - board.n * 4 + (board.n - 4)].children[1].visible = true;
        board.children[(board.n*board.n) - board.n * 4 + (board.n - 4)].children[1].color = "black";

	board.player = "white";
}

function Click(index)
{
	if (board.children[index].children[1].visible) return;

	board.children[index].children[1].visible = true;
	board.children[index].children[1].color = board.player;
	var i = Check(index);

	if (!i && gamewidget.players != "two")
	{
		PlayAI();
	}

	if (i)
	{
		board.children[index].children[1].visible = false;
		board.children[index].children[1].color = "";
	}
}

function willBeTaken(i, index, board)
{
	var exit = 0;

	if (board.children[i] &&
		board.children[i].children[1] &&
		board.children[i].children[1].visible)
	{
		var a = true;
		var c = board.children[i].children[1].color; 

		if (i - 1 >= 0 && (i % board.n))
		{
			if (!board.children[i - 1].children[1].visible || board.children[i - 1].children[1].color == c)
			{
				a = false;
			}
		}

		if (i + 1 < board.n*board.n)
                       {
                               if (!board.children[i + 1].children[1].visible || board.children[i + 1].children[1].color == c)
                               {
                                       a = false;
                               }
                       }

		if (i - board.n >= 0)
                       {
                               if (!board.children[i - board.n].children[1].visible || board.children[i - board.n].children[1].color == c)
                               {
                                       a = false;
                               }
                       }

		if (i + board.n < board.n*board.n)
                       {
                               if (!board.children[i + board.n].children[1].visible || board.children[i + board.n].children[1].color == c)
                               {
                                       a = false;
                               }
                       }

		if (a)
		{
			board.children[i].children[1].visible = false;
			if (i == index) exit += 1;
		}
	}

	return exit;
}

function Check(index, win = true)
{
	var exit = 0;
	var all = true;
	var numwhite = 0, numblack = 0;

	exit += willBeTaken(index, index, board);
	exit += willBeTaken(index + 1, index, board);
	exit += willBeTaken(index - 1, index, board);
	exit += willBeTaken(index + board.n, index, board);
	exit += willBeTaken(index - board.n, index, board);

	if (win)
	{
		for (var i = 0; i < board.n*board.n; i++)
		{
			if (!board.children[i].children[1].visible) all = false;
			else
			{
				if (board.children[i].children[1].color == "white") numwhite++;
				else numblack++;
			}
		}
	
		if (all)
		{
			if (numwhite > numblack)
			{
				gamewidget.endGame("White won");
			}
			else
			{
				if (gamewidget.players != "two") gamewidget.endGame("Spectrum won"); 
				else gamewidget.endGame("Black won");
			}

			return;

		}

		if (board.player == "white") board.player = "black";
	        else board.player = "white";
	}

	return exit;
}
