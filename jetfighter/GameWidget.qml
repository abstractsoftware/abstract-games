import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.1
import AbstractGames.Logic 1.0 as Logic
import AbstractGames.Widgets 1.0
import "game.js" as Game

SimpleGameWidget
{
	id: gamewidget
	
	property bool lock: false
	property var pause: false
	property int maxheight
	property double speed: 10
	property int lifes
	property int score
	property bool wahIn: false
	property int maxlevels: 2
	property int level: 0

	property var properties: [
		"level",
		"maxlevels",
		"maxheight",
		"lifes",
		"score",
		"lock",
		"pause"
	]

	onGameRestarted:
	{
		Game.Restart();
	}
	
	onGamePaused:
	{
		timer.running = false;
	}
	
	onGameEnded:
	{
		timer.running = false;
	}
	
	onGameStarted:
	{
		timer.running = true;
	}
	
	onClicked:
	{
		if (gamewidget.lock) return;
		
		gamewidget.lock = true;
		
		var component = Qt.createComponent("bomb.qml")
		var bomb = component.createObject(gamewidget, 
			{"x": jetplane.x, "y": jetplane.y, "width": gamewidget.width / 30, "height": gamewidget.height / 30});
		
		var i = 0;
		var step = 2;
		var stop = false;
		
		var its = 0;
		var nspe = (gamewidget.speed == 5 ? 6 : 7);
		var spe = nspe;
		var inc = false;
		
		while (true)
		{
			bomb.x -= step;
			bomb.y += spe;
			its++;
			gamewidget.update();
			
			for (i = 0; i < 10; i++)
			{
				if (Logic.Collisions.isColliding(bomb, buildings.children[i]))
				{
					stop = true;
					break;
				}
			}

			if (bomb.x < 0 || bomb.y > 600)
			{
				bomb.destroy();
				gamewidget.lock = false;
				return;
			}
			
			if (step > 0) step -= 0.05;
			if (stop) break;
		}
		
		bomb.destroy();
		Game.burndown(i);
		gamewidget.score += 5;
		gamewidget.lock = false;
	}
	
	Rectangle {
		id: jetplane
		x: 10
		y: 10
		height: 22.7
		width: 50
		z: 1000
		color: "transparent"
		
		property var properties: [
			"x",
			"y"
		]

		Image {
			id: i
			anchors.fill: parent
			source: "images/plane.svg"
		}
	}
	
	Item {
		id: buildings
		anchors.fill: parent
		
		Repeater {
			model: 10
			
			Rectangle {
				property var properties: [
					"height",
					"width",
					"x"
				]

				id: rect
				anchors.bottom: parent.bottom
				width: gamewidget.width / 13
				height: Math.ceil(Math.random() * gamewidget.maxheight) + 100
				
				x: ((index * rect.width) * 1.2) + 30
				
				Behavior on height {
					NumberAnimation
					{
						duration: 250
					}
				}
				
				color: "transparent"
				
				Image {
					anchors.fill: parent
					source: "images/mountain.svg"
					smooth: true
				}
			}
		}
	}
	
	GameBar {
                text: "lives left: " + gamewidget.lifes + ", score: " + gamewidget.score
        }
	
	Timer {
		id: timer
		repeat: true
		interval: 10
		
		onTriggered:
		{
			Game.moveplane();
		}
	}
	
	PauseScreen {
		width: 600
		height: 600
		
		Timer {
			running: true
			interval: 500
			repeat: true
			
			onTriggered: {
				parent.containsMouse = gamewidget.containsMouse; 
			}
		}
	}
}

