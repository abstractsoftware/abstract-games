import QtQuick 2.7
import QtQuick.Controls.Material 2.7
import AbstractGames.Widgets 1.0

GameController {
	gameType: "ArcadeGame"
	name: "JetFighter"

	background: Rectangle {
		color: Material.color(Material.Cyan)
	}
}
