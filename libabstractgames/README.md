# LibAbstractGames

LibAbstractGames is a general library for creating and deploying games so that they can easily be added to Abstract Games. This framework uses a QML and Javascript API. LibAbstractGames contains four parts, each which has a different purpose.

* LibAbstractGames.Core - core C++ classes
* LibAbstractGame.Widgets - QML types and widgets used for gaming
* LibAbstractGames.Logic - JS skeleton types for game logic and special types of widgets
* LibAbstractGames.Templates - Templates for different types of game, used when creating new games

LibAbstractGames implements all of the basic features for games - automatically making the startupscreen, swiping, and ensures that all games use the same theme. It also automatically makes the background, and features like pausing and continuing games. This framework is what makes Abstract Games different from all other games.
