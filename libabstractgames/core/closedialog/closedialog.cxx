#include "closedialog.h"
#include <QUrl>
#include <QQuickItem>

CloseDialog::CloseDialog(QWidget* parent) : QDialog(parent)
{
	QQuickWidget* widget = new QQuickWidget(this);
        widget->setFixedSize(300, 50);
        widget->setSource(QUrl("qrc:/qml/CloseDialog.qml"));

	setFixedSize(300, 50);

	connect(widget->rootObject(), SIGNAL(accepted()), this, SLOT(accept()));
	connect(widget->rootObject(), SIGNAL(rejected()), this, SLOT(reject()));
}
