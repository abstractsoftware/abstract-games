#pragma once

#include <QDialog>
#include <QQmlEngine>
#include <QQuickWidget>

class CloseDialog : public QDialog
{
	Q_OBJECT

public:
	CloseDialog(QWidget* parent = nullptr);
};
