import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.1

Rectangle
{
	Material.accent: Material.green
	Material.theme: "Light"
	id: closedialog
	visible: true
	objectName: "closedialog"
	height: 50
	width: 300

	property string bg: closedialog.color

	signal accepted()
	signal rejected()

	SequentialAnimation
                {
                        running: true
                        loops: Animation.Infinite

                        ColorAnimation
                        {
                                properties: "color"
                                target: closedialog
                                duration: 5000
                                from: "#F44336"
                                to: "#2196F3"
                        }

                        ColorAnimation
                        {
                                properties: "color"
                                target: closedialog
                                duration: 5000
                                from: "#2196F3"
                                to: "#4CAF50"
                        }

                        ColorAnimation
                        {
                                properties: "color"
                                target: closedialog
                                duration: 5000
                                from: "#4CAF50"
                                to: "#F44336"
                        }
                }

	Rectangle
	{
		anchors.fill: parent
		color: closedialog.bg

		Row
		{
			anchors.centerIn: parent

			Rectangle
			{
				color: closedialog.bg
				width: closedialog.width / 2.0
				height: closedialog.height / 2.0

				Text
				{
					anchors.left: parent.left
					anchors.leftMargin: 10
					anchors.verticalCenter: parent.verticalCenter
					font.family: "Ubuntu"
					font.pixelSize: 15
					text: "Are You Sure?"
				}
			}
		

			Rectangle
			{
				color: closedialog.bg
				width: closedialog.width / 2.0
				height: closedialog.height / 2.0

				Row
				{
					anchors.right: parent.right
					anchors.rightMargin: 10
					anchors.verticalCenter: parent.verticalCenter
					spacing: 5

					Button
					{
						text: "Yes"

						onClicked: {
							closedialog.accepted();
						}
					}

					Button
					{
						text: "No"

						onClicked: {
							closedialog.rejected();
						}
					}
				}
			}
		}
	}
}
