#include "gamewindow.h"
#include <QDebug>
#include <QtConcurrent>
#include <QFont>
#include <QJSValue>
#include <QQuickItem>
#include <cstdlib>
#include <QQmlEngine>
#include <QGuiApplication>
#include <QIcon>
#include <QFile>
#include <QCursor>
#include <QDataStream>
#include <QMetaProperty>
#include <QFile>

GameWindow::GameWindow(QString name, QString prefix, QWidget* parent, QString icon) : QWidget(parent)
{
	QString Prefix = "qrc:" + name;

	if (!prefix.isEmpty())
	{
		Prefix = prefix;
	}

	Name = name;

	QFont font = qApp->font();
	font.setFamily("Roboto");

	if (QQuickStyle::name().isEmpty())
	{
		QQuickStyle::setStyle("Material");
		QGuiApplication::setFont(font);
	}

	widget = new QQuickWidget(this);
	widget->setFixedSize(600, 600);
	widget->engine()->setBaseUrl(QUrl(Prefix));
	widget->engine()->addImportPath(Prefix + "/");
	widget->setSource(QUrl(Prefix + "/main.qml"));

	setFixedSize(600, 600);
	setWindowTitle(Name);

	QPixmap pixmap;
	QByteArray data;
	
	if (icon.isEmpty())
	{
		if (prefix.isEmpty())
		{
			windowIcon = ":" + name + "/images/icon.svg";
		}
		else
		{
			windowIcon = QUrl(Prefix + "/images/icon.svg").toLocalFile();
		}
	}
	else
	{
		windowIcon = icon;
	}

	setWindowIcon(QIcon(windowIcon));

	show();
	setMouseTracking(true);

	connect(this, &GameWindow::close, this, &GameWindow::onClose);
}
 
void GameWindow::closeEvent(QCloseEvent *event)
{
	this->onClose();
	//QFuture<void> future = QtConcurrent::run([=]() { this->onClose(); });
	//event->ignore();
}

void GameWindow::onClose()
{
	bool val = false;
	QQuickItem* rootItem = widget->rootObject();
	QMetaObject* metaObject = (QMetaObject*) rootItem->metaObject();
	metaObject->invokeMethod(rootItem, "closeDialog", Qt::DirectConnection, Q_RETURN_ARG(bool, val));

	if (val)
	{
		qApp->quit();
		return;
	}
}
