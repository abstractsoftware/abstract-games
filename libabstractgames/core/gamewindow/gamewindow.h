#pragma once

#include <QMainWindow>
#include <QIcon>
#include <QFile>
#include <QFileDialog>
#include <QQmlEngine>
#include <QDebug>
#include <QQuickWidget>
#include <QMessageBox>
#include <QDesktopServices>
#include <QPixmap>
#include <QMouseEvent>
#include <QQuickStyle>
#include <QCloseEvent>

#include "../closedialog/closedialog.h"

class GameWindow : public QWidget
{
	Q_OBJECT

public:
	GameWindow(QString name, QString prefix = "", QWidget* parent = nullptr, QString icon = "");
	~GameWindow()
	{
		delete widget;
	}

	QQuickItem* rootObject() {
		return widget->rootObject();
	}

	QString icon()
	{
		//qDebug() << windowIcon;
		return windowIcon;
	}

private:
	QQuickWidget* widget;

	QString Name;
	QString windowIcon;

	void save(QString name);
	void open(QString name);

signals:
	void close();
	
protected:
	void closeEvent(QCloseEvent *event);

private slots:
	void onClose();
};
