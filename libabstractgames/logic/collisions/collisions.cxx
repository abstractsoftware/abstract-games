#include "collisions.h"
#include <QtQml>
#include <QDebug>
#include <QTextStream>
#include <QVariant>
#include <QJSValue>

Collisions::Collisions() : QObject()
{
}

bool Collisions::isColliding(QQuickItem* item1, QQuickItem* item2)
{
	double p1 = item1->x() >= item2->x() && item1->x() < item2->x() + item2->width() && 
		item1->y() >= item2->y() && item1->y() < item2->y() + item2->height();

	double p2 = item1->x() + item1->width() >= item2->x() && item1->x() + item1->width() < item2->x() + item2->width() && 
		item1->y() >= item2->y() && item1->y() < item2->y() + item2->height();

	double p3 = item1->x() >= item2->x() && item1->x() < item2->x() + item2->width() &&
                item1->y() + item1->height() >= item2->y() && item1->y() + item2->height() < item2->y() + item2->height();

	double p4 = item1->x() + item1->width() >= item2->x() && item1->x() + item1->width() < item2->x() + item2->width() &&
                item1->y() + item1->height() >= item2->y() && item1->y() + item1->height() < item2->y() + item2->height();

	return p1 || p2 || p3 || p4;
}

QJSValue collisions(QQmlEngine *engine, QJSEngine *scriptEngine)
{
	Q_UNUSED(engine);

	Collisions* collisions = new Collisions;
	QJSValue _module = scriptEngine->newQObject(collisions);

	return _module;
}

