#include "intersecting.h"
#include <QtQml>
#include <QDebug>
#include <QPoint>
#include <QTextStream>
#include <QVariant>
#include <QJSValue>

Intersecting::Intersecting() : QObject()
{
}

bool Intersecting::isIntersecting(QQuickItem* item1, QQuickItem* item2)
{

	for (int x = 0; x < item1->width(); x++)
	{
		for (int y = 0; y < item1->height(); y++)
		{
			if (item2->contains(item1->mapToItem(item2, QPointF(x, y))))
			{
				return true;
			}
		}
	}

	return false;
}

QJSValue intersecting(QQmlEngine *engine, QJSEngine *scriptEngine)
{
	Q_UNUSED(engine);

	Intersecting* intersecting = new Intersecting;
	QJSValue _module = scriptEngine->newQObject(intersecting);

	return _module;
}

