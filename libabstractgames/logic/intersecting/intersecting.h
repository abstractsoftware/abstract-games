#include <QJSValue>
#include <QQmlEngine>
#include <QJSValue>
#include <QQuickItem>

class Intersecting : public QObject {
	Q_OBJECT

public:
	Intersecting();
	Q_INVOKABLE bool isIntersecting(QQuickItem* item1, QQuickItem* item2);
};

QJSValue intersecting(QQmlEngine *engine, QJSEngine *scriptEngine);
