#pragma once
#include "collisions/collisions.h"
#include "intersecting/intersecting.h"

void RegisterLogic()
{
        qmlRegisterSingletonType("AbstractGames.Logic", 1, 0, "Collisions", collisions);
	qmlRegisterSingletonType("AbstractGames.Logic", 1, 0, "Intersecting", intersecting);
}
