import QtQuick 2.7
import AbstractGames.Logic 1.0 as Logic
import AbstractGames.Widgets 1.0
import "game.js" as Game

SimpleGameWidget {
	id: gamewidget

	onGameRestarted: {
		Game.Restart();
	}

	// Game Graphics
}
