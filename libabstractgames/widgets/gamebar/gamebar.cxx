#include "gamebar.h"

void GameBar::componentComplete()
{
	QQuickItem::componentComplete();

        QQmlEngine* engine = qmlEngine(this);
        QQmlComponent component(engine);
        component.loadUrl(QUrl("qrc:/qml/GameBar.qml"));
        gameBar = qobject_cast<QQuickItem *>(component.create());
        gameBar->setParentItem(this);
        gameBar->setWidth(600);
        gameBar->setHeight(600);
        gameBar->setZ(10000); // to ensure it is above all other widgets
	gameBar->setProperty("buttonImages", buttonImages);
	gameBar->setProperty("buttonPresses", buttonPresses);
	gameBar->setProperty("buttonReleases", buttonReleases);
}
