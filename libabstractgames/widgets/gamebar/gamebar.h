#pragma once
#include <QDebug>
#include <QQuickItem>
#include <QUrl>
#include <QVariant>

class GameBar : public QQuickItem {
	Q_OBJECT

	QQuickItem* gameBar;

	QVariant buttonImages;
	QVariant buttonReleases;
	QVariant buttonPresses;

	Q_PROPERTY(QString text READ getText WRITE setText NOTIFY textChanged)
	Q_PROPERTY(QVariant buttonImages READ getButtonImages WRITE setButtonImages NOTIFY buttonImagesChanged)
	Q_PROPERTY(QVariant buttonPresses READ getButtonPresses WRITE setButtonPresses NOTIFY buttonPressesChanged)
	Q_PROPERTY(QVariant buttonReleases READ getButtonReleases WRITE setButtonReleases NOTIFY buttonReleasesChanged)
protected:

	void componentComplete();

public:
	GameBar()
	{
		gameBar = nullptr;
	}

	~GameBar()
	{
		if (gameBar) delete gameBar;
	}

	QString getText() const {
                if (!gameBar) return "";;
                return gameBar->property("text").value<QString>();     
        }

        void setText(QString _text)
        {
                if (!gameBar) return;
                gameBar->setProperty("text", _text);
        }

	QVariant getButtonImages() const {
		if (!gameBar) return QVariant();
		return gameBar->property("buttonImages"); 
	}

	void setButtonImages(QVariant _buttonImages)
	{
		buttonImages = _buttonImages;
	}

	QVariant getButtonPresses() const {
                if (!gameBar) return QVariant();
                return gameBar->property("buttonPresses");
        }

        void setButtonPresses(QVariant _buttonImages)
        {
		buttonPresses = _buttonImages;
                //if (!gameBar) return;
                //gameBar->setProperty("buttonPresses", _buttonImages);
        }

	QVariant getButtonReleases() const {
                if (!gameBar) return QVariant();
                return gameBar->property("buttonReleases");
        }

        void setButtonReleases(QVariant _buttonReleases)
        {
		buttonReleases = _buttonReleases;
        }

	enum Icon { Left, Right, Up, Down };
        Q_ENUM(Icon)

signals:
	void textChanged();
	void buttonImagesChanged();
        void buttonPressesChanged();
        void buttonReleasesChanged();
};
