import QtQuick 2.7

Rectangle {
	id: bar
	radius: 3
	color: "transparent"
	width: 600
	height: 35

	property string text: "Some Text Here!"
	property var buttonImages
	property var buttonPresses
	property var buttonReleases

	Row {
		id: row
		anchors.left: parent.left
		anchors.right: parent.right
		anchors.rightMargin: parent.width * 0.25
		spacing: 6
		leftPadding: 10
		smooth: true
		topPadding: 20

		Repeater {
			model: bar.buttonImages ? bar.buttonImages.length : 0

			Image {
				source: bar.buttonImages[index]   == 1 ? "images/arrow-left.svg"
					: bar.buttonImages[index] == 2 ? "images/arrow-right.svg" 
					: bar.buttonImages[index] == 3 ? "images/arrow-up.svg"
					: "images/arrow-down.svg"

				z: 500
				width: 15
				height: 15
				anchors.verticalCenter: parent.verticalCenter

				MouseArea
				{
					anchors.fill: parent

					onPressed: {
						bar.buttonPresses[index]();
					}

					onReleased: {
						bar.buttonReleases[index]();
					}
				}
			}
		}
	}

	Text
	{
		anchors.right: parent.right
		anchors.rightMargin: 10
		text: bar.text
		font.family: "Ubuntu"
		font.pixelSize: 15
		z: 5000
		anchors.verticalCenter: row.verticalCenter
	}
}
