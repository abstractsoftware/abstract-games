#include "gamecontroller.h"
#include <QDebug>
#include <QUrl>
#include <QtQml>
#include <QJSValue>
#include <QList>
#include <QVariant>
#include <QSettings>
#include <QQmlListProperty>

QVariantMap SerializeQObject(QObject* item)
{
	QVariantMap map;

	if (item == 0) return map;

	QStringList properties = item->property("properties").value<QStringList>();

	properties.append("data");

	for (int i = 0; i < item->metaObject()->propertyCount(); i++)
	{
		QString name = item->metaObject()->property(i).name();
		QVariant property = item->metaObject()->property(i).read(item);

		if (properties.contains(name))
		{
			if (property.canConvert<QQmlListProperty<QObject>>())
			{
				QVariantList list;
	
				QQmlListReference qmllist = QQmlListReference(item, qPrintable(name));

				for (int i = 0; i < qmllist.count(); i++)
				{
					QObject* obj = qmllist.at(i);

					list.append(SerializeQObject(obj));
				}

				map[name] = list;
			}
			else
			{
				map[name] = property;
			}
		}
	}

	return map;
}

void DeserializeQObject(QVariantMap map, QObject* item)
{
	QVariantMap::iterator it;

	for (it = map.begin(); it != map.end(); it++)
	{
		if (it.value().canConvert<QVariantList>())
		{
			QVariantList list;
			QVariantList objlist = it.value().value<QVariantList>();

			for (QVariantList::iterator it3 = objlist.begin(); it3 != objlist.end(); it3++)
			{
				QVariant obj = *it3;
				QQmlListReference qmllist = QQmlListReference(item, qPrintable(it.key()));

				if (obj.canConvert<QVariantMap>())
				{
					QObject* _ = qmllist.at(it3 - objlist.begin());
					DeserializeQObject(obj.value<QVariantMap>(), _);
					list.append(QVariant::fromValue(_));
				}
				else
				{
					list.append(obj);
				}
			}

			item->setProperty(qPrintable(it.key()), list);
		}
		else
		{
			item->setProperty(qPrintable(it.key()), it.value());
		}
	}
}

GameController::GameController() : QQuickItem(), background { nullptr }, gameType { "" }, name { "" }
{
	_error = false;
	QSettings settings;
	settings.beginGroup("abstractgames");
	defaultDifficulty = settings.value("defaultDifficulty", "medium").toString();
	defaultPlayers = settings.value("defaultPlayers", "one").toString();
	settings.endGroup();

	startupScreen = nullptr;
	gameWidget = nullptr;
	swipeview = nullptr;
	//thingie = nullptr;
	setGameFileError(0);
}

QString GameController::getgameType()
{
	return gameType;
}

void GameController::setgameType(QString _gameType)
{
	gameType = _gameType;
	emit gameTypeChanged(_gameType);

	createGame();
}

QString GameController::getname()
{
	return name;
}

void GameController::setname(QString _name)
{       
	name = _name;
	emit nameChanged(_name);
	
	createGame();
}

QString GameController::getDifficulty()
{
	return difficulty;
}

void GameController::setDifficulty(QString _difficulty)
{
	difficulty = _difficulty;

	gameWidget->setDifficulty(difficulty);
	emit difficultyChanged(_difficulty);
}

QString GameController::getPlayers()
{
	return players;
}

void GameController::setPlayers(QString _players)
{
	players = _players;
	
	gameWidget->setPlayers(players);
	emit playersChanged(_players);
}

void GameController::createGame()
{
	if (gameType != "" && name != "")
	{
		QQmlEngine* engine = qmlEngine(this);
		QQmlComponent component(engine);

		if (gameType != "None")
		{
			component.loadUrl(QUrl("qrc:/qml/SwipeView.qml"));
			swipeview = qobject_cast<QQuickItem *>(component.create());

			swipeview->setParentItem(this);
			swipeview->setWidth(600);
			swipeview->setHeight(600);
		
			if (gameType == "ArcadeGame")
			{
				component.loadUrl(QUrl("qrc:/qml/SinglePlayerStartupScreen.qml"));
			}
			else if (gameType == "BoardGame")
			{
				component.loadUrl(QUrl("qrc:/qml/MutiplePlayerStartupScreen.qml"));
			}
			else
			{
				QtQml::qmlWarning(this) << "Invaild value for property \"gameType\" - must be \"ArcadeGame\".";
			}

			startupScreen = qobject_cast<QQuickItem *>(component.create());

			startupScreen->setParentItem(swipeview);
			startupScreen->setWidth(600);
			startupScreen->setHeight(600);

			component.loadUrl(QUrl(qmlEngine(this)->baseUrl().toString() + "/GameWidget.qml"));
	
			gameWidget = qobject_cast<SimpleGameWidget *>(component.create());

			if (component.isError())
			{
				for (auto error : component.errors())
					QtQml::qmlWarning(this) << error;

				delete swipeview; delete startupScreen;
				_error = true;
				return;
			}

			gameWidget->setParentItem(swipeview);
			gameWidget->setWidth(600);
			gameWidget->setHeight(600);

			connect(startupScreen, SIGNAL(newGame()), SLOT(newGame()));
			connect(startupScreen, SIGNAL(restartGame()), SLOT(restartGame()));

			connect(startupScreen, SIGNAL(save(QString)), SLOT(save(QString)));
			connect(startupScreen, SIGNAL(open(QString)), SLOT(open(QString)));

			connect(gameWidget, SIGNAL(endGame(QString)), SLOT(endGame(QString)));
			connect(gameWidget, SIGNAL(updateStatus(QString)), SLOT(updateStatus(QString)));

			connect(gameWidget, &SimpleGameWidget::wantsToBeOpened, this, &GameController::open);
			connect(gameWidget, &SimpleGameWidget::wantsToBeSaved, this, &GameController::save);

			connect(swipeview, SIGNAL(pauseGame()), SLOT(pauseGame()));
			connect(swipeview, SIGNAL(newGame()), SLOT(newGame()));
			connect(gameWidget, SIGNAL(pauseGame()), SLOT(pauseGame()));

			if (gameType == "ArcadeGame")
			{
				connect(startupScreen, SIGNAL(difficultyChanged(QString)), SLOT(setDifficulty(QString)));

				if (defaultDifficulty == "easy")
				{
					startupScreen->setProperty("easy", true);
					startupScreen->setProperty("medium", false);
				}
				else if (defaultDifficulty == "medium")
				{
					startupScreen->setProperty("easy", false);
					startupScreen->setProperty("medium", true);
				}
				else if (defaultDifficulty == "hard")
				{
					startupScreen->setProperty("easy", false);
					startupScreen->setProperty("medium", false);
				}
			
				if (startupScreen->property("easy").value<bool>() == true)
				{
					difficulty = "easy";
				}
				else if (startupScreen->property("medium").value<bool>() == true)
				{
					difficulty = "medium";
				}
				else
				{
					difficulty = "hard";
				}

				gameWidget->setDifficulty(difficulty);
				emit difficultyChanged(difficulty);
			}
			else if (gameType == "BoardGame")
			{
				connect(startupScreen, SIGNAL(playersChanged(QString)), SLOT(setPlayers(QString)));

				if (defaultPlayers == "one")
				{
					startupScreen->setProperty("spectrumontwo", true);
				}
				else if (defaultPlayers == "two")
				{
					startupScreen->setProperty("spectrumontwo", false);
				}

				if (startupScreen->property("spectrumontwo").value<bool>() == true)
				{
					players = "one";
				}
				else
				{
					players = "one";
				}

				gameWidget->setPlayers(players);
			}

			startupScreen->setProperty("theme", "Dark");
			startupScreen->setProperty("name", name);

			if (background)
			{
				QVariant xx = QVariant::fromValue(background);
				swipeview->setProperty("back", xx);
			}

			//open("game.save");
		} else {
			component.loadUrl(QUrl(qmlEngine(this)->baseUrl().toString() + "/GameWidget.qml"));

			gameWidget = qobject_cast<SimpleGameWidget *>(component.create());

			if (component.isError())
			{
				for (auto error : component.errors())
					QtQml::qmlWarning(this) << error;

				delete swipeview; delete startupScreen;
				_error = true;
				return;
			}

	       		gameWidget->setParentItem(this);
	       		gameWidget->setWidth(600);
	       		gameWidget->setHeight(600);

			startupScreen = nullptr;
			swipeview = nullptr;

			connect(gameWidget, SIGNAL(endGame(QString)), SLOT(endGame(QString)));
			connect(gameWidget, SIGNAL(updateStatus(QString)), SLOT(updateStatus(QString)));

			restartGame();
		}
	}
}

void GameController::newGame()
{
	gameWidget->new_();

	startupScreen->setProperty("theme", "Dark");

	gameWidget->dialog->setProperty("theme", startupScreen->property("theme").value<QString>());
	gameWidget->dialog2->setProperty("theme", startupScreen->property("theme").value<QString>());

	gameWidget->dialog->setProperty("accent", startupScreen->property("accent").value<int>());
	gameWidget->dialog2->setProperty("accent", startupScreen->property("accent").value<int>());

	if (!startupScreen->property("pause").value<bool>())
	{
		gameWidget->restart();
	}

	startupScreen->setProperty("pause", false);
	swipeview->setProperty("currentIndex", 1);
}

void GameController::restartGame()
{
	gameWidget->restart();

	if (startupScreen && swipeview)
	{
		startupScreen->setProperty("pause", false);
		swipeview->setProperty("currentIndex", 1);
	}
}

void GameController::pauseGame()
{
	if (startupScreen->property("pause").value<bool>() == true) return;

	gameWidget->pause();

	startupScreen->setProperty("pause", true);
	swipeview->setProperty("currentIndex", 0);
}

void GameController::endGame(QString text)
{
	if (startupScreen)
	{
		if (startupScreen->property("pause").value<bool>()) return;
	}

	gameWidget->dialog->setProperty("text", text);
	gameWidget->dialog->setProperty("visible", true);

	while (true)
	{
		qApp->processEvents();

		if (gameWidget->dialog->property("visible") == false)
		{
			break;
		}
	}

	gameWidget->end(text);

	if (startupScreen && swipeview)
	{
		swipeview->setProperty("currentIndex", 0);
		startupScreen->setProperty("pause", false);
	}
	else
	{
		restartGame();
	}
}

void GameController::updateStatus(QString text)
{
	if (startupScreen)
	{
		if (startupScreen->property("pause").value<bool>()) return;
	}

	gameWidget->dialog->setProperty("text", text);
	gameWidget->dialog->setProperty("visible", true);

	while (true)
	{
		qApp->processEvents();

		if (gameWidget->dialog->property("visible") == false)
		{
			return;
		}
	}

	gameWidget->statusWasUpdated(text);
}

bool GameController::closeDialog()
{
	//save("game.save");

	if (gameWidget->dialog2->property("visible").toBool() == true)
	{
		return false;
	}

	gameWidget->pause();

	if (startupScreen)
	{
		if (swipeview->property("currentIndex").value<int>() == 0) return true;
	}

	gameWidget->dialog2->setProperty("text", "You're sure you want to close?");
	gameWidget->dialog2->setProperty("visible", true);

	while (true)
	{
		qApp->processEvents();

		if (gameWidget->dialog2->property("visible") == false)
		{
			if (gameWidget->dialog2->property("result").value<int>() == 1) return true;
			else
			{
				gameWidget->new_();
				return false;
			}
		}
	}

	return true;
}

void GameController::setBackground(QQuickItem* _background)
{
	background  = _background;
}

void GameController::save(QString name)
{
	QVariantMap data = SerializeQObject(gameWidget);

	QFile outfile(name);
	if (!outfile.open(QIODevice::WriteOnly))
	{
		qCritical("error: `%s' cannot be opened", qUtf8Printable(name));
		setGameFileError(1);
		return;
	}

	QDataStream out(&outfile);

	out << "ABSTRACTGAMES";
	out << (quint32) 1;

	out.setVersion(QDataStream::Qt_5_13);

	out << this->name;
	out << difficulty;
	out << players;
	out << data;

	outfile.close();

	setGameFileError(0);
	return;
}

void GameController::open(QString name)
{
	QVariantMap data;

	QFile infile(name);
	if (!infile.open(QIODevice::ReadOnly))
	{
		qCritical("error: `%s' cannot be opened", qUtf8Printable(name));
		setGameFileError(1);
		return;
	}

	QDataStream in(&infile);

	char* magic = new char;
	quint32 version;

	in >> magic;
	in >> version;

	if (QByteArray(magic) == "ABSTRACTGAMES")
	{
		if (version == 1) 
		{
			QString _name;
			in.setVersion(QDataStream::Qt_5_13);

			in >> _name;

			if (_name != this->name)
			{
				qCritical("error: `%s' was not saved by this game", qUtf8Printable(name));
				setGameFileError(2);
				return;
			}

			QString _difficulty;
			QString _players;

			in >> _difficulty;
			in >> _players;

			setDifficulty(_difficulty);
			setPlayers(_players);

			in >> data;
		}
		else
		{
			qCritical("error: `%s' is either too old or too new", qUtf8Printable(name));
			setGameFileError(3);
			return;
		}

		swipeview->setProperty("currentIndex", 1);
		restartGame();

		DeserializeQObject(data, gameWidget);
	}

	infile.close();

	delete magic;
	setGameFileError(0);
	return;
}
