#pragma once
#include <QVariant>
#include <QQuickItem>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QJSValue>
#include "../simplegamewidget/simplegamewidget.h"

class GameController : public QQuickItem
{
	Q_OBJECT

	Q_PROPERTY(QString gameType READ getgameType WRITE setgameType NOTIFY gameTypeChanged)
	Q_PROPERTY(QString name READ getname WRITE setname NOTIFY nameChanged)
	Q_PROPERTY(QString difficulty READ getDifficulty NOTIFY difficultyChanged)
	Q_PROPERTY(QString players READ getPlayers NOTIFY playersChanged)
	Q_PROPERTY(QQuickItem* background READ getBackground WRITE setBackground NOTIFY backgroundChanged)
	Q_PROPERTY(bool error READ getError NOTIFY errorChanged)

public:
	bool getError()
	{
		return _error;
	}

        GameController();
	~GameController()
	{
		if (swipeview) delete swipeview;
		if (background) delete background;
		if (startupScreen) delete startupScreen;
		if (gameWidget) delete gameWidget;
	}


	QString getgameType();
	QString getDifficulty();
	QString getPlayers();
	QString getname();

	QQuickItem* getBackground()
	{
		return background;
	}

	void setBackground(QQuickItem* _background);

	Q_INVOKABLE bool closeDialog();

	QQuickItem* background;
	QString defaultDifficulty;
	QString defaultPlayers;

	SimpleGameWidget* game() {
		return gameWidget;
	}

	QQuickItem* swipeview;

signals:
	void gameTypeChanged(QString gameType);
	void difficultyChanged(QString difficulty);
	void playersChanged(QString players);
	void nameChanged(QString name);
	void closeDialogChanged(QJSValue closeDialog);
	void backgroundChanged(QQuickItem* _background);
	void errorChanged();

private:
	QString gameType;
	QString difficulty;
	QString players;
	QString name;

	SimpleGameWidget* gameWidget;
	QQuickItem* startupScreen;
	//QQuickItem* thingie;

	void createGame();

	bool _error;
	int gamefileError;

	void setGameFileError(int _)
	{
		gamefileError = _;
		if (startupScreen) startupScreen->setProperty("gamefileError", _);
	}

public slots:
	void save(QString name);
	void open(QString name);

public slots:
	void newGame();
	void restartGame();
	void pauseGame();
	void endGame(QString text);
	//void continueGame();
	void updateStatus(QString text);

	void setDifficulty(QString _difficulty);
	void setgameType(QString _gameType);
	void setPlayers(QString _players);
	void setname(QString _name);
};
