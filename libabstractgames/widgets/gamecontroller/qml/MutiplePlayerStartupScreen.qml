import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.1

Control {
	leftPadding: 0
	topPadding: 0
	rightPadding: 0
	bottomPadding: 0
	width: 600
	height: 600
       	signal newGame()
	signal restartGame()
	signal displayText(string text)
	signal update()

	signal open(string s)
	signal save(string s)

	signal playersChanged(string text)

	property string name
	property string theme: tiles.theme
	property int accent: tiles.accent
   
       	Material.accent: accent
	Material.theme: theme

	property int gamefileError: 0

	MouseArea
	{
		anchors.fill: parent

		onClicked:
		{
			tiles._menu.dismiss();
		}
	}


	id: msg

	property bool spectrumonone: false 
	property bool spectrumontwo: true
       	property string text: "Click Here to Begin a Game"
	property bool pause: false

	Column {
		anchors.centerIn: parent
		anchors.fill: parent

		Tiles
		{
			pause: msg.pause
			width: msg.width 
			height: msg.height / 2
			//pause: msg.pause
			name: msg.name
			id: tiles

			spinbox: QtObject { property var model: ["One", "Two"]; property var valueModified: function (index) 
				{
					if (index == 0)
                                	{
                                		msg.spectrumontwo = true;
                                	        msg.playersChanged("one");
                                	}
                                	else 
					{	
						msg.spectrumontwo = false;
						msg.playersChanged("two");
					}

				}
	
				property var value: msg.spectrumontwo ? 0 : 1
				property var heading: "Players:"
			}

			onNewGame:
			{
				msg.newGame();
			}

			onRestartGame:
			{
				msg.restartGame();
			}

			onOpen: {
				msg.open(s);
			}

			onSave: {
				msg.save(s);
			}

			gamefileError: msg.gamefileError
		}
	}
}
