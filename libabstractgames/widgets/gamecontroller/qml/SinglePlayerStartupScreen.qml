import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.1

Control {
	leftPadding: 0
	topPadding: 0
	rightPadding: 0
	bottomPadding: 0
	width: 600
	height: 600
        signal newGame()
	signal restartGame()
	signal displayText(string text)
	signal update()

	signal difficultyChanged(string text)

	signal open(string s)
	signal save(string s)

	property string name
	property int accent: tiles.accent
	property string theme: tiles.theme
   
       	Material.accent: accent
	Material.theme: theme

	property int gamefileError: 0
	
	MouseArea
	{
		anchors.fill: parent

		onClicked:
		{
			tiles._menu.dismiss();
		}
	}


	id: msg

	property bool easy: false 
	property bool medium: true
       	property string text: "Click Here to Begin a Game"
	property bool pause: false

	Column {
		anchors.centerIn: parent
		anchors.fill: parent

		Tiles {
			pause: msg.pause
			width: msg.width 
			//height: msg.height / 1.5
			name: msg.name
			id: tiles
			currentIndex: 3

			spinbox: QtObject {
				property var model: ["Easy", "Medium", "Hard"]
				//property var index: 2//msg.easy ? 1 : msg.medium ? 2 : 3
				property var valueModified: function (index) 
				{
					if (index == 0)
                                        {
                                        	msg.easy = true;
                                                msg.medium = false;
                                                msg.difficultyChanged("easy");
                                         }
                                         else if (index == 1)
                                         {
                                                msg.medium = true;
                                                msg.easy = false;
                                                msg.difficultyChanged("medium");
                                          }
                                          else
                                          {     msg.medium = false;
                                                msg.easy = false;
                                                msg.difficultyChanged("hard");
                                          }

				}

				property var value: msg.easy ? 0 : msg.medium ? 1 : 2
				property var heading: "Difficulty:"

			}

			onOpen:
			{
				msg.open(s);
			}

			onSave:
			{
				msg.save(s);
			}

			onNewGame:
			{
				msg.newGame();
			}

			onRestartGame:
			{
				msg.restartGame();
			}

			gamefileError: msg.gamefileError
		}
	}
}
