import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.1

SwipeView {
	id:swipeview
	signal pauseGame()
	signal newGame()

	font.family: "Ubuntu"
	font.pointSize: 12

	property var back: null

	background: back

	QtObject {
		id: qtobject
		property int theIndex: swipeview.currentIndex
		
		Behavior on theIndex {
			ScriptAction {
				script: {
					if (qtobject.theIndex == 1) {
						swipeview.pauseGame();
					}
					else if (qtobject.theIndex == 0)
					{
						swipeview.newGame();
					}
				}
			}
		}
	}
}
