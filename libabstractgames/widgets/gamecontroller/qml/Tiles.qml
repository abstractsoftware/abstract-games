import QtQuick 2.7
import QtQuick.Layouts 1.7
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.1
import QtGraphicalEffects 1.9

Control
{
	leftPadding: 0
        topPadding: 0
        rightPadding: 0
	bottomPadding: 0
	property string theme: "Light"
	property int accent: Material.Green
	property int currentIndex: 3//ui.currentIndex
	Material.primary: Material.Teal

	id: tiles
	property var spinbox

	width: 360
	height: 360

	property bool pause: false
	property var _menu: menu
	property string name: ""

	signal newGame()
	signal restartGame()
	signal open(string s)
	signal save(string s)

	property int gamefileError

	ColumnLayout {
		anchors.fill: parent
		Control {
			id: i
			z: 1
			Material.theme: "Light"
			height: 50
			Layout.fillWidth: true

			Rectangle {
				height: 1
				width: parent.width - 10
				color: "black"
				x: 5
				y: parent.height

				layer.enabled: false
				layer.effect: DropShadow {
					verticalOffset: 1
					//color: "#330066ff"
					samples: 17
					spread: 0.5
				}
			}

			RowLayout {
				ToolButton
				{
					font.family: "Ubuntu"
					icon.source: "images/start.svg"
					width: 50
					height: 50

					onClicked:
					{
						if (tiles.pause)
						{
							if (!menu.opened)
							{
								menu.popup();
							}
							else
							{
								menu.dismiss();
							}
						}
						else
						{
							tiles.newGame();
						}
					}

					Menu
					{
						font.family: "Ubuntu"
						id: menu
						MenuItem
						{
							text: "Resume"

							onTriggered:
							{
								tiles.newGame();
							}
						}

						MenuItem
						{
							text: "Restart"

							onTriggered:
							{
								tiles.restartGame();
							}
						}
					}
				}

				Label { text: name; font.family: "Ubuntu"; font.pointSize: 12; }

			}

		}

	 	ButtonGroup {
      	 		id: buttonGroup
  	 	}


		Label {
			Material.theme: "Light"
			id: bt
			leftPadding: 15
		  	topPadding: 10
		  	height: 50
		  	font.pointSize: 15
		  	font.family: "Ubuntu"
		  	text: spinbox.heading
		  	Layout.fillWidth: true
	 	}

	 	Item {
	 		height: 5
	 	}

	 	ListView {
			Material.theme: "Light"
			id: ui
	        	Layout.fillWidth: true
			Layout.fillHeight: true

	          	model: spinbox.model
			delegate: RadioDelegate {
		 		font.pointSize: 12
                 	 	font.family: "Ubuntu"

			  	width: 600*0.25
			  	text: modelData
			  	checked: index == spinbox.value
			  	ButtonGroup.group: buttonGroup

	  	  	  	onClicked: {
					spinbox.valueModified(index);
	 		 	}
      		  	}
		}

		TextField {
			Material.theme: Material.Light
			Material.accent: tiles.accent
			id: filename
			leftInset: 10
			leftPadding: 17
			Layout.fillWidth: true
			Layout.rightMargin: 20
		}

		RowLayout {
			Layout.leftMargin: 10
			DelayButton {
				Material.theme: Material.Light; Material.accent: tiles.accent
				text: "Open Game"

				onClicked: {
					tiles.open(filename.displayText);
				}
			}

			DelayButton {
				Material.theme: Material.Light; Material.accent: tiles.accent
				text: "Save Game"

				onClicked: {
					tiles.save(filename.displayText);
				}
			}

			Text {
				Layout.leftMargin: 10
				font.family: "Ubuntu"
				font.pixelSize: 15
				text:   if (gamefileError == 0) "No Errors"
					else if (gamefileError == 1) "Error - Cannot Open File"
					else if (gamefileError == 2) "Error - File is for Different Game"
					else if (gamefileError == 3) "Error - File is Too Old or Too New"
			}
		}
	}
}
