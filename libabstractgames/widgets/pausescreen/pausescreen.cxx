#include "pausescreen.h"
#include "../simplegamewidget/simplegamewidget.h"
#include <QDebug>
#include <QVariant>

void PauseScreen::componentComplete()
{
	QQuickItem::componentComplete();

	QQmlEngine* engine = qmlEngine(this);
        QQmlComponent component(engine);
        component.loadUrl(QUrl("qrc:/qml/PauseScreen.qml"));
        pausescreen = qobject_cast<QQuickItem *>(component.create());
	pausescreen->setParentItem(this);
        pausescreen->setWidth(600);
        pausescreen->setHeight(600);
	pausescreen->setZ(10000); // to ensure it is above all other widgets

	pausescreen->setProperty("active", true);

	connect(pausescreen, SIGNAL(onPause()), SLOT(onPause()));
	connect(pausescreen, SIGNAL(onContinue()), SLOT(onContinue()));

	//SimpleGameWidget* x = qobject_cast<SimpleGameWidget *>(this->parentItem());
}

void PauseScreen::onPause()
{

	SimpleGameWidget* x = qobject_cast<SimpleGameWidget *>(this->parentItem());

	x->pause();
}

void PauseScreen::onContinue()
{
	SimpleGameWidget* x = qobject_cast<SimpleGameWidget *>(this->parentItem());

        x->new_();
}
