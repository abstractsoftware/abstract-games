#pragma once
#include <QQuickItem>
#include <QUrl>
#include <QVariant>

class PauseScreen : public QQuickItem {
	Q_OBJECT

	QQuickItem* pausescreen;

	Q_PROPERTY(bool containsMouse READ getContainsMouse WRITE setContainsMouse NOTIFY containsMouseChanged)
protected:

	void componentComplete();

public:
	bool getContainsMouse() const {
		if (!pausescreen) return false;
		return pausescreen->property("containsMouse").value<bool>(); 
	}

	void setContainsMouse(bool _containsMouse)
	{
		if (!pausescreen) return;
		pausescreen->setProperty("containsMouse", _containsMouse);
	}

public slots:
	void onPause();
	void onContinue();

signals:
	void containsMouseChanged(bool _containsMouse);

public:
	//Q_INVOKABLE bool notContainsMouse();
};
