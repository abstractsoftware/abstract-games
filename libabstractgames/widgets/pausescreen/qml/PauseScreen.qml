import QtQuick 2.9

Rectangle {
	id: thingie1
	anchors.fill: parent
	color: "#00000000"
	
	signal onPause()
	signal onContinue()
	property bool alreadyPause: false
	property bool alreadyContinue: false
	property bool containsMouse
	property bool active: true

	Timer {
        	id: timer
        	repeat: true
		interval: 1
		running: true

    	        onTriggered:
		{
			if (!active)
			{
				thingie.visible = false;
				return;
			}

               		if (Qt.application.state == Qt.ApplicationInactive || !containsMouse)
			{
				thingie.visible = true;
				if (!thingie1.alreadyPause)
				{
					thingie1.onPause();
				}

				thingie1.alreadyPause = true;
				thingie1.alreadyContinue = false;
                	}
                	else
                	{
				thingie.visible = false;

				if (!thingie1.alreadyContinue)
				{
					thingie1.onContinue();
				}

				thingie1.alreadyPause = false;
                                thingie1.alreadyContinue = true;
                	}
        	}
	}

	Rectangle {
		id: thingie
		color: "#555555"
		opacity: 0.5
		anchors.fill: parent
	        Text {
        	        anchors.centerIn: parent
        	        text: "It looks like your window or mouse cursor isn't in focus any more."
        	        font.family: "Ubuntu"
       	 	        font.pointSize: 12
        	}
	}
}
