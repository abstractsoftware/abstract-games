import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.3

Rectangle {
	id: item
	property bool containsMouse: x.containsMouse

	color: "transparent"
	z: -10

	signal clicked(double x, double y)

	Dialog {
		id: dialog
		property string theme: "Dark"
		property int accent
                anchors.centerIn: parent
                modal: true
		visible: false
		z: 10000

                Material.accent: accent
                Material.theme: theme
		standardButtons: Dialog.Ok

		objectName: "dialog"

                property string text: "You won!"

                Label { text: dialog.text; anchors.fill: parent }
	}

	Dialog {
		id: dialog2
		property string theme: "Dark"
		property int accent
		objectName: "dialog2"

		anchors.centerIn: parent
		modal: true
                visible: false
                z: 10000

                Material.accent: accent
                Material.theme: theme
                standardButtons: Dialog.Yes | Dialog.No

                property string text: "This will close all running games."

                Label { text: dialog2.text; anchors.fill: parent }
	}

	MouseArea {
		id: x
		anchors.fill: parent
		hoverEnabled: true
		//propagateComposedEvents: true

		onClicked:
		{
			mouse.accepted = false;
			item.clicked(mouse.x,mouse.y);
		}
	}
}
