#include "simplegamewidget.h"
#include <QDebug>
#include <QUrl>
#include <QJSValue>
#include <QVariant>
#include <QTest>

SimpleGameWidget::SimpleGameWidget() : QQuickItem()
{
	players = "one";
	setPlayers(players);
	difficulty = "medium";
	setDifficulty(difficulty);
}

void SimpleGameWidget::componentComplete()
{
	QQuickItem::componentComplete();

	QQmlEngine* engine = qmlEngine(this);
	QQmlComponent component(engine);
	component.loadUrl(QUrl("qrc:/qml/SimpleGameWidget.qml"));
	mousearea = qobject_cast<QQuickItem *>(component.create());

	mousearea->setParentItem(this);
	mousearea->setWidth(600);
	mousearea->setHeight(600);

	dialog = mousearea->findChild<QObject*>("dialog");
	dialog2 = mousearea->findChild<QObject*>("dialog2");

	connect(mousearea, SIGNAL(clicked(double,double)), this, SLOT(emitClicked(double,double)));
}

bool SimpleGameWidget::getContainsMouse()
{
	if (!mousearea) 
	{
		return false;
	}

	return mousearea->property("containsMouse").toBool();
}

void SimpleGameWidget::emitPauseGame()
{
	emit pauseGame();
}

QString SimpleGameWidget::getDifficulty()
{
        return difficulty;
}

void SimpleGameWidget::setDifficulty(QString _difficulty)
{
        difficulty = _difficulty;

        emit difficultyChanged(_difficulty);
}

QString SimpleGameWidget::getPlayers()
{
        return players;
}

void SimpleGameWidget::setPlayers(QString _players)
{
	players = _players;
        //players = "two";//_players; TODO fix bug here

        emit playersChanged(players);
}

void SimpleGameWidget::emitClicked(double x, double y)
{
	emit clicked(x, y);
}

void SimpleGameWidget::restart()
{
	emit gameRestarted();
}

void SimpleGameWidget::pause()
{
        emit gamePaused();
}

void SimpleGameWidget::end(QString text)
{
        emit gameEnded(text);
}

void SimpleGameWidget::new_()
{
        emit gameStarted();
}

void SimpleGameWidget::statusWasUpdated(QString text)
{
	emit statusUpdated(text);
}

void SimpleGameWidget::update()
{
	qApp->processEvents();
}

void SimpleGameWidget::sleep(int ms)
{
	QTest::qSleep(ms);
}

void SimpleGameWidget::wait(int ms)
{
	QTest::qWait(ms);
}
