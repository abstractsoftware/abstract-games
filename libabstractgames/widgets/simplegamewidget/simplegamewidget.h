#pragma once
#include <QQuickItem>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QMouseEvent>

class SimpleGameWidget : public QQuickItem
{
	Q_OBJECT

	Q_PROPERTY(QString difficulty READ getDifficulty NOTIFY difficultyChanged)
	Q_PROPERTY(QString players READ getPlayers NOTIFY playersChanged)
	Q_PROPERTY(bool containsMouse READ getContainsMouse NOTIFY containsMouseChanged)

public:
        SimpleGameWidget();
	~SimpleGameWidget()
	{
		delete dialog;
		delete dialog2;
		delete mousearea;
	}


	QString getDifficulty();
	QString getPlayers();
	bool getContainsMouse();

	void restart();
	void pause();
	void end(QString text);
	void new_();
	void statusWasUpdated(QString text);
	
	Q_INVOKABLE void update();
	Q_INVOKABLE void sleep(int ms);
	Q_INVOKABLE void wait(int ms);

	Q_INVOKABLE void save(QString s)
	{
		emit wantsToBeSaved(s);
	}

	Q_INVOKABLE void open(QString s)
	{
		emit wantsToBeOpened(s);
	}

	QObject* dialog;
	QObject* dialog2;

signals:
	void gameStarted();
	void gameRestarted();
	void endGame(QString text);
	void gameEnded(QString text);
	void updateStatus(QString text);
	void statusUpdated(QString text);
	void gamePaused();
	void pauseGame();
	void clicked(double x, double y);

	void difficultyChanged(QString difficulty);
	void playersChanged(QString players);
	void containsMouseChanged(bool containsMouse);

	void wantsToBeOpened(QString);
	void wantsToBeSaved(QString);

public slots:
	void emitClicked(double x, double y);
	void emitPauseGame();

	void setDifficulty(QString _difficulty);
	void setPlayers(QString _players);

protected:
	void componentComplete();

private:
	QString difficulty;
	QString players;
	QQuickItem* mousearea;
};
