#pragma once
#include "simplegamewidget/simplegamewidget.h"
#include "gamecontroller/gamecontroller.h"
#include "pausescreen/pausescreen.h"
#include "gamebar/gamebar.h"

void RegisterWidgets()
{
	qmlRegisterType<PauseScreen>("AbstractGames.Widgets", 1, 0, "PauseScreen");
	qmlRegisterType<GameController>("AbstractGames.Widgets", 1, 0, "GameController");
	qmlRegisterType<SimpleGameWidget>("AbstractGames.Widgets", 1, 0, "SimpleGameWidget");
	qmlRegisterType<GameBar>("AbstractGames.Widgets", 1, 0, "GameBar");
}
