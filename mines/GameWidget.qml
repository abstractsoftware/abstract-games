import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.1
import AbstractGames.Widgets 1.0
import "game.js" as Game

SimpleGameWidget {
	id: gamewidget

	onGameRestarted:
	{
		Game.Restart();
	}

	property bool noclick: true

	Grid {
		property var properties: [
			"mines",
			"n",
			"pause"
		]

		id: board
		anchors.fill: parent
		rows: board.n
		columns: board.n

		property int mines: gamewidget.difficulty == "easy" ? 5 : gamewidget.difficulty == "medium" ? 10 : 30
		property int n: gamewidget.difficulty == "easy" ? 10 : gamewidget.difficulty == "medium" ? 12 : 15
		property bool pause: false

		Repeater {
			model: board.n*board.n

			Rectangle {
				id: rect
				width: gamewidget.width/board.n
				height: gamewidget.height/board.n
				color: "transparent"
				z: 0

				Label {
					property var properties: [
						"text",
						"color",
						"visible"
					]

					font.pointSize: 15
					font.bold: true
					id: text
					property bool bomb: false
                                        property bool nb: false
                                        property bool fb: false
                                        property bool nfb: false
                                        property string oldtext: ""
					property int time
					anchors.centerIn: parent
					property var orig_color: Material.color(Material.Red)
					visible: false

					Behavior on color {
						ColorAnimation {
							from: Material.color(Material.Red, Material.Shade300)
							duration: 500
						}
					}
				}

				ToolButton {
					id: toolbutton
					anchors.fill: parent

					MouseArea {
						id: mousearea
                                                anchors.fill: parent
						acceptedButtons: Qt.LeftButton | Qt.RightButton
						propagateComposedEvents: true
						enabled: !gamewidget.noclick

						onClicked:
						{
							mouse.accepted = false;

                                                        if (mouse.button == undefined || mouse.button == Qt.RightButton)
                                                        {
                                                                Game.Click(index, true);
                                                        }
                                                        else
                                                        {
                                                                Game.Click(index, false);
                                                        }
                                               }
					}
                                }
			}
		}
	}
}
