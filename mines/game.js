/*
	Small uillity functions.
*/

function Contains(a, c) {
	for (var i = 0; i < a.length; i++) {
		if (a[i] == c) return true;
	}
	return false;
}

function GetPos(i) {
	var r, c;
	var k = 1;

	for (var j = 0; j < board.n*board.n; j++)
	{
		if (j % board.n == 0 && j != 0) k++;
		if (i < k * board.n)
		{
			c = k - 1;
			r = i - ((k - 1) * board.n);
			break;
		}
	}

	return [r, c];
}

function GetInvPos(r, c) {
	if (r >= board.n || r < 0 || c >= board.n || c < 0) return -1;

	return r + c * board.n
}

/*
	Core game functions.
*/

function Restart() {
	gamewidget.noclick = false;

	var entries = new Array();
	var num = 0;
	var r = 0;
	var c = 0;
	var bomb = "B";

	for (var i = 0; i < board.n*board.n; i++) {
		try
		{
			board.children[i].children[0].text = "";
		}
		catch (any)
		{
			break;
		}
	}

	for (var i = 0; i < board.mines; i++) {
		entries.push(Math.floor(Math.random() * board.n * board.n));
	}

	for (var i = 0; i < board.mines; i++) {
		board.children[entries[i]].children[0].text = bomb;
	}

	for (var i = 0; i < board.n*board.n; i++) {
		board.children[i].children[0].bomb = false;
		board.children[i].children[0].nb = false;
		board.children[i].children[0].color = "#FFFFFF";
		board.children[i].color = "transparent";
		board.children[i].children[1].visible = true;
		board.children[i].visible = true;
		if (!Contains(entries, i)) {
			num = 0;
			r = GetPos(i)[0];
			c = GetPos(i)[1];
			if (GetInvPos(r + 1, c) != -1) {
				if (board.children[GetInvPos(r + 1, c)].children[0].text == bomb) num++;
			}
			if (GetInvPos(r + 1, c + 1) != -1) {
				if (board.children[GetInvPos(r + 1, c + 1)].children[0].text == bomb) num++;
			}
			if (GetInvPos(r, c + 1) != -1) {
				if (board.children[GetInvPos(r, c + 1)].children[0].text == bomb) num++;
			}
			if (GetInvPos(r - 1, c) != -1) {
				if (board.children[GetInvPos(r - 1, c)].children[0].text == bomb) num++;
			}
			if (GetInvPos(r - 1, c - 1) != -1) {
				if (board.children[GetInvPos(r - 1, c - 1)].children[0].text == bomb) num++;
			}
			if (GetInvPos(r, c - 1) != -1) {
				if (board.children[GetInvPos(r, c - 1)].children[0].text == bomb) num++;
			}
			if (GetInvPos(r + 1, c - 1) != -1) {
				if (board.children[GetInvPos(r + 1, c - 1)].children[0].text == bomb) num++;
			}
			if (GetInvPos(r - 1, c + 1) != -1) {
				if (board.children[GetInvPos(r - 1, c + 1)].children[0].text == bomb) num++;
			}

			board.children[i].children[0].text = num;
		}
	}

	for (var i = 0; i < board.n*board.n; i++) {
		board.children[i].children[0].visible = false;
	}
}

function Click(i, f) {
	if (f)
	{
		if (board.children[i].children[0].visible == false)
		{
			board.children[i].children[0].visible = true;
			board.children[i].children[0].oldtext = board.children[i].children[0].text;
			board.children[i].children[0].color = "#2196F3";
			board.children[i].children[0].text = "F";
		}
		else if (board.children[i].children[0].text == "F")
		{
			board.children[i].children[0].visible = false;
			board.children[i].children[0].text = board.children[i].children[0].oldtext;
			board.children[i].children[0].oldtext = "";
			board.children[i].children[0].color = board.children[i].children[0].orig_color;
		}

		return;
	}

	if (board.children[i].children[0].text != "" && board.children[i].children[0].text != "F"  
		&& board.children[i].children[0].visible != true) {

		board.children[i].children[0].visible = true;

		if (board.children[i].children[0].text == "B") {
			board.children[i].children[0].visible = true;
                	board.children[i].children[0].color = "#FFFFFF";
        	        gamewidget.update();
	                board.children[i].children[0].color = "#F44336";

			var j = i;

			gamewidget.noclick = true;

			for (var i = 0; i < board.n*board.n; i++) {
				if (board.children[i].children[0].text == "B" && i != j) {
					board.children[i].children[0].visible = true;
					board.children[i].children[0].color = "#FFFFFF";
					gamewidget.update();
					board.children[i].children[0].color = "#F44336";
					board.children[i].children[0].text = "B";
				}

				gamewidget.wait(25);
			}

			gamewidget.wait(25);
			gamewidget.endGame("You Lost");
			return;
		} else {
			if (board.children[i].children[0].text == "0") {
				var r = GetPos(i)[0];
				var c = GetPos(i)[1];
				if (GetInvPos(r + 1, c) != -1) {
					Click(GetInvPos(r + 1, c), false);
				}
				if (GetInvPos(r + 1, c + 1) != -1) {
					Click(GetInvPos(r + 1, c + 1), false);
				}
				if (GetInvPos(r, c + 1) != -1) {
					Click(GetInvPos(r, c + 1), false);
				}
				if (GetInvPos(r - 1, c) != -1) {
					Click(GetInvPos(r - 1, c), false);
				}
				if (GetInvPos(r - 1, c - 1) != -1) {
					Click(GetInvPos(r - 1, c - 1), false);
				}
				if (GetInvPos(r, c - 1) != -1) {
					Click(GetInvPos(r, c - 1), false);
				}
				if (GetInvPos(r + 1, c - 1) != -1) {
					Click(GetInvPos(r + 1, c - 1), false);
				}
				if (GetInvPos(r - 1, c + 1) != -1) {
					Click(GetInvPos(r - 1, c + 1), false);
				}
			}

			board.children[i].children[0].color = "#2C8F30";

			var notmines = 0;
			for (var i = 0; i < board.n*board.n; i++) {
				if (board.children[i].children[0].text != "B" &&
					board.children[i].children[0].text != "F" && 
					board.children[i].children[0].visible == true) {
					notmines++;
				}
			}

			if (notmines == board.n*board.n - board.mines)
			{
				gamewidget.endGame("You Won");
				return;
			}
		}
	}
}
