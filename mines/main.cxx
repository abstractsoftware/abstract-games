#include <QApplication>
#include <QFont>

#include "main.h"
#include "../libabstractgames/widgets/widgets.h"
#include "../libabstractgames/logic/logic.h"

int main(int argc, char* argv[])
{
        RegisterWidgets();
        RegisterLogic();

        QApplication* app = new QApplication(argc, argv);
        
	LaunchMines();

        return app->exec();
}
