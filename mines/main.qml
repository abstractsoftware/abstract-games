import QtQuick 2.7
import AbstractGames.Widgets 1.0
import QtQuick.Controls.Material 2.0

GameController {
	gameType: "ArcadeGame"
	name: "Mines"

	background: Rectangle {
		color: Material.color(Material.Red, Material.Shade300)
	}

}
