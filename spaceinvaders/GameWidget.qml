import QtQuick 2.9
import QtQuick.Layouts 1.9
import AbstractGames.Logic 1.0 as Logic
import AbstractGames.Widgets 1.0
import QtQuick.Controls 2.7
import "game.js" as Game

SimpleGameWidget {
	id: gamewidget

	RowLayout {
		anchors.top: parent.top
		ToolButton {
			text: "<"
			width: 20
			height: 20

			onPressed: {
				doing = true
			}

			onReleased: {
				doing = false
			}

			property bool doing: false

			Timer {
				running: parent.doing
				repeat: true
				interval: 10

				onTriggered: {
					cannon.x -= 5;
				}
			}
		}

		ToolButton {
                        text: ">"
                        width: 20
			height: 20

			onPressed: {
                                doing = true
                        }

                        onReleased: {
                                doing = false
                        }

                        property bool doing: false

                        Timer {
                                running: parent.doing
                                repeat: true
                                interval: 10

                                onTriggered: {
                                        cannon.x += 5;
                                }
                        }
		}

		ToolButton {
			text: "S"
			width: 20
			height: 20

			onClicked: {
				shoot.y = cannon.y - cannon.height
	                        shoot.running = true;
			}
		}
	}

	onGameRestarted: {
		Game.Restart();
	}

	Rectangle {
		id: shoot
		width: 30
		height: 30
		x: cannon.x// + cannon.width*0.5
		y: gamewidget.height
		visible: running
		property bool running: false

		Timer {
			running: parent.running
			interval: 1
			repeat: true

			onTriggered: {
				parent.y -= 10;

				for (var i = 0; i < 1; i++)
			        {
        			        if (intersecting(enemy.children[i], shoot))
       				        {
        			                enemy.children[i].visible = false;
			                }
				}
	
				if (parent.y < 0)
				{
					parent.running = false;
				}
			}
		}
	}

	RowLayout {
		id: enemy
		anchors.top: parent.top
		anchors.topMargin: 50
		x: 0

		Timer {
			repeat: true
			running: true
			interval: 10

			onTriggered: {
				if (parent.x > gamewidget.width)
				{
					parent.x = 0
				} else
				{
					parent.x += 0.1
				}
			}
		}

		Repeater {
			model: 1

			Rectangle {
				color: "blue"
				width: 30
				height: 30

				Rectangle {
					parent: gamewidget
					width: 30
					height: 30
					visible: false
					//x: (!run) ? parent.x : null
					//y: (!run) ? parent.y : nul
					property bool run: false
					property int start: 1

					Timer {
						repeat: true
						running: parent.run
						interval: 1

						onTriggered: {
							parent.y += 10;
							parent.x = parent.start;//gamewidget.mapToItem(parent, 0, 0).x;

							if (parent.y > gamewidget.height)
							{
								parent.run = false;
							}

                   			                if (Game.intersecting(cannon, parent))
							{
								parent.run = false;
								gamewidget.endGame("You lost!");
                			                }
						}
					}

					Timer {
						repeat: true
						running: true
						interval: 1000

						onTriggered: {
							if (!parent.run)
							{
								if (Math.random() > 0.67)
								{
									//parent.x = 0
									parent.start = enemy.x + index*30 + 15;
									parent.y = enemy.y;
									parent.run = true
									parent.visible = true
								}
							}
						}
					}
				}
			}
		}
	}

	Rectangle {
		id: cannon
		anchors.bottom: parent.bottom
		height: 30
		width: 50
		anchors.bottomMargin: 10
		x: 10
		color: "red"
	}
}
