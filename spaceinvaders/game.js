/*
	Core game functions.
*/

.import AbstractGames.Logic 1.0 as Logic

function Restart()
{
}

function intersecting(item1, item2)
{
	for (var x = 0; x < item1.width; x++)
	{
		for (var y = 0; y < item1.height; y++)
		{
			if (item2.contains(item1.mapToItem(item2, x, y)))
			{
				return true;
			}
		}
	}

	return false;
}

function Check()
{
	for (var i = 0; i < 1; i++)
	{
		if (intersecting(enemy.children[i], shoot))
		{
			enemy.children[i].visible = false;
		}
	}
}
