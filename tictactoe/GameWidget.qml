import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.1
import AbstractGames.Widgets 1.0
import QtGraphicalEffects 1.12

import "game.js" as Game

SimpleGameWidget {
	id: gamewidget

	onGameRestarted: {
		Game.Restart()
	}

	Grid {
		property int n: 3 

		id: board
		columns: n 
		rows: n 

		property string player: "X"
		property bool pause: false

		Repeater {
			model: board.n*board.n 

			Rectangle {
				width: gamewidget.width/board.n
				height: gamewidget.height/board.n
				color: "transparent"
				id: square

				ItemDelegate {
					enabled: text.text == ""
					id: mousearea

					onClicked: {
						Game.Click(index);
					}

					anchors.centerIn: parent
					anchors.fill: parent
				}

				border {
					width: 1
					color: "black"
				}

				Text {
					id: text
					color: "#00000000"
				}

				Image {
					id: img
					anchors.centerIn: parent
					smooth: true
					source: text.text == "X" ? "images/x.svg" : "images/o.svg"
					visible: text.text != "" 
				}

				Colorize {
					visible: img.visible
					anchors.fill: img
					source: img
					hue: text.text == "X" ? 0 : 0.65
					saturation: 1
					lightness: -0.5
				}

				property bool clickedred: false
				property bool clickedblue: false

				/*ColorAnimation {
					target: square 
					properties: "color"
					running: clickedred
					duration: 2500
					from: "#B0BEC5"
					to: "#F44336"
				}

				ColorAnimation {
					target: square 
					properties: "color"
					running: clickedblue
					duration: 2500
					from: "#B0BEC5"
					to: "#2196F3"
				}*/
			}
		}
	}
}
