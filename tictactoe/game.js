/*
	Small utillity functions.
*/

function CanMakeThree()
{
	function istheretwo(a, b, c, x)
	{
		if (a[0] == b[0] && b[0] == x) return c;
		if (a[0] == c[0] && c[0] == x) return b;
		if (b[0] == c[0] && c[0] == x) return a;
		return ["INVAILD", null];
	}

	var moves = new Array();

	var n1, n2, n3;

	// Blocking moves

	// Columns

	n1 = [board.children[0].children[1].text, 0];
	n2 = [board.children[3].children[1].text, 3];
	n3 = [board.children[6].children[1].text, 6];

	if (istheretwo(n1, n2, n3, board.player == "X" ? "O" : "X")[0] == "")
		moves.push([istheretwo(n1, n2, n3, board.player == "X" ? "O" : "X")[1], 0]);

	n1 = [board.children[1].children[1].text, 1];
        n2 = [board.children[4].children[1].text, 4];
        n3 = [board.children[7].children[1].text, 7];

        if (istheretwo(n1, n2, n3, board.player == "X" ? "O" : "X")[0] == "")
		moves.push([istheretwo(n1, n2, n3, board.player == "X" ? "O" : "X")[1], 0]);

	n1 = [board.children[2].children[1].text, 2];
        n2 = [board.children[5].children[1].text, 5];
        n3 = [board.children[8].children[1].text, 8];

        if (istheretwo(n1, n2, n3, board.player == "X" ? "O" : "X")[0] == "")
                moves.push([istheretwo(n1, n2, n3, board.player == "X" ? "O" : "X")[1], 0]);

	// Rows

	n1 = [board.children[0].children[1].text, 0];
        n2 = [board.children[1].children[1].text, 1];
        n3 = [board.children[2].children[1].text, 2];

        if (istheretwo(n1, n2, n3, board.player == "X" ? "O" : "X")[0] == "")
                moves.push([istheretwo(n1, n2, n3, board.player == "X" ? "O" : "X")[1], 0]);

        n1 = [board.children[3].children[1].text, 3];
        n2 = [board.children[4].children[1].text, 4];
        n3 = [board.children[5].children[1].text, 5];

        if (istheretwo(n1, n2, n3, board.player == "X" ? "O" : "X")[0] == "")
                moves.push([istheretwo(n1, n2, n3, board.player == "X" ? "O" : "X")[1], 0]);

        n1 = [board.children[6].children[1].text, 6];
        n2 = [board.children[7].children[1].text, 7];
        n3 = [board.children[8].children[1].text, 8];

        if (istheretwo(n1, n2, n3, board.player == "X" ? "O" : "X")[0] == "")
                moves.push([istheretwo(n1, n2, n3, board.player == "X" ? "O" : "X")[1], 0]);

	// Diagonals
	

	n1 = [board.children[0].children[1].text, 0];
        n2 = [board.children[4].children[1].text, 4];
        n3 = [board.children[8].children[1].text, 8];

        if (istheretwo(n1, n2, n3, board.player == "X" ? "O" : "X")[0] == "")
                moves.push([istheretwo(n1, n2, n3, board.player == "X" ? "O" : "X")[1], 0]);

        n1 = [board.children[2].children[1].text, 2];
        n2 = [board.children[4].children[1].text, 4];
        n3 = [board.children[6].children[1].text, 6];

        if (istheretwo(n1, n2, n3, board.player == "X" ? "O" : "X")[0] == "")
                moves.push([istheretwo(n1, n2, n3, board.player == "X" ? "O" : "X")[1], 0]);

	// Non blocking moves

	// Columns

	n1 = [board.children[0].children[1].text, 0];
	n2 = [board.children[3].children[1].text, 3];
	n3 = [board.children[6].children[1].text, 6];

	if (istheretwo(n1, n2, n3, board.player)[0] == "")
		moves.push([istheretwo(n1, n2, n3, board.player)[1], 1]);

	n1 = [board.children[1].children[1].text, 1];
        n2 = [board.children[4].children[1].text, 4];
        n3 = [board.children[7].children[1].text, 7];

        if (istheretwo(n1, n2, n3, board.player)[0] == "")
		moves.push([istheretwo(n1, n2, n3, board.player)[1], 1]);

	n1 = [board.children[2].children[1].text, 2];
        n2 = [board.children[5].children[1].text, 5];
        n3 = [board.children[8].children[1].text, 8];

        if (istheretwo(n1, n2, n3, board.player)[0] == "")
                moves.push([istheretwo(n1, n2, n3, board.player)[1], 1]);

	// Rows

	n1 = [board.children[0].children[1].text, 0];
        n2 = [board.children[1].children[1].text, 1];
        n3 = [board.children[2].children[1].text, 2];

        if (istheretwo(n1, n2, n3, board.player)[0] == "")
                moves.push([istheretwo(n1, n2, n3, board.player)[1], 1]);

        n1 = [board.children[3].children[1].text, 3];
        n2 = [board.children[4].children[1].text, 4];
        n3 = [board.children[5].children[1].text, 5];

        if (istheretwo(n1, n2, n3, board.player)[0] == "")
                moves.push([istheretwo(n1, n2, n3, board.player)[1], 1]);

        n1 = [board.children[6].children[1].text, 6];
        n2 = [board.children[7].children[1].text, 7];
        n3 = [board.children[8].children[1].text, 8];

        if (istheretwo(n1, n2, n3, board.player)[0] == "")
                moves.push([istheretwo(n1, n2, n3, board.player)[1], 1]);

	// Diagonals
	

	n1 = [board.children[0].children[1].text, 0];
        n2 = [board.children[4].children[1].text, 4];
        n3 = [board.children[8].children[1].text, 8];

        if (istheretwo(n1, n2, n3, board.player)[0] == "")
                moves.push([istheretwo(n1, n2, n3, board.player)[1], 1]);

        n1 = [board.children[2].children[1].text, 2];
        n2 = [board.children[4].children[1].text, 4];
        n3 = [board.children[6].children[1].text, 6];

        if (istheretwo(n1, n2, n3, board.player)[0] == "")
                moves.push([istheretwo(n1, n2, n3, board.player)[1], 1]);


	return moves;
}

function PlayAI() {
	var moves = CanMakeThree();

	if (moves.length == 0)
	{
		for (var index = 0; index < board.n*board.n; index++) {
			if (board.children[index].children[1].text == "") {
				moves.push([index, 1]);
			}
		}
	}

	var GoodMoves = new Array();

	for (var i = 0; i < moves.length; i++)
	{
		if (moves[i][1] == 1) GoodMoves.push(moves[i][0]);
	}

	if (GoodMoves.length == 0)
	{
		for (var i = 0; i < moves.length; i++)
		{
			if (moves[i][1] == 0) GoodMoves.push(moves[i][0]);
		}
		
		if (GoodMoves.length == 0)
		{
			for (var i = 0; i < moves.length; i++)
			{
				if (moves[i][1] == 2) GoodMoves.push(moves[i][0]);
			}

			if (GoodMoves.length == 0)
			{
				for (var i = 0; i < moves.length; i++)
				{
					GoodMoves.push(moves[i][0]);
				}
			}
		}
	}

	var move = GoodMoves[Math.floor(Math.random() * GoodMoves.length)];

	board.children[move].children[1].text = board.player;
	if (board.player == "X")
		board.children[move].clickedred = true;
	else
		board.children[move].clickedblue = true;
}

function AlertUser() {
	if (CheckForWin() == "X") {
		if (gamewidget.players == "one")
		{
			gamewidget.endGame("You Won");
		}
		else
		{
			gamewidget.endGame("X Won");
		}
		return -1;
	} else if (CheckForWin() == "O") {
		if (gamewidget.players == "one")
		{
			gamewidget.endGame("Spectrum Won");
		}
		else
		{
			gamewidget.endGame("O Won");
		}
		return -1;
	}

	if (CheckForDraw()) {
		gamewidget.endGame("Draw");
		return -1;
	}

	return 0;
}

function CheckForDraw() {
	for (var index = 0; index < board.n*board.n; index++) {
		if (board.children[index].children[1].text == "")
			return false;
	}
	return true;
}

function CheckForWin() {
	for (var i = 0; i < board.n*board.n; i++)
	{
		if (board.children[i].children[1].text != "")
		{
			if (board.children[i + 1] != undefined && board.children[i + 1].children[1] != undefined) 
			{
				if (board.children[i].children[1].text == board.children[i + 1].children[1].text)
				{
					if (board.children[i + 2] != undefined && board.children[i + 2].children[1] != undefined)
					{
						if (board.children[i].children[1].text == board.children[i + 2].children[1].text)
						{
							if (((i + 1) % board.n != 0) && ((i + 2) % board.n != 0))
								return board.children[i].children[1].text;
						}
					}
				}
			}
		}
		if (board.children[i].children[1].text != "")
		{
			if (board.children[i + board.n] != undefined && board.children[i + board.n].children[1] != undefined)
			{
				if (board.children[i].children[1].text == board.children[i + board.n].children[1].text)
				{
					if (board.children[i + board.n*2] != undefined 
						&& board.children[i + board.n*2].children[1] != undefined)
					{
						if (board.children[i + board.n*2].children[1].text == board.children[i].children[1].text)
						{
							return board.children[i].children[1].text;
						}
					}
				}
			}
		}
		if (board.children[i].children[1].text != "")
		{
			if (board.children[i + (board.n + 1)] != undefined  && board.children[i + (board.n + 1)].children[1] != undefined)
			{
				if (board.children[i].children[1].text == board.children[i + (board.n + 1)].children[1].text)
				{
					if (board.children[i + (board.n + 1)*2] != undefined 
						&& board.children[i + (board.n + 1)*2].children[1] != undefined)
					{
						if (board.children[i + (board.n + 1)*2].children[1].text 
							== board.children[i].children[1].text)
						{
							if ((i + (board.n + 1)*2) % board.n != 0)
								return board.children[i].children[1].text;
						}
					}
				}
			}
		}
		if (board.children[i].children[1].text != "")
		{
			if (board.children[i + (board.n - 1)] != undefined && board.children[i + (board.n - 1)].children[1] != undefined)
			{
				if (board.children[i].children[1].text == 
					board.children[i + (board.n - 1)].children[1].text)
				{
					if (board.children[i + (board.n - 1)*2] != undefined && 
						board.children[i + (board.n - 1)*2].children[1] != undefined)
					{
						if (board.children[i + (board.n - 1)*2].children[1].text == 
							board.children[i].children[1].text)
						{
							if (((i + (board.n - 1)) % board.n != 0) && 
								((i + board.n) % board.n != 0))
				 				return board.children[i].children[1].text;
						}
					}
				}
			}
		}
	}

	return "";
}

function Restart() {
	board.player = "X";

	for (var index = 0; index < board.n*board.n; index++) {
		board.children[index].children[1].text = "";
		board.children[index].clickedred = false;
		board.children[index].clickedblue = false;
		//board.children[index].color = "#B0BEC5";
	}
}

function Click(index) {
	if (board.children[index].children[1].text == "") {
	        if (board.player == "X") {
			board.children[index].children[1].text = "X";
			board.children[index].clickedred = true;

			board.player = "O";
		} else {
			board.children[index].children[1].text = "O";
			board.children[index].clickedblue = true;

			board.player = "X";
		}
	} else {
		return;
	}

	if (AlertUser() == 0 && gamewidget.players != "two") {
		PlayAI();

   		//if (gamewidget.players == "two") {
        	board.player = "X";
   	 	//} else {
        	//	board.player = "O";
    		//}

		AlertUser();
	}
}

